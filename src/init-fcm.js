import firebase from 'firebase/app'
import 'firebase/messaging'
const initializedFirebaseApp = firebase.initializeApp({
    messagingSenderId: '425118420550',
    apiKey: "AIzaSyDIFGj8T-sshZHM_7jUskFv2pWOEK_YnWw",
    authDomain: "irbit-f7b38.firebaseapp.com",
    projectId: "irbit-f7b38",
    storageBucket: "irbit-f7b38.appspot.com",
    messagingSenderId: "425118420550",
    appId: "1:425118420550:web:3c6585b37ca6c11fbb4447",
    measurementId: "G-Z4YF80CRFW"

    // Project Settings => Add Firebase to your web app
    // messagingSenderId: '962676166711',
    // apiKey: 'AIzaSyBOcUYRq_AlP8gwc5a_hJDQYu_MTBzG3kc',
    // authDomain: 'forusha.firebaseapp.com',
    // databaseURL: 'https://forusha.firebaseio.com',
    // projectId: 'forusha',
    // storageBucket: 'forusha.appspot.com',
    // appId: '1:962676166711:web:b670d2a2f4293dd8326fb2',
    // measurementId: 'G-CFFVN8BE52',
})
var messaging = null

if (firebase.messaging.isSupported()) {
    messaging = initializedFirebaseApp.messaging()
    messaging.usePublicVapidKey(
        // Project Settings => Cloud Messaging => Web Push certificates
        'BMGDA4xQLLA3uVFAKPDUttmRNZyHCVm6v_vCGhbqUbmj07xE12MYIJGlA8dG5yzyRUh0TIZCYoO331A3qvhfXt4'
    )
}
export { messaging }
