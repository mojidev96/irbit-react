import React, { useEffect, useState, useRef, createRef } from 'react'
import { AppBar, Toolbar, Dialog, Typography, DialogTitle, Backdrop, DialogContent, DialogContentText, DialogActions, GridList, Fab, Box, LinearProgress, IconButton, TextField, Checkbox, RadioGroup, Card, MenuItem, FormControlLabel, Button, CircularProgress, Grid, Divider } from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { accentcolor, backgroundColor, globalTabIndex, greenColor, itemColor, notifyErrorMessage, token, webUrl } from '../../App'
import { parsePrice, parseLikeOrDislikes, parseNumber } from '../../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../../widgets/EmptyView'
import TryAgainWidget from '../../widgets/TryAgainWidget'
import { isAndroid, isIOS } from 'react-device-detect'
import ArroUpIcon from '../../icons/up-arrow.png'
import ArroDownIcon from '../../icons/down-arrow.png'
import ViewIcon from '../../icons/view.png'
import TitleIcon from '../../icons/title.png'
import ChatIcon from '../../icons/chat.png'
import ArroUpOutlineIcon from '../../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../../icons/down-arrow-outline.png'
import { Category } from '@material-ui/icons'
import LoadingDialog from '../dialogs/LoadingDialog'

function SocietyCategoriesScreen(props) {
    let history = useHistory()

    const [isLoading, setLoading] = useState(true)
    const [cats, setCats] = useState([])
    const [itHasProblem, setItHasProblem] = useState(false)

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem('societyData'))
        if (data.length > 0) {
            setCats(data)
            setLoading(false)
            setItHasProblem(false)
        } else {
            getCats()
        }

    }, [])

    useEffect(() => {
        if (globalTabIndex == 2) {
            window.scrollTo({ top: localStorage.getItem('societyScroll') })
        }
        window.onscroll = () => {
            if (window.pageYOffset != 0 && globalTabIndex == 2) {
                localStorage.setItem('societyScroll', window.pageYOffset)
            }
        }

    }, [globalTabIndex])


    const getCats = async () => {
        setLoading(true)
        setItHasProblem(false)
        try {
            const response = await Axios.get('societyCategories', { headers: { token: token } })
            if (response.data instanceof Array && response.data.length > 0) {
                setCats(response.data)
                localStorage.setItem('societyData', JSON.stringify(response.data))
            }
        } catch (error) {
            //console.log('err comments :: ', error)
            setItHasProblem(true)
        }
        setLoading(false)
    }

    const pushToTopicsList = (id, title) => {
        history.push({
            pathname: '/society/topics',
            search: '?id=' + id,
            data: { title },
        })
    }

    const [upDownLoadingId, setUpDownLoadingId] = useState(0)
    const upOrDown = async (index, id, isUp, isForgive) => {
        if (localStorage.getItem('token') == null) {
            history.push('/login')
            return
        }
        setUpDownLoadingId(id)
        try {
            const response = await Axios.post('societyUpAndDowns', {
                isUp,
                isForgive,
                contentId: id,
                contentType: 0,
            })
            if (response.status == 201) {
                if (isUp == true) {
                    if (cats[index].iDown != null && cats[index].iDown == true) {
                        cats[index].iDown = false
                        cats[index].downs -= 1
                    }
                } else {
                    if (cats[index].iUp != null && cats[index].iUp == true) {
                        cats[index].iUp = false
                        cats[index].ups -= 1
                    }
                }

                if (isUp == true) {
                    cats[index].iUp = isForgive == false
                } else {
                    cats[index].iDown = isForgive == false
                }

                if (isUp == true) {
                    if (isForgive == false) {
                        cats[index].ups += 1
                    } else {
                        cats[index].ups -= 1
                    }
                } else {
                    if (isForgive == false) {
                        cats[index].downs += 1
                    } else {
                        cats[index].downs -= 1
                    }
                }
                setCats(cats)
            } else {
                notifyErrorMessage('نشد که بشه... لطفا دوباره امتحان کنید.')
            }
        } catch (error) { }
        setUpDownLoadingId(0)
    }

    return (
        <div style={{ direction: 'rtl', overflow: 'hidden', display: 'flex', justifyContent: 'center', minHeight: window.innerHeight }}>
            {!isLoading && itHasProblem ? (
                <TryAgainWidget onTryAgain={getCats} />
            ) : cats.length == 0 && !isLoading ? (
                <EmptyView />
            ) : (
                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%' }}>
                    {isLoading && cats.length == 0 ? <LoadingDialog /> : <div />}
                    <div style={{ width: '100%', paddingTop: 56, overflow: 'hidden' }}>
                        <GridList id="gridSocietyList" cellHeight="auto" cols={isAndroid || isIOS ? 1 : props.width / 300 - 16} style={{ justifyContent: 'center', overflowX: 'hidden', paddingBottom: 32, width: '100%' }}>
                            {cats.map((data, index) => (
                                <div
                                    key={data.id}
                                    className={!isAndroid && !isIOS ? 'cardHover' : ''}
                                    style={{
                                        width: isAndroid || isIOS ? '100%' : 300,
                                        height: isAndroid || isIOS ? 150 : 180,
                                        marginTop: 4,
                                        marginBottom: 4,
                                        marginRight: isAndroid || isIOS ? 0 : 4,
                                        marginLeft: isAndroid || isIOS ? 0 : 4,
                                        paddingTop: 4,
                                        paddingBottom: 4,
                                        borderStyle: isAndroid || isIOS ? 'none' : 'solid',
                                        borderRadius: isAndroid || isIOS ? 0 : 4,
                                        borderWidth: isAndroid || isIOS ? 0 : 2,
                                        backgroundColor: itemColor,
                                        borderColor: '#99999939',
                                    }}
                                    onClick={() => pushToTopicsList(data.id, data.title)}
                                >
                                    <div style={{ display: 'flex', width: '100%', height: isAndroid || isIOS ? 150 : 180 }}>
                                        <img
                                            src={webUrl + data.icon}
                                            className="image_no_background"
                                            style={{ backgroundColor: backgroundColor, width: isAndroid || isIOS ? 100 : 70, minWidth: isAndroid || isIOS ? 100 : 70, height: isAndroid || isIOS ? 100 : 70, minHeight: isAndroid || isIOS ? 100 : 70, borderRadius: isAndroid || isIOS ? 50 : 35, margin: 8, objectFit: 'cover' }}
                                        />
                                        <div style={{ width: '100%', overflow: 'hidden', display: 'flex', flexFlow: 'column', paddingLeft: 8 }}>
                                            <div style={{ flex: 1, overflow: 'hidden' }}>
                                                <p className="PrimaryText" style={{ marginTop: 12 }}>
                                                    {data.title}
                                                </p>
                                                <p className="ThirdText">{data.description}</p>
                                            </div>
                                            <div style={{ display: 'flex', alignItems: 'center', direction: 'ltr', float: 'inline-end', marginTop: 12, marginBottom: 4 }}>
                                                {upDownLoadingId != data.id ? (
                                                    <img
                                                        className="clickHoverJustTooZoom"
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            upOrDown(index, data.id, false, data.iDown != null && data.iDown == true)
                                                        }}
                                                        src={data.iDown && data.iDown == true ? ArroDownIcon : ArroDownOutlineIcon}
                                                        style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16 }}
                                                    />
                                                ) : (
                                                    <CircularProgress size={16} style={{ marginLeft: 4 }} color="secondary" />
                                                )}
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(data.downs)}
                                                </p>

                                                {upDownLoadingId != data.id ? (
                                                    <img
                                                        className="clickHoverJustTooZoom"
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            upOrDown(index, data.id, true, data.iUp != null && data.iUp == true)
                                                        }}
                                                        src={data.iUp && data.iUp == true ? ArroUpIcon : ArroUpOutlineIcon}
                                                        style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }}
                                                    />
                                                ) : (
                                                    <CircularProgress size={16} style={{ marginLeft: 8 }} color="secondary" />
                                                )}
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(data.ups)}
                                                </p>

                                                <img src={ViewIcon} style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(data.seenCount)}
                                                </p>

                                                <img src={ChatIcon} style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(data.messagesCount)}
                                                </p>

                                                <img src={TitleIcon} style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(data.topicsCount)}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </GridList>
                    </div>
                    {cats.length > 0 && isLoading ? (
                        <div className="StackElement" style={{ bottom: 0, position: 'fixed', width: '100%' }}>
                            <LinearProgress color="secondary" />
                        </div>
                    ) : (
                        <div />
                    )}
                </div>
            )}

            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default SocietyCategoriesScreen
