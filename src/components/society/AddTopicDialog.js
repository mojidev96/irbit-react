import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, IconButton, TextField, Checkbox, RadioGroup, Radio, Button, CircularProgress, Divider } from '@material-ui/core'
import { Bar } from 'react-chartjs-2'
import { notifySuccessful, notifyError, notifyLoginNeeded, notifyErrorMessage, accentcolor, greenColor, blueColor, account, notifyWarnMessage } from '../../App'
import Axios from 'axios'
import { MdArrowForward, MdBookmarkBorder, MdCheckCircle, MdChat, MdExitToApp, MdInfo, MdMoreVert, MdOpenInNew, MdCall, MdStore, MdReport, MdShare, MdClose, MdEdit, MdDelete, MdAdd } from 'react-icons/md'
import { parseDistance, parsePrice, parseLikeOrDislikes, parseDate } from '../../tools/extentions'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import copy from 'copy-to-clipboard'
const Compress = require('compress.js')
function AddTopicDialog(props) {
    let history = useHistory()
    const [isLoading, setLoading] = useState(false)

    const isFromLocalScreens = useRef(false)
    const [responseMessage, setResponseMessage] = useState('')
    const [isShowingResponse, setShowingResponse] = useState('')
    const [responseColor, setResponseColor] = useState(greenColor)

    const [topicTitle, setTopicTitle] = useState('')
    const [topicDescription, setTopicDescription] = useState('')

    const [avatarFile, setAvatarFile] = useState()
    const [avatarPreview, setAvatarPreview] = useState()
    const catId = useRef(0)

    useEffect(() => {
        catId.current = props.data.catId
        setTopicTitle(localStorage.getItem('gti8371221123') ?? '')
        setTopicDescription(localStorage.getItem('gde837122112') ?? '')
    }, [])

    useEffect(() => {
        if (!avatarFile) {
            setAvatarPreview(undefined)
            return
        }

        var objectUrl
        objectUrl = URL.createObjectURL(avatarFile)
        setAvatarPreview(objectUrl)
        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [avatarFile])

    const pickAvatarImage = () => {
        document.getElementById('pickImageInput').click()
    }

    const onSelectAatarFile = async (e) => {
        if (!e.target.files || e.target.files.length === 0) {
            setAvatarFile(undefined)
            return
        }

        if (e.target.files[0].size > 100000) {
            try {
                const compress = new Compress()
                await compress
                    .compress([e.target.files[0]], {
                        size: 0.2,
                        quality: 0.75,
                        maxWidth: 1024,
                        maxHeight: 1024,
                        resize: true,
                    })
                    .then((data) => {
                        let mimeType = 'image/png'
                        const bstr = atob(data[0].data)
                        let n = bstr.length
                        const u8arr = new Uint8Array(n)
                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n)
                        }
                        setAvatarFile(new File([u8arr], data.alt, { type: mimeType }))
                    })
            } catch (error) {
                //console.log(error)
                notifyErrorMessage('خطا در فشرده سازی تصویر.')
            }
        } else {
            setAvatarFile(e.target.files[0])
        }
    }

    useEffect(() => {}, [])

    const addTopic = async () => {
        if (topicTitle.length === 0 || topicDescription.length === 0) {
            return
        }
        setLoading(true)
        closeResponseMessage()

        const formData = new FormData()
        formData.append('title', topicTitle)
        formData.append('description', topicDescription)
        formData.append('catId', catId.current)
        if (avatarFile != null) {
            formData.append('image', avatarFile)
        }

        await Axios.post('societyTopics', formData)
            .then((response) => {
                setResponseMessage(response.data.message)
                if (response.status == 201) {
                    localStorage.setItem('gti8371221123', '')
                    localStorage.setItem('gde837122112', '')
                    props.data.onDone()
                }
            })
            .catch((err) => {
                if (err.response.status == 401) {
                    setResponseMessage('لطفا ابتدا وارد حساب کاربری خود شوید.')
                } else {
                    setResponseMessage(err.response.data.message)
                }
                setResponseColor(accentcolor)
                openResponseMessage()
            })

        setLoading(false)
    }

    const onCloseScreen = () => {
        isFromLocalScreens ? props.data.onCloseFunc() : history.goBack()
    }

    const openResponseMessage = () => {
        setShowingResponse(true)
    }

    const closeResponseMessage = () => {
        setShowingResponse(false)
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="sm" aria-labelledby="addressDialog">
            <div>
                {isLoading ? (
                    <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                        <CircularProgress color="white" />
                    </Backdrop>
                ) : (
                    <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 24, paddingRight: 24, paddingLeft: 24, paddingBottom: 16 }}>
                        <p className="PrimaryText">ثبت موضوع جدید</p>
                        <p className="ThirdLightText" style={{ marginLeft: 16, marginTop: 6, textAlign: 'justify' }}>
                            لطفا موضوع مرتبط با این دسته بندی بنویسید. با توجه به ماهیت انجمن، ویرایش محتوای این موضوع پس از انتشار میسر نمی باشد. شما می توانید از منوی برنامه و در بخش انجمن اقدام به حذف یک موضوع نمایید. مطابق قانون مسئولیت محتوای منتشر شده در فضای مجازی به عهده ناشر می باشد.
                        </p>
                        <TextField
                            label="عنوان"
                            value={topicTitle}
                            onChange={(e) => {
                                localStorage.setItem('gti8371221123', e.target.value)
                                setTopicTitle(e.target.value)
                            }}
                            style={{ direction: 'rtl', textAlign: 'right', marginTop: 16, marginBottom: 8 }}
                        />
                        <TextField
                            label="توضیحات"
                            multiline
                            value={topicDescription}
                            onChange={(e) => {
                                localStorage.setItem('gde837122112', e.target.value)
                                setTopicDescription(e.target.value)
                            }}
                            style={{ direction: 'rtl', textAlign: 'right', marginTop: 0, marginBottom: 8 }}
                        />
                        <div className="StackContainer" onClick={pickAvatarImage} style={{ marginTop: 12, marginBottom: 16, width: '100%', height: 200 }}>
                            <MdAdd size={24} color="black" className="StackElement" style={{ backgroundColor: 'white', borderBottomLeftRadius: 4 }} />
                            <input id="pickImageInput" type="file" hidden onChange={onSelectAatarFile} />
                            <img src={avatarPreview} className="image" style={{ borderRadius: 0, width: '100%', height: 200 }} />
                        </div>
                        <Button variant="contained" onClick={addTopic} fullWidth={true} style={{ backgroundColor: blueColor, color: 'white' }}>
                            ثبت
                        </Button>
                        <div style={{ height: isShowingResponse ? 'auto' : 0, display: 'flex', alignItems: 'center', backgroundColor: responseColor, marginTop: 8, paddingRight: 8, paddingLeft: 8, visibility: isShowingResponse ? 'visible' : 'hidden' }}>
                            <div style={{ width: 20, height: 20 }}>
                                <MdClose size={20} color="white" onClick={closeResponseMessage} />
                            </div>
                            <p className="ThirdBoldText" style={{ color: 'white', padding: 8 }}>
                                {responseMessage}
                            </p>
                        </div>
                    </div>
                )}
            </div>
        </Dialog>
    )
}

export default AddTopicDialog
