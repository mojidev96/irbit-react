import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, IconButton, TextField, Checkbox, RadioGroup, Radio, Button, CircularProgress, Divider } from '@material-ui/core'
import { Bar } from 'react-chartjs-2'
import { notifySuccessful, notifyError, notifyLoginNeeded, notifyErrorMessage, accentcolor, greenColor, blueColor, account, notifyWarnMessage, backgroundColor } from '../../App'
import Axios from 'axios'
import { MdArrowForward, MdBookmarkBorder, MdCheckCircle, MdChat, MdExitToApp, MdInfo, MdMoreVert, MdOpenInNew, MdCall, MdStore, MdReport, MdShare, MdClose, MdEdit, MdDelete, MdAdd } from 'react-icons/md'
import { parseDistance, parsePrice, parseLikeOrDislikes, parseDate } from '../../tools/extentions'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import copy from 'copy-to-clipboard'
const Compress = require('compress.js')
function AddTopicMessageDialog(props) {
    let history = useHistory()
    const [isLoading, setLoading] = useState(false)

    const isFromLocalScreens = useRef(false)
    const [responseMessage, setResponseMessage] = useState('')
    const [isShowingResponse, setShowingResponse] = useState('')
    const [responseColor, setResponseColor] = useState(greenColor)

    const [message, setMessage] = useState('')

    const [imageFile, setImageFile] = useState()
    const [imagePreview, setImagePreview] = useState()
    const topicId = useRef(0)

    const [replyMessage, setReplyMessage] = useState(null)

    useEffect(() => {
        topicId.current = props.data.topicId
        setMessage(localStorage.getItem('gmm8371221') ?? '')

        if (props.data.isReply == true) {
            // console.log('rghe: ' + props.data.replyMessage.id)
            setReplyMessage(props.data.replyMessage)
        }
    }, [])

    useEffect(() => {
        if (!imageFile) {
            setImagePreview(undefined)
            return
        }

        var objectUrl
        objectUrl = URL.createObjectURL(imageFile)
        setImagePreview(objectUrl)
        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [imageFile])

    const pickAvatarImage = () => {
        document.getElementById('pickImageInput').click()
    }

    const onSelectAatarFile = async (e) => {
        if (!e.target.files || e.target.files.length === 0) {
            setImageFile(undefined)
            return
        }

        if (e.target.files[0].size > 100000) {
            try {
                const compress = new Compress()
                await compress
                    .compress([e.target.files[0]], {
                        size: 0.2,
                        quality: 0.75,
                        maxWidth: 1024,
                        maxHeight: 1024,
                        resize: true,
                    })
                    .then((data) => {
                        let mimeType = 'image/png'
                        const bstr = atob(data[0].data)
                        let n = bstr.length
                        const u8arr = new Uint8Array(n)
                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n)
                        }
                        setImageFile(new File([u8arr], data.alt, { type: mimeType }))
                    })
            } catch (error) {
                //console.log(error)
                notifyErrorMessage('خطا در فشرده سازی تصویر.')
            }
        } else {
            setImageFile(e.target.files[0])
        }
    }

    useEffect(() => {}, [])

    const sendTopicMessage = async () => {
        if (message.length === 0) {
            return
        }
        setLoading(true)
        closeResponseMessage()

        const formData = new FormData()
        formData.append('message', message)
        formData.append('topicId', topicId.current)

        if (replyMessage != null) {
            formData.append('isReply', true)
            formData.append('replyId', replyMessage.id)
            formData.append('replyCreatorId', replyMessage.userId)
            formData.append('replyCreatorName', replyMessage.senderName)
            formData.append('replyMessage', replyMessage.message)
            formData.append('replyImage', replyMessage.image)
        }

        if (imageFile != null) {
            formData.append('image', imageFile)
        }

        await Axios.post('societyMessages', formData)
            .then((response) => {
                setResponseMessage(response.data.message)
                if (response.status == 201) {
                    localStorage.setItem('gmm8371221', '')
                    props.data.onDone()
                }
            })
            .catch((err) => {
                if (err.response.status == 401) {
                    setResponseMessage('لطفا ابتدا وارد حساب کاربری خود شوید.')
                } else {
                    setResponseMessage(err.response.data.message)
                }
                setResponseColor(accentcolor)
                openResponseMessage()
            })

        setLoading(false)
    }

    const onCloseScreen = () => {
        isFromLocalScreens ? props.data.onCloseFunc() : history.goBack()
    }

    const openResponseMessage = () => {
        setShowingResponse(true)
    }

    const closeResponseMessage = () => {
        setShowingResponse(false)
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="sm" aria-labelledby="addressDialog">
            <div>
                {isLoading ? (
                    <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                        <CircularProgress color="white" />
                    </Backdrop>
                ) : (
                    <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 24, paddingRight: 24, paddingLeft: 24, paddingBottom: 16 }}>
                        <p className="PrimaryText">{replyMessage != null ? 'پاسخ پیام' : 'پیام جدید'}</p>
                        <p className="ThirdLightText" style={{ marginLeft: 16, marginTop: 6, textAlign: 'justify' }}>
                            مطابق قانون مسئولیت محتوای منتشر شده در فضای مجازی به عهده ناشر می باشد.
                        </p>

                        {replyMessage != null && (
                            <div style={{ backgroundColor: backgroundColor, padding: 8, marginTop: 8, borderRadius: 8, marginBottom: 8 }}>
                                <p className="ThirdText">{replyMessage.message}</p>
                            </div>
                        )}

                        <TextField label={replyMessage != null ? 'متن پاسخ' : 'متن پیام'} multiline value={message} onChange={(e) => {
                            localStorage.setItem('gmm8371221', e.target.value)
                            setMessage(e.target.value)}} style={{ direction: 'rtl', textAlign: 'right', marginTop: 8, marginBottom: 8 }} />
                        <div className="StackContainer" onClick={pickAvatarImage} style={{ marginTop: 12, marginBottom: 16, width: '100%', height: 200 }}>
                            <MdAdd size={24} color="black" className="StackElement" style={{ backgroundColor: 'white', borderBottomLeftRadius: 4 }} />
                            <input id="pickImageInput" type="file" hidden onChange={onSelectAatarFile} />
                            <img src={imagePreview} className="image" style={{ borderRadius: 0, width: '100%', height: 200 }} />
                        </div>
                        <Button variant="contained" onClick={sendTopicMessage} fullWidth={true} style={{ backgroundColor: blueColor, color: 'white' }}>
                            ثبت
                        </Button>
                        <div style={{ height: isShowingResponse ? 'auto' : 0, display: 'flex', alignItems: 'center', backgroundColor: responseColor, marginTop: 8, paddingRight: 8, paddingLeft: 8, visibility: isShowingResponse ? 'visible' : 'hidden' }}>
                            <div style={{ width: 20, height: 20 }}>
                                <MdClose size={20} color="white" onClick={closeResponseMessage} />
                            </div>
                            <p className="ThirdBoldText" style={{ color: 'white', padding: 8 }}>
                                {responseMessage}
                            </p>
                        </div>
                    </div>
                )}
            </div>
        </Dialog>
    )
}

export default AddTopicMessageDialog
