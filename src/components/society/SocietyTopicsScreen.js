import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    DialogTitle,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    Popover,
    Card,
    InputBase,
} from '@material-ui/core'
import { MdAdd, MdArrowBack, MdArrowForward, MdBookmarkBorder, MdClose, MdEdit, MdExitToApp, MdKeyboardArrowLeft, MdMoreVert, MdOpenInNew, MdSearch } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { backgroundColor, greenColor, token, account, notifyWarnMessage, webUrl, accentcolor, notifyErrorMessage } from '../../App'
import { parsePrice, parseDate, parseNumber } from '../../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../../widgets/EmptyView'
import { isIOS, isAndroid } from 'react-device-detect'

import ArroUpIcon from '../../icons/up-arrow.png'
import ArroDownIcon from '../../icons/down-arrow.png'
import ViewIcon from '../../icons/view.png'
import TitleIcon from '../../icons/title.png'
import ChatIcon from '../../icons/chat.png'
import ProfileScreen from '../ProfileScreen'
import ArroUpOutlineIcon from '../../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../../icons/down-arrow-outline.png'
import AddTopicDialog from './AddTopicDialog'
import LoadingDialog from '../dialogs/LoadingDialog'
import ReactTextFormat from 'react-text-format';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function SocietyTopicsScreen(props) {

    let history = useHistory()
    const classes = useStyles()

    const page = useRef(1)
    const catId = useRef(0)
    const sortType = useRef(0)
    const wordToFind = useRef('')
    const [wordToFindShow, setWordToFindShow] = useState('')
    const [headerTitle, setHeaderTitle] = useState('در حال بارگزاری')

    const [isLoading, setLoading] = useState(true)
    const isAllGot = useRef(false)
    const [topics, setTopics] = useState([])

    useEffect(() => {
        const search = props.location.search
        const params = new URLSearchParams(search)
        const id = params.get('id')
        catId.current = id
        if (props.location.data == null) {
            // catId.current = localStorage.getItem('ca-id-tps-i97')
            wordToFind.current = localStorage.getItem('ca-nam-sea-tps-i9')
            sortType.current = localStorage.getItem('jg38f2ow389')
            page.current = localStorage.getItem('tps-pa-oi90')
            setTopics(JSON.parse(localStorage.getItem('tps-reusable-78y')))
            setHeaderTitle(localStorage.getItem('ghwo-883-3ff-gg'))
            setLoading(false)
            try {
                setTimeout(() => {
                    window.scrollTo({ top: localStorage.getItem('cat-tps-sc-re-y8') > 100 ? localStorage.getItem('cat-tps-sc-re-y8') : 0 })
                }, 100)
            } catch (e) { }
        } else {
            setHeaderTitle(props.location.data.title)
            localStorage.setItem('ghwo-883-3ff-gg', props.location.data.title)
            localStorage.setItem('ca-id-tps-i97', props.location.catId)
            localStorage.setItem('ca-nam-sea-tps-i9', '')
            localStorage.setItem('jg38f2ow389', 0)
            localStorage.setItem('tps-pa-oi90', 1)
            localStorage.setItem('tps-reusable-78y', '[]')
            getTopics()
        }
        return () => {
            // Anything in here is fired on component unmount.
            window.onscroll = ''
        }
    }, [])

    const getTopics = async () => {
        if (isAllGot.current) {
            return
        }
        setLoading(true)
        try {
            const response = await Axios.post(
                'societyTopics/list',
                {
                    catId: catId.current,
                    orderType: sortType.current,
                    page: page.current,
                    word: wordToFind.current,
                },
                { headers: { token: token } }
            )

            // console.log(response)
            if (response.data.topics instanceof Array && response.data.topics.length > 0) {
                setHeaderTitle(response.data.category.title)
                topics.push(...response.data.topics)
                setTopics(topics)
                ++page.current
                localStorage.setItem('tps-reusable-78y', JSON.stringify(topics))
                localStorage.setItem('tps-pa-oi90', page.current)
                localStorage.setItem('ghwo-883-3ff-gg', response.data.category.title)
            } else {
                isAllGot.current = true
            }

            setLoading(false)
        } catch (error) {
            //console.log('err filters :: ', error)
            setLoading(false)
        }
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        localStorage.setItem('cat-tps-sc-re-y8', window.pageYOffset)
        try {
            if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridTopics').offsetHeight) {
                isWorkingScroll.current = 1
                getTopics()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleClickOpenPopover = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClosePopover = () => {
        setAnchorEl(null)
    }

    const openPopover = Boolean(anchorEl)
    const popoverId = openPopover ? 'simple-popover' : undefined

    const [sortTypeTitle, setSortTypeTitle] = useState('جدیدترین')
    const sortTypes = ['جدیدترین', 'قدیمی ترین', 'بالاترین']

    // handle showing profile dialog screen
    const [selectedUserId, setSelectedUserId] = useState(0)
    const [isShowingProfileDialog, setShowingProfileDialog] = useState(false)
    const showProfileScreen = (isShowing) => {
        if (isShowing) {
            return <ProfileScreen data={{ id: selectedUserId, onCloseFunc: closeProfileScreen }} />
        } else {
            return <div />
        }
    }

    const closeProfileScreen = () => {
        setShowingProfileDialog(false)
    }
    // end of profile dialog screen

    const [upDownLoadingId, setUpDownLoadingId] = useState(0)
    const upOrDown = async (index, id, isUp, isForgive) => {
        if (localStorage.getItem('token') == null) {
            history.push('/login')
            return
        }

        setUpDownLoadingId(id)
        try {
            const response = await Axios.post('societyUpAndDowns', {
                isUp,
                isForgive,
                contentId: id,
                contentType: 1,
            })
            if (response.status == 201) {
                if (isUp == true) {
                    if (topics[index].iDown != null && topics[index].iDown == true) {
                        topics[index].iDown = false
                        topics[index].downs -= 1
                    }
                } else {
                    if (topics[index].iUp != null && topics[index].iUp == true) {
                        topics[index].iUp = false
                        topics[index].ups -= 1
                    }
                }

                if (isUp == true) {
                    topics[index].iUp = isForgive == false
                } else {
                    topics[index].iDown = isForgive == false
                }

                if (isUp == true) {
                    if (isForgive == false) {
                        topics[index].ups += 1
                    } else {
                        topics[index].ups -= 1
                    }
                } else {
                    if (isForgive == false) {
                        topics[index].downs += 1
                    } else {
                        topics[index].downs -= 1
                    }
                }
                setTopics(topics)
            } else {
                notifyErrorMessage('نشد که بشه... لطفا دوباره امتحان کنید.')
            }
        } catch (error) { }
        setUpDownLoadingId(0)
    }

    // handle add topic dialog ::::::::
    const [isShowingAddTopicDialog, setShowingAddTopicDialog] = useState(false)
    const showChooseAddEditDialog = (isShowing) => {
        console.log('gbw:: ' + catId.current)
        if (isShowing) {
            return (
                <AddTopicDialog
                    data={{
                        onCloseFunc: closeChooseAddTopicDialog,
                        onDone: reloadList,
                        catId: catId.current,
                    }}
                />
            )
        } else {
            return <div />
        }
    }

    const closeChooseAddTopicDialog = () => {
        setShowingAddTopicDialog(false)
    }
    // end handle add topic dialog ----------

    const [isInSearchMode, setIsInSearchMode] = useState(false)

    const reloadList = () => {
        setShowingAddTopicDialog(false)
        page.current = 1
        isAllGot.current = false
        setTopics([])
        topics.length = 0
        getTopics()
    }

    return (
        <div className={classes.root} style={{ overflow: 'hidden' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography className="single-line-truncate" variant="h6" color="inherit">
                        {headerTitle}
                    </Typography>
                    <section style={{ marginRight: 'auto', marginLeft: 0, paddingLeft: 0, display: 'flex', alignItems: 'center' }}>
                        <IconButton aria-describedby={popoverId} onClick={handleClickOpenPopover}>
                            <p className="ThirdBoldText">{sortTypeTitle}</p>
                        </IconButton>
                    </section>
                </Toolbar>
            </AppBar>
            {showChooseAddEditDialog(isShowingAddTopicDialog)}
            {showProfileScreen(isShowingProfileDialog)}

            <div style={{ marginTop: isAndroid || isIOS ? 38 : 48, paddingTop: 12, paddingRight: isAndroid || isIOS ? 0 : 12, paddingLeft: isAndroid || isIOS ? 0 : 12, paddingBottom: isAndroid || isIOS ? 8 : 4, backgroundColor: backgroundColor }}>
                <Card style={{ borderRadius: isAndroid || isIOS ? 0 : 4 }}>
                    <div style={{ display: 'flex', alignItems: 'center' }}>
                        <InputBase
                            placeholder="جستجو"
                            onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                    setIsInSearchMode(true)
                                    page.current = 1
                                    isAllGot.current = false
                                    setTopics([])
                                    topics.length = 0
                                    setTimeout(() => {
                                        getTopics()
                                    }, 100)
                                }
                            }}
                            value={wordToFindShow}
                            style={{ width: '100%', paddingTop: 6, paddingRight: 8, paddingLeft: 8 }}
                            onChange={(e) => {
                                localStorage.setItem('ca-nam-sea-tps-i9', e.target.value)
                                wordToFind.current = e.target.value
                                setWordToFindShow(e.target.value)
                            }}
                        />
                        <IconButton
                            onClick={() => {
                                if (isInSearchMode) {
                                    setWordToFindShow('')
                                    wordToFind.current = ''
                                }
                                setIsInSearchMode(!isInSearchMode)
                                page.current = 1
                                isAllGot.current = false
                                setTopics([])
                                topics.length = 0
                                setTimeout(() => {
                                    getTopics()
                                }, 100)
                            }}
                        >
                            {isInSearchMode ? <MdClose color="red" /> : <MdSearch />}
                        </IconButton>
                    </div>
                </Card>
            </div>

            <Popover
                id={popoverId}
                open={openPopover}
                anchorEl={anchorEl}
                onClose={handleClosePopover}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <div style={{ padding: 4, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', textAlign: 'right' }}>
                    {sortTypes.map((type, index) => {
                        return (
                            <div
                                onClick={() => {
                                    handleClosePopover()
                                    setSortTypeTitle(type)
                                    setTopics([])
                                    topics.length = 0
                                    isAllGot.current = false
                                    page.current = 1
                                    sortType.current = index
                                    localStorage.setItem(index, 'jg38f2ow389')
                                    setTimeout(() => {
                                        getTopics()
                                    }, 100)
                                }}
                                style={{ display: 'flex', alignItems: 'center', paddingRight: 8, paddingLeft: 8 }}
                            >
                                <Button variant="text">{type}</Button>
                            </div>
                        )
                    })}
                </div>
            </Popover>
            {topics.length == 0 && !isLoading ? (
                <EmptyView />
            ) : (
                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%', backgroundColor: backgroundColor }}>
                    <div style={{ paddingTop: 0, width: '100%', overflow: 'hidden' }}>
                        <div
                            className="masonry-container"
                            id="gridTopics"
                            style={{
                                width: '100%',
                                display: 'inline-block',
                                alignContent: 'center',
                                justifyContent: 'center',
                                minHeight: props.height,
                                columnCount: isAndroid || isIOS ? 1 : Math.round(props.width / 300),
                                paddingRight: !isAndroid && !isIOS ? 12 : 0,
                                paddingLeft: !isAndroid && !isIOS ? 12 : 0,
                            }}
                        >
                            {topics.map((item, index) => (
                                <Card
                                    className={'masonry-item clickable ' + (!isAndroid && !isIOS ? 'cardHover' : '')}
                                    key={item.id}
                                    variant="elevation"
                                    elevation={3}
                                    style={{
                                        width: isAndroid || isIOS ? '100%' : props.width / (props.width / 300) - 16,
                                        height: '100%',
                                        marginTop: 8,
                                        borderRadius: isAndroid || isIOS ? 0 : 4,
                                        marginRight: isAndroid || isIOS ? '16px !important' : 0,
                                        marginLeft: isAndroid || isIOS ? '16px !important' : 0,
                                        marginBottom: 8,
                                        padding: 0,
                                        position: 'relative',
                                        boxShadow: '0 1px 3px 0px #000000',
                                    }}
                                    onClick={() => {
                                        history.push({ pathname: '/society/topics/messages', search: '?id=' + item.id, data: { title: item.title } })
                                    }}
                                >
                                    {item.banner != null && item.banner.length > 0 && <img src={webUrl + item.banner} style={{ width: '100%', maxHeight: 300, marginTop: -2, borderTopLeftRadius: 4, borderTopRightRadius: 4, objectFit: 'cover' }} />}
                                    <div style={{ marginTop: item.banner != null && item.banner.length > 0 ? 4 : 12 }}>
                                        <div style={{ fontFamily: 'iransansLight', fontSize: 13, marginRight: 12, marginLeft: 12 }}>
                                            <h1 className="PrimaryText" style={{ marginTop: isAndroid || isIOS ? 12 : 8 }}>
                                                {item.title}
                                            </h1>
                                            <ReactTextFormat className="multi-line-truncate" style={{ margin: 0, marginTop: 8, padding: 0, color: '#CECECE', textAlign: 'justify' }}>
                                                {item.description}
                                            </ReactTextFormat>

                                            <div style={{ display: 'flex', alignItems: 'center', direction: 'ltr', marginTop: 12, marginBottom: 4 }}>
                                                {upDownLoadingId != item.id ? (
                                                    <img
                                                        className="clickHoverJustTooZoom"
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            upOrDown(index, item.id, false, item.iDown != null && item.iDown == true)
                                                        }}
                                                        src={item.iDown && item.iDown == true ? ArroDownIcon : ArroDownOutlineIcon}
                                                        style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16 }}
                                                    />
                                                ) : (
                                                    <CircularProgress size={16} style={{ marginRight: 4 }} color="secondary" />
                                                )}
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.downs)}
                                                </p>
                                                {upDownLoadingId != item.id ? (
                                                    <img
                                                        className="clickHoverJustTooZoom"
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            upOrDown(index, item.id, true, item.iUp != null && item.iUp == true)
                                                        }}
                                                        src={item.iUp && item.iUp == true ? ArroUpIcon : ArroUpOutlineIcon}
                                                        style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }}
                                                    />
                                                ) : (
                                                    <CircularProgress size={16} style={{ marginRight: 4, marginLeft: 4 }} color="secondary" />
                                                )}
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.ups)}
                                                </p>

                                                <img src={ViewIcon} style={{ colorAdjust: 'red', opacity: 0.5, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.seenCount)}
                                                </p>

                                                <img src={ChatIcon} style={{ colorAdjust: 'red', opacity: 0.5, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.messagesCount)}
                                                </p>
                                            </div>
                                            <Divider style={{ marginTop: 10, marginBottom: -2 }} />
                                        </div>
                                        <div
                                            className="darkHover"
                                            style={{ display: 'flex', alignItems: 'center', paddingTop: 10, paddingBottom: 10, paddingRight: 12, paddingLeft: 12 }}
                                            onClick={(e) => {
                                                e.stopPropagation()
                                                setSelectedUserId(item.makerId)
                                                localStorage.setItem('filterUserId', 'uid-' + item.makerId)
                                                // setShowingProfileDialog(true)
                                                history.push({ pathname: 'user', search: '?id=' + item.makerId })
                                            }}
                                        >
                                            <img src={webUrl + item.makerAvatar} style={{ width: 50, height: 50, borderRadius: 25, objectFit: 'cover' }} />
                                            <div style={{ width: '100%', marginRight: 8 }}>
                                                <p style={{ fontSize: 12, fontFamily: 'iransansMedium' }}>{item.makerName}</p>
                                                <p className="ThirdLightText">
                                                    {item.makerCity} - {item.makerJob}
                                                </p>
                                            </div>
                                            <MdKeyboardArrowLeft size={24} color="#888888" />
                                        </div>
                                    </div>
                                </Card>
                            ))}
                        </div>
                    </div>
                </div>
            )}
            {topics.length == 0 && isLoading ? (
                <LoadingDialog />
            ) : (
                <div />
            )}
            {isLoading && topics.length > 0 ? (
                <div style={{ position: 'fixed', width: '100%', bottom: 0 }}>
                    <div style={{ width: '100%' }}>
                        <LinearProgress color="secondary" />
                    </div>
                </div>
            ) : (
                <div></div>
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
            <div style={{ position: 'fixed', bottom: 16, left: 16 }}>
                <Fab
                    color="secondary"
                    aria-label="addTopic"
                    onClick={() => {
                        if (localStorage.getItem('token') == null) {
                            history.push('/login')
                            return
                        } else if (localStorage.getItem('account') == null || JSON.parse(localStorage.getItem('account')).name == null) {
                            notifyWarnMessage('لطفا اطلاعات حساب کاربری خود را از منوی برنامه تکمیل نمایید.')
                            return
                        } else {
                            setShowingAddTopicDialog(true)
                        }
                    }}
                >
                    <MdAdd size={24} />
                </Fab>
            </div>
        </div>
    )
}

export default SocietyTopicsScreen
