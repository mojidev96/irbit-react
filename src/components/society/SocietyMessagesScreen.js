import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    DialogTitle,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    Popover,
    Card,
} from '@material-ui/core'
import { MdAccountBox, MdAccountCircle, MdAdd, MdArrowBack, MdArrowForward, MdBookmarkBorder, MdDelete, MdEdit, MdExitToApp, MdKeyboardArrowLeft, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { backgroundColor, greenColor, token, account, notifyWarnMessage, webUrl, accentcolor, notifyErrorMessage, itemColor, notifySuccessful } from '../../App'
import { parsePrice, parseDate, parseNumber, parseJustDate } from '../../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../../widgets/EmptyView'
import { isIOS, isAndroid } from 'react-device-detect'
import ArroUpIcon from '../../icons/up-arrow.png'
import ArroDownIcon from '../../icons/down-arrow.png'
import ArroUpOutlineIcon from '../../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../../icons/down-arrow-outline.png'
import ViewIcon from '../../icons/view.png'
import TitleIcon from '../../icons/title.png'
import ChatIcon from '../../icons/chat.png'
import ProfileScreen from '../ProfileScreen'
import AddTopicMessageDialog from './AddTopicMessageDialog'
import ReactTextFormat from 'react-text-format';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function SocietyMessagesScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const page = useRef(1)
    const topicId = useRef(0)
    const sortType = useRef(0)

    const [headerTitle, setHeaderTitle] = useState('در حال بارگزاری')

    const [isLoading, setLoading] = useState(true)
    const isAllGot = useRef(false)
    const messages = useRef([])
    const [topic, setTopic] = useState(null)

    useEffect(() => {
        const search = props.location.search
        const params = new URLSearchParams(search)
        const id = params.get('id')
        topicId.current = id
        // if (props.location.data == null) {
        //     // topicId.current = localStorage.getItem('ca-id-tps-i972344')
        //     sortType.current = localStorage.getItem('jg38f2ow389355')
        //     page.current = localStorage.getItem('tps-pa-oi906332')
        //     setTopic(JSON.parse(localStorage.getItem('tps-reusable-gn39bvnfh')))
        //     setMessages(JSON.parse(localStorage.getItem('tps-reusable-78y5563')))
        //     setHeaderTitle(localStorage.getItem('ghwo-883-3ff-gg3523'))
        //     setLoading(false)
        //     try {
        //         setTimeout(() => {
        //             window.scrollTo({ top: localStorage.getItem('cat-tps-sc-re-y845334') < 100 ? 0 : localStorage.getItem('cat-tps-sc-re-y845334') })
        //         }, 100)
        //     } catch (e) {}
        // } else {
        // setHeaderTitle(props.location.data.title)
        // localStorage.setItem('ghwo-883-3ff-gg3523', props.location.data.title)
        // localStorage.setItem('ca-id-tps-i972344', props.location.catId)
        // localStorage.setItem('tps-reusable-gn39bvnfh', '{}')
        // localStorage.setItem('jg38f2ow389355', 0)
        // localStorage.setItem('tps-pa-oi906332', 1)
        // localStorage.setItem('tps-reusable-78y5563', '[]')
        getTopics()
        // }
        return () => {
            // Anything in here is fired on component unmount.
            window.onscroll = ''
        }
    }, [])

    const getTopics = async () => {
        if (isAllGot.current) {
            return
        }
        setLoading(true)
        try {
            const response = await Axios.post(
                'societyTopics/byId',
                {
                    id: topicId.current,
                    orderType: sortType.current,
                    page: page.current,
                },
                { headers: { token: token } }
            )

            setTopic(response.data.topic)
            setHeaderTitle(response.data.topic.title)
            localStorage.setItem('tps-reusable-gn39bvnfh', JSON.stringify(response.data.topic))
            localStorage.setItem('ghwo-883-3ff-gg3523', response.data.topic.title)

            if (response.data.messages instanceof Array && response.data.messages.length > 0) {
                messages.current.push(...response.data.messages)
                ++page.current
                localStorage.setItem('tps-reusable-78y5563', JSON.stringify(messages.current))
                localStorage.setItem('tps-pa-oi906332', page.current)
            } else {
                isAllGot.current = true
            }

            setLoading(false)
        } catch (error) {
            //console.log('err filters :: ', error)
            setLoading(false)
        }
    }

    const deleteMessage = async (id, index) => {
        setLoading(true)

        try {
            const response = await Axios.delete('societyMessages', {
                data: { id }
            })

            if (response.status == 200) {
                messages.current.splice(index, 1)
                notifySuccessful()
            } else {
                notifyErrorMessage(response.data.message ?? 'خطای نامشخص')
            }
        } catch (error) {
            notifyErrorMessage(error.response.data.message ?? 'خطای نامشخص')
        }
        setLoading(false)
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        localStorage.setItem('cat-tps-sc-re-y845334', window.pageYOffset)
        try {
            if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridMessages').offsetHeight) {
                isWorkingScroll.current = 1
                getTopics()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleClickOpenPopover = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClosePopover = () => {
        setAnchorEl(null)
    }

    const openPopover = Boolean(anchorEl)
    const popoverId = openPopover ? 'simple-popover' : undefined

    const [sortTypeTitle, setSortTypeTitle] = useState('جدیدترین')
    const sortTypes = ['جدیدترین', 'قدیمی ترین', 'بالاترین']

    // handle showing profile dialog screen
    const [selectedUserId, setSelectedUserId] = useState(0)
    const [isShowingProfileDialog, setShowingProfileDialog] = useState(false)
    const showProfileScreen = (isShowing) => {
        if (isShowing) {
            return <ProfileScreen data={{ id: selectedUserId, onCloseFunc: closeProfileScreen }} />
        } else {
            return <div />
        }
    }

    const closeProfileScreen = () => {
        setShowingProfileDialog(false)
    }
    // end of profile dialog screen

    const [upDownLoadingId, setUpDownLoadingId] = useState(0)
    const upOrDown = async (index, id, isUp, isForgive) => {
        if (localStorage.getItem('token') == null) {
            history.push('/login')
            return
        }
        setUpDownLoadingId(id)
        try {
            const response = await Axios.post('societyUpAndDowns', {
                isUp,
                isForgive,
                contentId: id,
                contentType: 2,
            })
            if (response.status == 201) {
                if (isUp == true) {
                    if (messages.current[index].iDown != null && messages.current[index].iDown == true) {
                        messages.current[index].iDown = false
                        messages.current[index].downs -= 1
                    }
                } else {
                    if (messages.current[index].iUp != null && messages.current[index].iUp == true) {
                        messages.current[index].iUp = false
                        messages.current[index].ups -= 1
                    }
                }

                if (isUp == true) {
                    messages.current[index].iUp = isForgive == false
                } else {
                    messages.current[index].iDown = isForgive == false
                }

                if (isUp == true) {
                    if (isForgive == false) {
                        messages.current[index].ups += 1
                    } else {
                        messages.current[index].ups -= 1
                    }
                } else {
                    if (isForgive == false) {
                        messages.current[index].downs += 1
                    } else {
                        messages.current[index].downs -= 1
                    }
                }
            } else {
                notifyErrorMessage('نشد که بشه... لطفا دوباره امتحان کنید.')
            }
        } catch (error) { }
        setUpDownLoadingId(0)
    }

    // handle add message dialog ::::::::
    const [isShowingAddMessageDialog, setShowingAddMessageDialog] = useState(false)
    const [isReplyMessage, setIsReplyMessage] = useState(false)
    const [replyMessage, setReplyMessage] = useState(null)
    const showChooseAddEditDialog = (isShowing) => {
        if (isShowing) {
            return (
                <AddTopicMessageDialog
                    data={{
                        onCloseFunc: closeChooseAddTopicDialog,
                        onDone: reloadList,
                        topicId: topicId.current,
                        isReply: isReplyMessage,
                        replyMessage,
                    }}
                />
            )
        } else {
            return <div />
        }
    }

    const closeChooseAddTopicDialog = () => {
        setShowingAddMessageDialog(false)
    }
    // end handle add message dialog ----------

    const reloadList = () => {
        setShowingAddMessageDialog(false)
        page.current = 1
        isAllGot.current = false
        messages.current = []
        messages.current.length = 0
        getTopics()
    }

    return (
        <div className={classes.root} style={{ overflow: 'hidden', display: 'flex', justifyContent: 'center', backgroundColor: backgroundColor }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography className="single-line-truncate" variant="h6" color="inherit">
                        {headerTitle}
                    </Typography>
                    <section style={{ marginRight: 'auto', marginLeft: 0, paddingLeft: 0, display: 'flex', alignItems: 'center' }}>
                        <IconButton aria-describedby={popoverId} onClick={handleClickOpenPopover}>
                            <p className="ThirdBoldText">{sortTypeTitle}</p>
                        </IconButton>
                    </section>
                </Toolbar>
            </AppBar>
            {showChooseAddEditDialog(isShowingAddMessageDialog)}
            {showProfileScreen(isShowingProfileDialog)}
            <Popover
                id={popoverId}
                open={openPopover}
                anchorEl={anchorEl}
                onClose={handleClosePopover}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <div style={{ padding: 4, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', textAlign: 'right' }}>
                    {sortTypes.map((type, index) => {
                        return (
                            <div
                                onClick={() => {
                                    handleClosePopover()
                                    setSortTypeTitle(type)
                                    messages.current = []
                                    messages.current.length = 0
                                    isAllGot.current = false
                                    page.current = 1
                                    sortType.current = index
                                    localStorage.setItem(index, 'jg38f2ow389355')
                                    setTimeout(() => {
                                        getTopics()
                                    }, 100)
                                }}
                                style={{ display: 'flex', alignItems: 'center', paddingRight: 8, paddingLeft: 8 }}
                            >
                                <Button variant="text">{type}</Button>
                            </div>
                        )
                    })}
                </div>
            </Popover>

            <div className="StackContainer" style={{ overflow: 'hidden', width: '100%', maxWidth: 700, minHeight: props.height }}>
                <div style={{ paddingTop: 56, width: '100%', overflow: 'hidden' }}>
                    {topic != null && (
                        <div onClick={(e) => e.stopPropagation()} className="SecondaryText" style={{ whiteSpace: 'pre-line', paddingTop: 0, marginRight: 16, marginLeft: 16, marginTop: 8, marginBottom: 8 }}>
                            {topic.banner != null && <img className="image" src={webUrl + topic.banner} style={{ width: '100%', maxHeight: 400 }} />}
                            <ReactTextFormat style={{ marginTop: 8 }}>
                                {topic.description}
                            </ReactTextFormat>
                            <p className="ThirdLightText">{parseDate(topic.dateCreate)}</p>
                        </div>
                    )}
                    <GridList cellHeight="auto" id="gridMessages" cols={1} style={{ justifyContent: 'center' }}>
                        {messages.current.map((item, index) => (
                            <Card
                                className={'clickable ' + (!isAndroid && !isIOS ? 'cardHover' : '')}
                                key={item.id}
                                variant="elevation"
                                elevation={3}
                                style={{
                                    width: '100%',
                                    height: '100%',
                                    marginTop: 8,
                                    marginRight: isAndroid || isIOS ? 0 : 14,
                                    marginLeft: isAndroid || isIOS ? 0 : 14,
                                    marginBottom: 8,
                                    padding: 0,
                                    position: 'relative',
                                    boxShadow: '0 1px 3px 0px #000000',
                                    borderRadius: isAndroid || isIOS ? 0 : 4,
                                }}
                                onClick={() => {
                                    setReplyMessage(item)
                                    setIsReplyMessage(true)
                                    setShowingAddMessageDialog(true)
                                }}
                            >
                                {item.replyId != null && (
                                    <div style={{ display: 'flex', alignItems: 'center', backgroundColor: backgroundColor, margin: 8, borderRadius: 4, padding: 8 }}>
                                        {item.replyImage != null && <img src={webUrl + item.replyImage} style={{ maxWidth: 100, maxHeight: 100, borderRadius: 4, marginLeft: 8 }} />}
                                        <div>
                                            <div
                                                style={{ display: 'flex', alignItems: 'center', marginBottom: 4 }}
                                                onClick={(e) => {
                                                    e.stopPropagation()
                                                    setSelectedUserId(item.userId)
                                                    localStorage.setItem('filterUserId', 'uid-' + item.userId)
                                                    // setShowingProfileDialog(true)
                                                    history.push({ pathname: 'user', search: '?id=' + item.userId })
                                                }}
                                            >
                                                <MdAccountCircle size={16} style={{ opacity: 0.7, marginLeft: 4 }} />
                                                <p className="ThirdBoldText">{item.replyCreatorName} :</p>
                                            </div>
                                            <p className="ThirdText" style={{ marginRight: 20 }}>
                                                {item.replyMessage}
                                            </p>
                                        </div>
                                    </div>
                                )}

                                {item.image != null && item.image.length > 0 && <img src={webUrl + item.image} style={{ width: '100%', maxHeight: 200, marginTop: -2, borderTopLeftRadius: 4, borderTopRightRadius: 4, objectFit: 'contain' }} />}
                                <div style={{ marginTop: item.image != null && item.image.length > 0 ? 4 : 12 }}>
                                    <div style={{ marginRight: 12, marginLeft: 12 }}>
                                        <h1 className="SecondaryText">{item.message}</h1>
                                        <div style={{ display: 'flex', alignItems: 'center', direction: 'ltr', marginTop: 12, marginBottom: 4 }}>
                                            {account && account.id && item.userId == account.id && <MdDelete size={24} onClick={(e) => {
                                                e.stopPropagation()
                                                deleteMessage(item.id, index)
                                            }} style={{ color: backgroundColor, marginRight: 8 }} />}
                                            {upDownLoadingId != item.id ? (
                                                <img
                                                    className="clickHoverJustTooZoom"
                                                    onClick={(e) => {
                                                        e.stopPropagation()
                                                        upOrDown(index, item.id, false, item.iDown != null && item.iDown == true)
                                                    }}
                                                    src={item.iDown && item.iDown == true ? ArroDownIcon : ArroDownOutlineIcon}
                                                    style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16 }}
                                                />
                                            ) : (
                                                <CircularProgress size={16} style={{ marginRight: 4 }} color="secondary" />
                                            )}
                                            <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                {parseNumber(item.downs)}
                                            </p>

                                            {upDownLoadingId != item.id ? (
                                                <img
                                                    className="clickHoverJustTooZoom"
                                                    onClick={(e) => {
                                                        e.stopPropagation()
                                                        upOrDown(index, item.id, true, item.iUp != null && item.iUp == true)
                                                    }}
                                                    src={item.iUp && item.iUp == true ? ArroUpIcon : ArroUpOutlineIcon}
                                                    style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }}
                                                />
                                            ) : (
                                                <CircularProgress size={16} style={{ marginRight: 4, marginLeft: 4 }} color="secondary" />
                                            )}
                                            <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                {parseNumber(item.ups)}
                                            </p>

                                            <p className="ThirdLightText" style={{ marginLeft: 12, textAlign: 'right', direction: 'rtl', opacity: 0.7 }}>
                                                {parseDate(item.dateCreate)}
                                            </p>

                                        </div>

                                        <Divider style={{ marginTop: 10, marginBottom: -2 }} />
                                    </div>
                                    <div
                                        className="darkHover"
                                        style={{ display: 'flex', alignItems: 'center', paddingTop: 10, paddingBottom: 10, paddingRight: 12, paddingLeft: 12 }}
                                        onClick={(e) => {
                                            e.stopPropagation()
                                            setSelectedUserId(item.userId)
                                            localStorage.setItem('filterUserId', 'uid-' + item.userId)
                                            // setShowingProfileDialog(true)
                                            history.push({ pathname: '/user', search: '?id=' + item.userId })
                                        }}
                                    >
                                        <img src={webUrl + item.senderAvatar} style={{ width: 50, height: 50, borderRadius: 25, objectFit: 'cover' }} />
                                        <div style={{ width: '100%', marginRight: 8 }}>
                                            <p style={{ fontSize: 12, fontFamily: 'iransansMedium' }}>{item.senderName}</p>
                                            <p className="ThirdLightText">
                                                {item.senderCity} - {item.senderJob}
                                            </p>
                                        </div>
                                        <MdKeyboardArrowLeft size={24} color="#888888" />
                                    </div>
                                </div>
                            </Card>
                        ))}
                    </GridList>
                </div>
            </div>
            {messages.current.length == 0 && isLoading ? (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            ) : (
                <div />
            )}
            {isLoading && messages.current.length > 0 ? (
                <div style={{ position: 'fixed', width: '100%', bottom: 0 }}>
                    <div style={{ width: '100%' }}>
                        <LinearProgress color="secondary" />
                    </div>
                </div>
            ) : (
                <div></div>
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
            <div style={{ position: 'fixed', bottom: 16, left: 16 }}>
                <Fab
                    color="secondary"
                    aria-label="addMessage"
                    onClick={() => {
                        if (localStorage.getItem('token') == null) {
                            history.push('/login')
                            return
                        } else if (localStorage.getItem('account') == null || JSON.parse(localStorage.getItem('account')).name == null) {
                            notifyWarnMessage('لطفا اطلاعات حساب کاربری خود را از منوی برنامه تکمیل نمایید.')
                            return
                        } else {
                            setReplyMessage(null)
                            setIsReplyMessage(false)
                            setShowingAddMessageDialog(true)
                        }
                    }}
                >
                    <MdAdd size={24} />
                </Fab>
            </div>
        </div>
    )
}

export default SocietyMessagesScreen
