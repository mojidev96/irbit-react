import React, { useState, useEffect } from 'react'
import { AppBar, Toolbar, Typography, IconButton, TextField, Checkbox, Backdrop, FormControl, RadioGroup, Radio, FormControlLabel, Button, CircularProgress, Grid } from '@material-ui/core'
import '../App.css'
import Axios from 'axios'
import { webUrl, notifySuccessful, notifyError, greenColor, blueColor, token, notifyErrorMessage, account } from '../App'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import ConfirmationDialog from './dialogs/ConfirmationDialog'
import TryAgainWidget from '../widgets/TryAgainWidget'
import LoginNeededView from '../widgets/LoginNeededView'
import loadImage from 'blueimp-load-image'
import { useRef } from 'react'
import LoadingDialog from './dialogs/LoadingDialog'
import AvatarPNG from '../icons/avatar.png'
import AboutUsDialog from './dialogs/AboutUsDialog'
import NewPostDialog from './dialogs/NewPostDialog'
const Compress = require('compress.js')


function AccountScreen() {
    let history = useHistory()
    const [isAccessibleLocation, setAccessLocation] = useState(false)
    const [isAccessiblePhone, setAccesPhone] = useState(false)
    const [isHideMesNotif, setHideMesNotif] = useState(false)
    const [isShowingLastOnline, setShowingLastOnline] = useState(false)
    const [isShowingOnCongenial, setShowOnCongenial] = useState(false)
    const [isShowOnCustomers, setShowOnCustomers] = useState(false)

    const [itHasProblem, setItHasProblem] = useState(false)

    const [isLoading, setLoading] = useState(true)
    const [data, setData] = useState()

    const [sexValue, setSexValue] = useState('1')

    const changeDistanceCB = (event) => {
        setAccessLocation(event.target.checked)
    }
    const changePhoneCB = (event) => {
        setAccesPhone(event.target.checked)
    }
    const changeNotifContentCB = (event) => {
        setHideMesNotif(event.target.checked)
    }
    const changeLastOnlineCB = (event) => {
        setShowingLastOnline(event.target.checked)
    }
    const changeCongenialCB = (event) => {
        setShowOnCongenial(event.target.checked)
    }
    const changeCustomerCB = (event) => {
        setShowOnCustomers(event.target.checked)
    }

    const handleSexChange = (event) => {
        setSexValue(event.target.value)
    }

    const [name, setName] = useState('')
    const [username, setUsername] = useState('')
    const [job, setJob] = useState('')
    const [about, setAbout] = useState('')
    const [email, setEmail] = useState('')
    const [city, setCity] = useState('')

    const [followersCount, setFollowersCount] = useState(0)
    const [followingCount, setFollowingCount] = useState(0)
    const [postsCount, setPostsCount] = useState(0)
    // const [district, setDistrict] = useState('')

    const getMyProfile = async () => {
        setLoading(true)
        setItHasProblem(false)
        try {
            const response = await Axios.get('users')
            setData(response.data)
            setName(response.data.user.name ?? '')
            setUsername(response.data.user.username ?? '')
            setJob(response.data.user.job ?? '')
            setAbout(response.data.user.about ?? '')
            setEmail(response.data.user.email ?? '')
            setCity(response.data.user.city ?? '')
            setFollowersCount(response.data.user.followersCount)
            setFollowingCount(response.data.user.followingCount)
            setPostsCount(response.data.user.postsCount)
            // setDistrict(response.data.user.district ?? '')

            setAccesPhone(response.data.user.isAccessiblePhone ?? false)
            setAccessLocation(response.data.user.isAccessibleLocation ?? false)
            setShowOnCustomers(response.data.user.isAccessibleMarketing ?? false)
            setHideMesNotif(response.data.user.isHideNotifContent ?? false)
            setShowOnCongenial(response.data.user.isAccessibleUnivocal ?? false)
            setSexValue((response.data.user.gender ?? '1') + '')
        } catch (error) {
            //console.log('err near users :: ', error)
            setItHasProblem(true)
        }
        setLoading(false)
    }
    useEffect(() => {
        if (localStorage.getItem('token') != null) {
            getMyProfile()
        }
    }, [])

    const avatarFile = useRef(null)
    const [avatarPreview, setAvatarPreview] = useState()
    // useEffect(() => {
    //     if (!avatarFile.current) {
    //         setAvatarPreview(undefined)
    //         return
    //     }

    //     var objectUrl
    //     objectUrl = URL.createObjectURL(avatarFile.current)
    //     setAvatarPreview(objectUrl)

    //     // free memory when ever this component is unmounted
    //     return () => URL.revokeObjectURL(objectUrl)
    // }, [avatarFile.current])

    const pickAvatarImage = () => {
        document.getElementById('pickImageInput').click()
    }

    const onSelectAatarFile = async (e) => {

        if (!e.target.files || e.target.files.length === 0) {
            avatarFile.current = undefined
            return
        }

        if (e.target.files[0].size > 100000) {
            try {
                const compress = new Compress()
                await compress
                    .compress([e.target.files[0]], {
                        size: 0.1,
                        quality: 0.80,
                        maxWidth: 450,
                        maxHeight: 450,
                        resize: true,
                    })
                    .then(async (data) => {
                        let mimeType = 'image/png'
                        const bstr = atob(data[0].data)
                        let n = bstr.length
                        const u8arr = new Uint8Array(n)
                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n)
                        }
                        const file = new File([u8arr], data.alt, { type: mimeType })
                        avatarFile.current = file
                    })
            } catch (error) {
                console.log(error)
                notifyErrorMessage('خطا در فشرده سازی تصویر.')
            }
        } else {
            avatarFile.current = e.target.files[0]
        }

        if (!avatarFile.current) {
            setAvatarPreview(undefined)
            return
        }

        var objectUrl
        objectUrl = URL.createObjectURL(avatarFile.current)
        setAvatarPreview(objectUrl)

    }

    const orientations = [8, 3, 6, 1]
    const rotateIndex = useRef(-1)
    const rotateAvatar = () => {
        ++rotateIndex.current
        if (rotateIndex.current > 3) {
            rotateIndex.current = 0
        }
        loadImage(
            avatarFile.current,
            (img) => {
                img.toBlob((blob) => {
                    avatarFile.current = blob
                    if (!avatarFile.current) {
                        setAvatarPreview(undefined)
                        return
                    }

                    var objectUrl
                    objectUrl = URL.createObjectURL(avatarFile.current)
                    setAvatarPreview(objectUrl)
                });
            },
            { orientation: orientations[rotateIndex.current] }
        )
    }

    const updateProfile = async () => {
        if (name.length < 5 || name.length > 20) {
            return
        } else if (username.length < 5 || username.length > 25) {
            return
        } else if (about.length > 1000) {
            return
        } else if (email.length > 50) {
            return
        } else if (job.length > 20) {
            return
        } else if (city.length < 2 || city.length > 20) {
            return
        }

        setLoading(true)

        if (avatarFile.current != null && avatarFile.current.size > 100000) {
            try {
                const compress = new Compress()
                await compress
                    .compress([avatarFile.current], {
                        size: 0.1,
                        quality: 0.80,
                        maxWidth: 450,
                        maxHeight: 450,
                        resize: true,
                    })
                    .then(async (data) => {
                        let mimeType = 'image/png'
                        const bstr = atob(data[0].data)
                        let n = bstr.length
                        const u8arr = new Uint8Array(n)
                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n)
                        }
                        const file = new File([u8arr], data.alt, { type: mimeType })
                        avatarFile.current = file
                    })
            } catch (error) {
                console.log(error)
                notifyErrorMessage('خطا در فشرده سازی تصویر.')
            }
        }

        const formData = new FormData()
        formData.append('name', name)
        formData.append('username', username)
        formData.append('about', about)
        formData.append('job', job)
        formData.append('email', email)
        formData.append('city', city)
        // formData.append('district', district)
        formData.append('isAccessibleLocation', isAccessibleLocation)
        formData.append('isAccessiblePhone', isAccessiblePhone)
        formData.append('isAccessibleMarketing', isShowOnCustomers)
        formData.append('isAccessibleUnivocal', isShowingOnCongenial)
        formData.append('isHideNotifContent', isHideMesNotif)
        formData.append('isShowingLastOnline', isShowingLastOnline)
        formData.append('gender', sexValue)
        if (avatarFile.current != null) {
            formData.append('avatar', avatarFile.current)
        }

        try {
            const response = await Axios.post('users/edit', formData)
            //console.log(response)
            if (response.status == 201) {
                localStorage.setItem('account', JSON.stringify(response.data))
                getMyProfile()
                notifySuccessful()
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            //console.log('err near users :: ', error)
            notifyErrorMessage(error.response.data.message ?? 'خطای نامشخص')
        }
        setLoading(false)
    }

    const pushToBlockListScreen = () => {
        history.push('/block-list')
    }

    const makeAccessAndInaccessibleStore = async () => {
        setLoading(true)
        try {
            const response = await Axios.post('stores/inaccessible')
            //console.log(response)
            if (response.status === 201) {
                notifySuccessful()
                getMyProfile()
            } else {
                notifyError()
            }
        } catch (error) {
            //console.log('err bge4 :: ', error)
            notifyErrorMessage(error.response.data.message ?? 'خطای نامشخص')
        }
        setLoading(false)
    }

    const makeAccountDisabled = async () => {
        setLoading(true)
        try {
            const response = await Axios.post('users/inaccessible')
            //console.log(response)
            if (response.status === 201) {
                notifySuccessful()
                logout()
            } else {
                notifyError()
            }
        } catch (error) {
            //console.log('err bge4 :: ', error)
            notifyErrorMessage(error.response.data.message ?? 'خطای نامشخص')
        }
        setLoading(false)
    }

    // handle confirmation dialog ::::::::
    const [isShowingDeactivateDialog, setShowingDeactivateDialog] = useState(false)
    const [isForDeativeStore, setIsForDeactiveStore] = useState(false)
    const showDeactivateConfirmationDialog = (isShowing) => {
        if (isShowing) {
            return (
                <ConfirmationDialog
                    data={{
                        onCloseFunc: closeConfirmationDialog,
                        ok,
                        cancel,
                        title: isForDeativeStore ? 'غیرفعال سازی فروشگاه' : 'غیرفعال سازی حساب کاربری',
                        message: isForDeativeStore
                            ? 'کاربر گرامی؛ در صورت غیرفعال سازی فروشگاه سفارشات فعلی شما همچنان به قوت خود باقی خواهد ماند و بدیهی است پاسخگویی به سفارش دهندگان الزامیست. پس از این اقدام اما سفارش جدیدی قابل ثبت نخواهد بود و فروشگاه شما از دسترس خارج می شود. برای فعال سازی مجدد فروشگاه می توانید از همین گزینه در ادامه استفاده نمایید.'
                            : 'کاربر گرامی؛ در صورت انجام، حساب کاربری شما از دسترس خارج می شود. شما می توانید با ورود مجدد به این حساب کاربری آن را دوباره فعال نمایید.  اطمینان دارید؟',
                    }}
                />
            )
        } else {
            return <div />
        }
    }

    const cancel = () => {
        setShowingDeactivateDialog(false)
    }

    const ok = () => {
        if (isForDeativeStore) {
            makeAccessAndInaccessibleStore()
        } else {
            makeAccountDisabled()
        }
        setShowingDeactivateDialog(false)
    }

    const closeConfirmationDialog = () => {
        setShowingDeactivateDialog(false)
    }
    // end handle confirmation dialog ----------

    // handle confirmation exit account dialog ::::::::
    const [isShowingConfirmationLogoutDialog, setShowingConfirmationLogoutDialog] = useState(false)
    const showConfirmationLogoutDialog = (isShowing) => {
        if (isShowing) {
            return (
                <ConfirmationDialog
                    data={{
                        onCloseFunc: closeConfirmationLogoutDialog,
                        ok: () => {
                            setShowingConfirmationLogoutDialog(false)
                            logout()
                        },
                        cancel: cancelLogout,
                        title: 'خروج از حساب کاربری',
                        message: 'کاربر گرامی؛ نسبت به خروج از حساب کاربری خود اطمینان دارید؟',
                    }}
                />
            )
        } else {
            return <div />
        }
    }

    const cancelLogout = () => {
        setShowingConfirmationLogoutDialog(false)
    }

    const closeConfirmationLogoutDialog = () => {
        setShowingConfirmationLogoutDialog(false)
    }
    // end handle confirmation exit account dialog ----------

    const logout = async () => {
        setLoading(true)
        try {
            const response = await Axios.post('users/logout', { isWeb: true })
            //console.log(response)
            if (response.status == 201) {
                localStorage.setItem('chatsData', '[]')
                localStorage.setItem('chatsScroll', 0)
                localStorage.setItem('chatsPage', 1)
                localStorage.removeItem('token')
                localStorage.removeItem('account')
                localStorage.removeItem('selectedAddress')
                localStorage.removeItem('drawer')
                localStorage.setItem('refreshDrawer', true)
                window.scrollTo({ top: 0 })
                history.replace('/')
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            //console.log('err near users :: ', error)
            notifyErrorMessage(error.response.data.message ?? 'خطای نامشخص')
        }
        setLoading(false)
    }

    // handle showing about us dialog screen
    const [isShowingAboutUsDialog, setShowingAboutUsDialog] = useState(false)
    const showAboutUsDialog = (isShowing) => {
        if (isShowing) {
            return <AboutUsDialog data={{ onCloseFunc: closeAboutUsDialog }} />
        } else {
            return <div />
        }
    }

    const closeAboutUsDialog = () => {
        setShowingAboutUsDialog(false)
    }
    // end of profile dialog screen

    return (
        <div style={{ direction: 'rtl', overflowX: 'hidden', display: 'flex', justifyContent: 'center', width: '100%' }}>
            {showDeactivateConfirmationDialog(isShowingDeactivateDialog)}
            {showConfirmationLogoutDialog(isShowingConfirmationLogoutDialog)}
            {showAboutUsDialog(isShowingAboutUsDialog)}
            {localStorage.getItem('token') == null ? (
                <LoginNeededView data={{ minusHeight: 0 }} />
            ) : isLoading ? (
                <LoadingDialog />
            ) : !isLoading && itHasProblem ? (
                <TryAgainWidget onTryAgain={getMyProfile} />
            ) : (
                <div id="accountDiv" style={{ justifyContent: 'center', alignItems: 'center', maxWidth: 500, paddingBottom: 100, width: '100%' }}>
                    {data.user.name == null && (
                        <p className="ThirdBoldText" style={{ paddingTop: 64, paddingRight: 16, paddingLeft: 16, paddingBottom: 16, textAlign: 'center', color: 'white', backgroundColor: greenColor }}>
                            کاربر گرامی؛ برای دسترسی به کلیه امکانات برنامه لطفا اطلاعات حساب کاربری خود را تکمیل نمایید.
                        </p>
                    )}
                    <div onClick={pickAvatarImage} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
                        <input id="pickImageInput" type="file" hidden onChange={onSelectAatarFile} />
                        <img src={avatarFile.current != null ? avatarPreview : data.user.avatar != null && data.user.avatar.length > 0 ? webUrl + data.user.avatar : AvatarPNG} style={{ objectFit: 'cover', borderRadius: 60, width: 120, height: 120, marginTop: 75 }} />
                    </div>
                    {avatarFile.current != null && <Button onClick={rotateAvatar}>چرخش تصویر</Button>}
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            marginTop: 24,
                            marginLeft: 24,
                            marginRight: 24,
                        }}
                    >
                        <div style={{ display: 'flex', alignItems: 'center' }}>
                            <div className="clickable" onClick={() => {
                                history.push({ pathname: 'follows/list', search: '?id=' + account.id + '&type=' + 1 })
                            }} style={{ flex: 1 }}>
                                <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 6, flex: 1, fontSize: 12 }}>
                                    {followersCount}
                                </p>
                                <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 6, flex: 1, fontSize: 12 }}>
                                    دنبال کننده
                                </p>
                            </div>
                            <div className="clickable" onClick={() => {
                                history.push({ pathname: 'follows/list', search: '?id=' + account.id + '&type=' + 2 })
                            }} style={{ flex: 1 }}>
                                <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 0, flex: 1, fontSize: 12 }}>
                                    {followingCount}
                                </p>
                                <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 0, flex: 1, fontSize: 12 }}>
                                    دنبال می کند
                                </p>
                            </div>
                            <div style={{ flex: 1 }}>
                                <p className="SecondaryText" style={{ textAlign: 'center', fontSize: 12 }}>
                                    {postsCount}
                                </p>
                                <p className="SecondaryText" style={{ textAlign: 'center', fontSize: 12 }}>
                                    پست
                                </p>
                            </div>
                        </div>
                        <div style={{ display: 'flex', marginTop: 16, marginRight: 8, alignItems: 'center' }}>
                            <Button variant="contained" fullWidth onClick={() => {
                                if (localStorage.getItem('token') == null) {
                                    history.push('/login')
                                } else {
                                    history.push({ pathname: 'user', search: '?id=' + account.id })
                                }
                            }} style={{ backgroundColor: greenColor, marginLeft: 8, color: 'white' }}>صفحه عمومی</Button>

                            <Button variant="contained" fullWidth onClick={() => {
                                if (localStorage.getItem('token') == null) {
                                    history.push('/login')
                                } else {
                                    history.push({ pathname: '/explore' })
                                }
                            }} style={{ backgroundColor: blueColor, color: 'white', marginLeft: 8 }}>فعالیت دوستان</Button>

                            <Button variant="contained" fullWidth onClick={() => {
                                if (localStorage.getItem('token') == null) {
                                    history.push('/login')
                                } else {
                                    history.push({ pathname: '/society/topics-manager', data: { title: 'مدیریت انجمن' } })
                                }
                            }} style={{ backgroundColor: blueColor, color: 'white' }}>مدیریت انجمن</Button>
                        </div>
                        <TextField error={name.length < 5 || name.length > 20} helperText={name.length < 5 || name.length > 20 ? 'بین ۵ تا ۲۰ کاراکتر' : ''} label="* نام و نام خانوادگی" value={name} onChange={(e) => setName(e.target.value)} style={{ marginTop: 16 }} />
                        <TextField error={username.length < 5 || username.length > 25} helperText={username.length < 5 || username.length > 25 ? 'بین ۵ تا 25 کاراکتر' : ''} label="* نام کاربری" value={username} onChange={(e) => setUsername(e.target.value)} style={{ marginTop: 16 }} />
                        <TextField error={job.length > 20} helperText={job.length > 20 ? 'بین 0 تا 20 کاراکتر' : ''} label="شغل / حرفه" value={job} onChange={(e) => setJob(e.target.value)} style={{ marginTop: 16 }} />
                        <TextField error={about.length > 1000} helperText={about.length > 1000 ? 'بین 0 تا 1000 کاراکتر' : ''} multiline label="درباره" value={about} onChange={(e) => setAbout(e.target.value)} style={{ marginTop: 16 }} />
                        <TextField label="ایمیل" value={email} onChange={(e) => setEmail(e.target.value)} style={{ marginTop: 16 }} />
                        <TextField error={city.length < 2 || city.length > 20} helperText={city.length < 2 || city.length > 20 ? 'بین 2 تا ۲۰ کاراکتر' : ''} label="* شهر" value={city} onChange={(e) => setCity(e.target.value)} style={{ marginTop: 16 }} />
                        {/* <TextField error={district.length < 2 || district.length > 20} helperText={district.length < 2 || district.length > 20 ? 'بین 2 تا ۲۰ کاراکتر' : ''} label="* منطقه / محله" value={district} onChange={(e) => setDistrict(e.target.value)} style={{ marginTop: 16 }} /> */}
                    </div>
                    <br />
                    <div style={{ display: 'flex', alignItems: 'center', marginRight: 20, marginLeft: 32, marginTop: 32 }}>
                        <Checkbox checked={isAccessibleLocation} onChange={changeDistanceCB} />
                        <p className="ThirdText"> فاصله تا موقعیت مکانی من نمایش داده شود. (حداکثر فاصله نمایش داده شده تا شعاع ۱ کیلومتری شما می باشد. مختصات دقیق منتشر نمی شود.)</p>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', marginRight: 20, marginLeft: 32, marginTop: 8 }}>
                        <Checkbox checked={isAccessiblePhone} onChange={changePhoneCB} />
                        <p className="ThirdText">شماره موبایل من نمایش داده شود.</p>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', marginRight: 20, marginLeft: 32, marginTop: 8 }}>
                        <Checkbox checked={isHideMesNotif} onChange={changeNotifContentCB} />
                        <p className="ThirdText">محتوای پیام افراد در اعلان ها مخفی شود.</p>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', marginRight: 20, marginLeft: 32, marginTop: 8 }}>
                        <Checkbox checked={isShowingLastOnline} onChange={changeLastOnlineCB} />
                        <p className="ThirdText"> آخرین فعالیت من نمایش داده شود. (در صورت فعال نبودن آنلاین بودن شما نیاز نمایش داده نخواهد شد.)</p>
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', paddingRight: 32, marginTop: 0 }}>
                        <p className="ThirdText" style={{ width: 100 }}>
                            جنسیت :
                        </p>
                        <section style={{ marginLeft: 44, marginRight: 'auto', marginTop: 4 }}>
                            <FormControl component="fieldset">
                                <RadioGroup row aria-label="gender" name="gender1" value={sexValue} onChange={handleSexChange}>
                                    <FormControlLabel value="1" control={<Radio />} label="آقا" />
                                    <FormControlLabel value="2" control={<Radio />} label="خانم" />
                                </RadioGroup>
                            </FormControl>
                        </section>
                    </div>
                    <div style={{ marginLeft: 32, marginRight: 32, marginTop: 16 }}>
                        <Button onClick={updateProfile} variant="contained" fullWidth style={{ backgroundColor: greenColor, color: 'white' }}>
                            ثبت اطلاعات
                        </Button>
                    </div>
                    <Button style={{ marginTop: 16, color: 'black' }} onClick={() => setShowingConfirmationLogoutDialog(true)}>خروج از حساب</Button>
                    <div style={{ marginLeft: 32, marginRight: 32 }}>
                        <Button onClick={pushToBlockListScreen} style={{ color: 'black' }}>لیست بلاک شده ها</Button>
                    </div>
                    <div style={{ marginLeft: 32, marginRight: 32 }}>
                        <Button
                            onClick={() => {
                                setIsForDeactiveStore(false)
                                setShowingDeactivateDialog(true)
                            }}
                            style={{ color: data.user.status === 1 ? 'black' : greenColor }}
                        >
                            {data.user.status === 1 ? 'غیرفعال سازی حساب کاربری' : 'فعالسازی حساب کاربری'}
                        </Button>
                    </div>
                    <div style={{ marginLeft: 32, marginRight: 32 }}>
                        <Button onClick={() => setShowingAboutUsDialog(true)} style={{ color: '#878787' }}>درباره irBit</Button>
                    </div>
                </div>
            )}

            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default AccountScreen
