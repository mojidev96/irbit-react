import React, { useEffect, useState, useRef, createRef } from 'react'
import { AppBar, Toolbar, Dialog, Typography, Divider, Backdrop, Popover, DialogContentText, Select, GridList, Fab, Box, LinearProgress, IconButton, TextField, Checkbox, RadioGroup, Radio, MenuItem, FormControl, Button, CircularProgress, Grid, InputLabel } from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdAdd, MdExitToApp, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { notifySuccessful, greenColor, token, notifyErrorMessage, webUrl, backgroundColor, blueColor, account } from '../App'
import { parsePrice, parseDate } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'
import AvatarPNG from '../icons/avatar.png'
import { ItemContent } from 'semantic-ui-react'

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function FollowsScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const userId = useRef(0)
    const type = useRef(0) // 1=followes, 2=following
    const page = useRef(1)

    const [isLoadingList, setLoadingList] = useState(true)
    const [isLoadingFollowUnfollow, setLoadingFollowUnfollow] = useState(false)
    const [isAllGot, setAllGot] = useState(false)
    const followsList = useRef([])

    useEffect(() => {
        const search = props.location.search
        const params = new URLSearchParams(search)
        userId.current = params.get('id')
        type.current = params.get('type')

        getList()
        return () => {
            window.onscroll = ''
        }
    }, [])

    const selectedUserId = useRef(0)

    const getList = async () => {
        if (isAllGot) {
            return
        }
        setLoadingList(true)
        try {
            const response = await Axios.post('follows/list', {
                id: userId.current,
                type: type.current,
                page: page.current,
            }, { headers: { token } })

            //console.log(response)
            if (response.data instanceof Array && response.data.length > 0) {
                followsList.current.push(...response.data)
                ++page.current
            } else {
                setAllGot(true)
            }

            setLoadingList(false)
        } catch (error) {
            setLoadingList(false)
        }
    }

    const followUnfollow = async (type, index) => {
        setLoadingFollowUnfollow(true)
        try {
            //1=follow, 2=unfollow
            const response = await Axios.post('follows', {
                id: selectedUserId.current,
                type
            })

            if (response.status == 201) {
                notifySuccessful()
                followsList.current[index].isFollowed = (type == 1)
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            notifyErrorMessage('اشکال در برقراری ارتباط')
        }
        setLoadingFollowUnfollow(false)
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        try {
            if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridLIst').offsetHeight) {
                console.log('gweeg32ef')
                isWorkingScroll.current = 1
                getList()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    return (
        <div className={classes.root} style={{ overflow: 'hidden' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        {type.current == 0 ? 'در حال بارگزاری' : type.current == 1 ? 'دنبال کنندگان' : 'دنبال شدگان'}
                    </Typography>
                </Toolbar>
            </AppBar>
            {isLoadingFollowUnfollow && (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            )}
            <div className="StackContainer" style={{ overflow: 'hidden' }}>
                <div style={{ paddingTop: 48, width: '100%', display: 'flex', justifyContent: 'center', overflow: 'hidden' }}>
                    <GridList cellHeight="auto" id="gridLIst" cols={1} style={{ width: '100%', maxWidth: 600 }}>
                        {followsList.current.map((item, index) => (
                            <div className="clickable" key={item.id}>
                                <Grid container>
                                    <div style={{ width: '100%', maxWidth: 600, paddingRight: 16, paddingLeft: 16, marginTop: 16 }}>
                                        <div onClick={() => {
                                            history.push({ pathname: '/user', search: '?id=' + item.id })
                                        }} style={{ display: 'flex', alignItems: 'center' }}>
                                            <img className="avatar_image" style={{ width: 50, height: 50, borderRadius: 25 }} src={item.avatar != null && item.avatar.length > 0 ? webUrl + item.avatar : AvatarPNG} />
                                            <div style={{ flex: 1, marginRight: 8, marginLeft: 16 }}>
                                                <p className="PrimaryText">{item.name}</p>
                                                <p className="ThirdLightText">{item.username}</p>
                                            </div>
                                            {(account && account.id != item.id || !account) && <Button
                                                variant={item.isFollowed == true ? "outlined" : "contained"}
                                                onClick={(e) => {
                                                    e.stopPropagation()
                                                    selectedUserId.current = item.id
                                                    followUnfollow(item.isFollowed == true ? 2 : 1, index)
                                                }}
                                                style={{ width: 100, color: 'white', backgroundColor: item.isFollowed == true ? backgroundColor : blueColor }}
                                            >
                                                {isLoadingFollowUnfollow && item.id == selectedUserId.current ? <CircularProgress size={24} style={{ color: 'white' }} /> : item.isFollowed == true ? 'آنفالو' : 'دنبال کردن'}
                                            </Button>}
                                        </div>
                                        <Divider style={{ width: '100%', marginTop: 8 }} />
                                    </div>
                                </Grid>
                                {index == followsList.current.length - 1 && <div style={{ height: 16 }}></div>}
                                {index == followsList.current.length - 1 && isLoadingList ? (
                                    <div style={{ bottom: 0 }}>
                                        <div style={{ width: window.innerWidth }}>
                                            <LinearProgress color="secondary" />
                                        </div>
                                    </div>
                                ) : (
                                    <div></div>
                                )}
                            </div>
                        ))}
                    </GridList>
                </div>
            </div>
            {followsList.current.length == 0 && !isLoadingList ? <EmptyView marginBottom={0} /> : <div />}
            {followsList.current.length == 0 && isLoadingList ? (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="secondary" />
                </Backdrop>
            ) : (
                <div />
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default FollowsScreen
