import React, { useEffect, useState, useRef, createRef, useMemo } from 'react'
import { AppBar, Toolbar, Popover, Backdrop, LinearProgress, IconButton, Dialog, List, RadioGroup, Radio, MenuItem, FormControlLabel, Button, CircularProgress, Grid, Divider, Card } from '@material-ui/core'
import AudioPlayer from 'react-h5-audio-player'
import 'react-h5-audio-player/lib/styles.css'
import Sound from 'react-sound'
import { MdArrowForward, MdClose, MdReply, MdDone, MdDoneAll, MdBlock, MdMoreVert, MdContentCopy, MdFiberManualRecord, MdEdit, MdDelete, MdImage, MdKeyboardVoice, MdPauseCircleFilled, MdSend, MdStop } from 'react-icons/md'
import ReactAudioPlayer from 'react-audio-player'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { account, backgroundColor, greenColor, token, webUrl, notifyErrorMessage, notifyWarnMessage, notifyCopied, itemColor, accentcolor, lightIconColor } from '../App'
import { parsePrice, parseDate } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import MicRecorder from 'mic-recorder-to-mp3'
import Timer from 'react-compound-timer'
import copy from 'copy-to-clipboard'
import ProfileScreen from './ProfileScreen'
import AvatarPNG from '../icons/avatar_white.png'
import WaitingPNG from '../icons/waiting.png'
import ReactTextFormat from 'react-text-format';
import { isAndroid, isIOS } from 'react-device-detect'

const Compress = require('compress.js')

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function CoinRoomMessages(props) {
    let history = useHistory()
    const classes = useStyles()

    const recorder = useMemo(() => new MicRecorder({ bitRate: 128 }), [])
    const chatData = useRef(null)
    const [isBlockMic, setIsBlockMic] = useState(false)
    const [isRecording, setIsRecording] = useState(false)
    const [isVoiceFileGet, setIsVoiceFileGet] = useState(false)
    const [voiceFile, setVoiceFile] = useState(null)
    const [voiceDuration, setVoiceDuration] = useState(0)

    var timerRef = useRef()

    const page = useRef(1)
    const isBottomScrolled = useRef(false)
    const lastId = useRef(null)

    const [isLoading, setLoading] = useState(true)
    const [isAllGot, setAllGot] = useState(false)

    const [messages, setMessages] = useState([])
    const [newTextMessage, setNewTextMessage] = useState('')
    const [messageHeight, setMessageHeight] = useState(30)

    const [isSettingUp, setIsSettingUp] = useState(false)
    const [isTyping, setIsTyping] = useState(false)

    const [isBlockedByMe, setIsBlockedByMe] = useState(false)
    const [isBlockedByFriend, setIsBlockedByFriend] = useState(false)

    const [onlineCount, setOnlineCount] = useState(0)

    const [price, setPrice] = useState(0)
    const [oneDayChange, setOneDayChange] = useState(0)


    const writers = useRef('')

    useEffect(() => {
        setIsUser(account != null && account.id != null && token != null && account.name != null)
        const search = props.location.search
        const params = new URLSearchParams(search)
        const coinId = params.get('coinId')

        chatData.current = { coinId }
        getCoinData()

        if (props.socket && props.socket.connected && account != null && account.id != null && token != null && account.name != null) {
            props.socket.emit('joinCoin', { coinId: chatData.current.coinId })
            props.socket.emit('join', { userId: account.id, token: token })
            props.socket.emit('joinPrices', { coinId: chatData.current.coinId })
        }

        props.socket.on('connect', () => {
            if (props.socket.connected) {
                if (account != null) {
                    props.socket.emit('joinCoin', { coinId: chatData.current.coinId })
                    props.socket.emit('join', { userId: account.id, token: token })
                    props.socket.emit('joinPrices', { coinId: chatData.current.coinId })
                }
            } else {
                if (token != null && account != null) {
                    props.socket.emit('online-users', { userId: account.id, token: token, isOnline: false })
                }
            }
        })

        props.socket.on('coinMessages', (data) => {
            console.log('wegwegw:000000: ' + JSON.stringify(data))
            if (chatData.current.coinId == data.coinId && data.price) {
                // console.log('wegwegw:: ' + data.price)
                setCanAnimate(true)
                setPrice(data.price)
                setOneDayChange(data.oneDayChange)
                setTimeout(() => {
                    setCanAnimate(false)
                }, 500);
            }
            if (chatData.current.coinId == data.coinId && data.onlineCount) {
                setOnlineCount(data.onlineCount)
            }
            if (data.senderName && chatData.current.coinId == data.coinId) {
                setIsTyping(false)
                if (data.senderUserId == account.id) {
                    // ده تای آخر هرچی بود میگیم درسته رسیده
                    // این خط اشتباست باعث میشه اگه فرضا ۲ تا پیام پشت هم بده و یکیشون رو فورا ریپلی کنه پیام ریپلای شدش به یه پیامی که نیست
                    // و درواقع با ایدی صفر رفرنس داده شه بعدا درستش میکنم با هش
                    setMessages((prv) => {
                        const list = prv
                        const newList = list.map((item, indexx) => {
                            if (indexx < 20) {
                                var updatedItem = {}
                                if (data.dateCreate == item.dateCreate) {
                                    updatedItem = {
                                        ...item,
                                        id: data.id,
                                        isLoadingToSend: false,
                                    }
                                } else {
                                    updatedItem = {
                                        ...item,
                                        isLoadingToSend: false,
                                    }
                                }
                                return updatedItem
                            }
                            return item
                        })
                        return [...newList]
                    })
                } else {
                    setMessages((prv) => {
                        const list = prv
                        list.insert(0, data)
                        return [...list]
                    })
                }
                scrollToBottom()
            }
        })

        props.socket.on('editCoinMessage', (data) => {
            if (data.toCoinId == chatData.current.coinId) {
                setMessages((prv) => {
                    const list = prv
                    const newList = list.map((item) => {
                        var updatedItem = { ...item }
                        if (data.fromUserId == account.id) {
                            if (data.messageId == item.id) {
                                // console.log('rghwerbk:: g44')
                                updatedItem = {
                                    ...item,
                                    id: data.messageId,
                                    message: data.newText,
                                    isEdited: true,
                                    isLoadingToSend: false,
                                }
                                return updatedItem
                            } else {
                                return item
                            }
                        } else {
                            return item
                        }
                    })
                    return [...newList]
                })
            }
        })

        props.socket.on('isTypingCoin', (data) => {
            if (chatData.current.coinId == data.coinId) {
                writers.current += (' ● ' + data.writerName)
                setIsTyping(true)
                setTimeout(() => {
                    writers.current = writers.current.replace(' ● ' + data.writerName, '')
                    setIsTyping(false)
                }, 3000)
            }
        })

        props.socket.on('deleteCoinMessage', (data) => {
            console.log('rhehrw:0: ' + chatData.current.coinId + ' --- ' + data.coinId)
            console.log('rhehrw:: ' + JSON.stringify(data))
            if (data.coinId == chatData.current.coinId) {
                console.log('rhehrw:2: ' + JSON.stringify(data))
                setMessages((prv) => {
                    const list = prv
                    list.forEach((item, index) => {
                        if (item.id == data.messageId) {
                            console.log('rhehrw:3: ' + JSON.stringify(data))
                            list.splice(index, 1)
                        }
                    })
                    return [...list]
                })
            }
        })

        getMicrophonePermission()

        setIsSettingUp(true)
        return () => {
            props.socket.emit('leaveCoin', { coinId: chatData.current.coinId })
            window.onscroll = ''
        }
    }, [])

    const [canSendIsTyping, setCanSendIsTyping] = useState(true)
    const [isSendIsTyping, setIsSendIsTyping] = useState(false)
    const [isUser, setIsUser] = useState(false)

    useEffect(() => {
        if (isSendIsTyping) {
            if (canSendIsTyping) {
                props.socket.emit('isTypingCoin', { coinId: chatData.current.coinId, writerName: account.name })
                setCanSendIsTyping(false)
                setTimeout(() => {
                    setCanSendIsTyping(true)
                }, 3000)
            }
            setIsSendIsTyping(false)
        }
    }, [isSendIsTyping])

    const [isSendDeleteMes, setIsSendDeleteMes] = useState(false)
    useEffect(() => {
        if (isSendDeleteMes) {
            props.socket.emit('deleteCoinMessage', { token: token, messageId: selectedMessage.id })
            setIsSendDeleteMes(false)
        }
    }, [isSendDeleteMes])

    const [isSendEditMes, setIsSendEditMes] = useState(false)
    useEffect(() => {
        if (isSendEditMes) {
            //console.log('ergbho4')
            setMessages((prv) => {
                const list = prv
                const newList = list.map((item, indexx) => {
                    var updatedItem = { ...item }
                    if (selectedMessageIndex == indexx) {
                        updatedItem = {
                            ...item,
                            isLoadingToSend: true,
                        }
                        return updatedItem
                    } else {
                        return item
                    }
                })
                return [...newList]
            })
            props.socket.emit('editCoinMessage', { token: token, messageId: selectedMessage.id, newText: newTextMessage })
            setIsEditingMode(false)
            setNewTextMessage('')
            setIsSendDeleteMes(false)
        }
    }, [isSendEditMes])

    const [isSendMessage, setIsSendMessage] = useState(false)
    const [isReply, setIsReply] = useState(false)
    // پیامی که پیام جدید کاربر در جواب اون هست
    const [replyMessage, setReplyMessage] = useState('')
    const [replyMessageId, setReplyMessageId] = useState(0)
    const [isEditingMode, setIsEditingMode] = useState(false)
    const mesToSend = useRef({})
    useEffect(() => {
        if (isSendMessage) {
            if (newTextMessage.length > 0 || voiceFile != null || selectedImageFile != null) {
                const dateCreate = new Date().toISOString()
                //console.log('gwegaa:: ' + dateCreate + '--- ' + voiceDuration + ' --- ' + account.id + ' --- ' + chatData.current)
                mesToSend.current = {
                    coinId: chatData.current.coinId,
                    senderToken: token,
                    senderName: account.name,
                    senderAvatar: account.avatar,
                    senderCity: account.city,
                    senderDistrict: account.district,
                    dateCreate: dateCreate,
                    senderUserId: account.id,
                    imagePath: '',
                    isLoadingToSend: true,
                    id: 0,
                    isReply: isReply,
                    isSeen: false,
                    message: newTextMessage,
                    replyMessage: replyMessage == '' ? null : replyMessage,
                    replyMessageId: replyMessageId,
                    voiceDuration: voiceDuration,
                    voicePath: '',
                    filePath: null,
                    fileSize: null,
                    filename: null,
                }
                setMessages((prv) => {
                    const list = prv
                    list.insert(0, mesToSend.current)
                    return [...list]
                })
                scrollToBottom()
                if (voiceFile != null || selectedImageFile != null) {
                    uploadNewMessageFiles()
                } else {
                    props.socket.emit('coinMessages', mesToSend.current)
                    setTimeout(() => {
                        setNewTextMessage('')
                        setMessageHeight(30)
                        resetVoiceRecorder()
                        setSelectedImageFile(null)
                        setVoiceDuration(0)
                    }, 100)
                }
            } else {
                notifyWarnMessage('پیام خالی قابل ارسال نیست.')
            }
            setIsSendMessage(false)
        }
    }, [isSendMessage])

    const getMicrophonePermission = () => {
        try {
            navigator.mediaDevices.getUserMedia(
                { audio: true },
                () => {
                    //console.log('Permission Granted')
                    setIsBlockMic(false)
                },
                () => {
                    //console.log('Permission Denied')
                    setIsBlockMic(true)
                }
            )
        } catch (e) {
            //console.log(e)
        }
    }

    const scrollToBottom = () => {
        setTimeout(function () {
            window.scrollTo({
                top: document.body.scrollHeight,
                left: 0,
            })
            if (!isBottomScrolled.current) {
                setTimeout(function () {
                    isBottomScrolled.current = true
                }, 100)
            }
        }, 100)
    }

    const getCoinData = async () => {
        setLoading(true)
        const coinId = chatData.current.coinId
        await Axios.post('coins/data', { id: coinId })

            .then((response) => {
                if (response.status == 201) {
                    chatData.current = { coinId, name: response.data.name, logoUrl: response.data.logoUrl }
                    setPrice(response.data.price)
                    setOneDayChange(response.data.oneDayChange)
                    getMessages()
                } else {
                    notifyErrorMessage(response.data.message ?? 'خطای نامشخص')
                }
            })
            .catch((e) => {
                notifyErrorMessage((e && e.response && e.response.data && e.response.data.message) ?? 'خطای نامشخص')
            })
        setLoading(false)
    }

    const [isLoadingForUpload, setIsLoadingForUpload] = useState(false)
    const uploadNewMessageFiles = async () => {
        setIsLoadingForUpload(true)
        const formData = new FormData()
        if (voiceFile != null) {
            formData.append('voice', voiceFile)
        }
        if (selectedImageFile != null) {
            formData.append('image', selectedImageFile)
        }

        setTimeout(() => {
            setNewTextMessage('')
            setMessageHeight(30)
            resetVoiceRecorder()
            setSelectedImageFile(null)
            setVoiceDuration(0)
        }, 100)

        await Axios.post('coinMessages', formData, { headers: { 'content-type': 'multipart/form-data' } })
            .then((response) => {
                //console.log('wewgf2ew:: ' + JSON.stringify(response))
                if (response.status == 201) {
                    mesToSend.current.voicePath = response.data.voicePath
                    mesToSend.current.imagePath = response.data.imagePath
                    //console.log('gwewewe#:' + mesToSend.current.imagePath)
                    props.socket.emit('coinMessages', mesToSend.current)
                } else {
                    notifyErrorMessage(response.data.message ?? 'خطای نامشخص')
                }
            })
            .catch((e) => {
                notifyErrorMessage(e.response.data.message ?? 'خطای نامشخص')
            })
        setIsLoadingForUpload(false)
    }

    const getMessages = async () => {
        if (isAllGot) {
            return
        }
        setLoading(true)
        try {
            const response = await Axios.post('coinMessages/conversation', {
                coinId: chatData.current.coinId,
                page: page.current,
            })

            // if (response.data.blockStatus == 2) {
            //     setIsBlockedByMe(true)
            // } else if (response.data.blockStatus == 3) {
            //     setIsBlockedByFriend(true)
            // }
            if (response.data.messages instanceof Array && response.data.messages.length > 0) {
                setMessages((prv) => {
                    const list = prv
                    list.push(...response.data.messages)
                    return [...list]
                })
                ++page.current

                if (lastId.current != null) {
                    setTimeout(function () {
                        try {
                            const element = document.getElementById(lastId.current)
                            element.scrollIntoView()
                        } catch (e) { }
                    }, 100)
                }
                lastId.current = response.data.messages[0].id
                if (page.current == 2) {
                    scrollToBottom()
                }
            } else {
                setAllGot(true)
            }

            setLoading(false)
        } catch (error) {
            console.log('server err :: ', error)
            setLoading(false)
        }
    }

    const getMessageWidth = (messageLength, voicePath, imagePath) => {
        if (voicePath != null || imagePath != null) {
            return 267
        } else if (messageLength < 10) {
            return 150
        } else if (messageLength < 50) {
            return 180
        } else if (messageLength < 80) {
            return 200
        } else if (messageLength < 100) {
            return 220
        } else if (messageLength < 150) {
            return 240
        } else {
            return 267
        }
    }

    const changeNewTextMessage = (event) => {
        const text = event.target.value
        if (isAndroid || isIOS) {
            if (text.length < 15) {
                setMessageHeight(30)
            } else if (text.length < 30) {
                setMessageHeight(40)
            } else if (text.length < 45) {
                setMessageHeight(50)
            } else if (text.length < 60) {
                setMessageHeight(60)
            } else if (text.length < 90) {
                setMessageHeight(90)
            } else if (text.length < 120) {
                setMessageHeight(120)
            }
        } else {
            if (text.length < 50) {
                setMessageHeight(30)
            } else if (text.length < 100) {
                setMessageHeight(80)
            } else if (text.length < 150) {
                setMessageHeight(110)
            } else if (text.length < 200) {
                setMessageHeight(130)
            } else if (text.length < 250) {
                setMessageHeight(150)
            }
        }
        setIsSendIsTyping(true)
        setNewTextMessage(text)
    }

    const [askedPermission, setAskedPermission] = useState(false)

    const startStopRecording = () => {
        if (!askedPermission) {
            getMicrophonePermission()
            setAskedPermission(true)
        }
        if (isRecording) {
            setVoiceDuration(timerRef.getTime())
            //console.log('webwed:: ' + timerRef.getTime())
            timerRef.stop()
            recorder
                .stop()
                .getMp3()
                .then(([buffer, blob]) => {
                    const file = new File(buffer, 'voice.mp3', {
                        type: blob.type,
                        lastModified: Date.now(),
                    })
                    setVoiceFile(file)
                    //console.log('eegg:: ' + file.size)
                    setIsRecording(false)
                    setIsVoiceFileGet(true)
                })
                .catch((e) => console.log(e))
        } else {
            if (isBlockMic) {
                notifyMicrophonePermission()
                //console.log('Permission Denied')
            } else {
                //console.log('gwew90tg3 000')
                timerRef.reset()
                timerRef.start()
                recorder
                    .start()
                    .then(() => {
                        //console.log('gwew90tg3')
                        setIsRecording(true)
                    })
                    .catch((e) => console.error('gwew90tg3 eee ' + e))
            }
        }
    }

    const resetVoiceRecorder = () => {
        try {
            timerRef.reset()
        } catch (e) { }

        setIsVoiceFileGet(false)
        setVoiceFile(null)
    }

    //image picker ::::::::
    const [selectedImageFile, setSelectedImageFile] = useState()
    const [imagePreview, setImagePreview] = useState()

    // create a preview as a side effect, whenever selected file is changed
    useEffect(() => {
        if (!selectedImageFile) {
            setImagePreview(undefined)
            return
        }

        const objectUrl = URL.createObjectURL(selectedImageFile)
        setImagePreview(objectUrl)

        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [selectedImageFile])

    const onSelectFile = async (e) => {
        if (!e.target.files || e.target.files.length == 0) {
            setSelectedImageFile(undefined)
            return
        }

        if (e.target.files[0].size > 100000) {
            try {
                const compress = new Compress()
                await compress
                    .compress([e.target.files[0]], {
                        size: 0.2,
                        quality: 0.75,
                        maxWidth: 1024,
                        maxHeight: 1024,
                        resize: true,
                    })
                    .then((data) => {
                        let mimeType = 'image/png'
                        const bstr = atob(data[0].data)
                        let n = bstr.length
                        const u8arr = new Uint8Array(n)
                        while (n--) {
                            u8arr[n] = bstr.charCodeAt(n)
                        }
                        setSelectedImageFile(new File([u8arr], data.alt, { type: mimeType }))
                    })
            } catch (error) {
                notifyErrorMessage('خطا در فشرده سازی تصویر.')
            }
        } else {
            setSelectedImageFile(e.target.files[0])
        }
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        try {
            if (isBottomScrolled.current == true && isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset == 0) {
                //console.log('gwee')
                isWorkingScroll.current = 1
                getMessages()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    const fileUploadButton = () => {
        document.getElementById('fileButton').click()
    }

    const removeImage = () => {
        setSelectedImageFile(null)
    }

    // handle message actions dialog ::::::::
    const [isShowingActions, setIsShowingActions] = useState(false)
    const [selectedMessage, setSelectedMessage] = useState(null)
    const [selectedMessageIndex, setSelectedMessageIndex] = useState(0)
    // end handle message actions dialog ----------

    const notifyIsRekeaseAfterAccepted = () => toast.success('سپاس؛ پس از تایید ناظر منتشر می شود.')
    const notifyMicrophonePermission = () => toast.success('دسترسی به میکروفون فراهم نیست.')
    const notifyMinAndMaxForComment = () => toast.error('لطفا بین ۲ تا ۳۰۰ کاراکتر وارد نمایید.', { autoClose: 3500 })

    const [anchorEl, setAnchorEl] = React.useState(null)
    const handleClickOpenPopover = (event) => {
        setAnchorEl(event.currentTarget)
    }
    const handleClosePopover = () => {
        setAnchorEl(null)
    }
    const openPopover = Boolean(anchorEl)
    const popoverId = openPopover ? 'simple-popover' : undefined

    const [canAnimate, setCanAnimate] = useState(false)

    const pushToImageViewer = (image) => {
        history.push({ pathname: '/image-viewer', image })
    }

    // handle showing profile dialog screen
    const [selectedUserId, setSelectedUserId] = useState(0)
    const [isShowingProfileDialog, setShowingProfileDialog] = useState(false)
    const showProfileScreen = (isShowing) => {
        if (isShowing) {
            return <ProfileScreen data={{ id: selectedUserId, onCloseFunc: closeProfileScreen }} />
        } else {
            return <div />
        }
    }

    const closeProfileScreen = () => {
        setShowingProfileDialog(false)
    }
    // end of profile dialog screen

    const goBack = () => {
        if (history.length < 3) {
            history.replace('/')
        } else {
            history.goBack()
        }
    }

    return !isSettingUp ? (
        <div>
            <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                <CircularProgress color="white" />
            </Backdrop>
        </div>
    ) : (
        <div className={classes.root} style={{ overflow: 'hidden', height: '100%' }}>
            {showProfileScreen(isShowingProfileDialog)}
            <AppBar position="fixed" style={{ backgroundColor: itemColor }}>
                <Toolbar variant="dense" style={{ marginLeft: 0, paddingLeft: 0 }}>
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={goBack}>
                        <MdArrowForward />
                    </IconButton>
                    <div className="rows" style={{ width: '100%' }}>
                        <div className="row float-right">
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                <img src={chatData.current.logoUrl ? chatData.current.logoUrl : WaitingPNG} style={{ backgroundColor: backgroundColor, width: 40, height: 40, borderRadius: 20, marginLeft: 12, marginTop: 0, objectFit: 'contain' }} />
                                <div style={{ display: 'flex', flexDirection: 'column' }}>
                                    <p className="ThirdBoldText" style={{ fontSize: 14, marginTop: 4, position: 'absolute', top: 2 }}>
                                        {chatData.current.name}
                                    </p>
                                    <div style={{ position: 'absolute', top: 2 }}>
                                        {isTyping ? (
                                            <p className="ThirdText" style={{ marginBottom: 2, marginRight: 0, width: 200, position: 'absolute', fontSize: 10, top: 26 }}>
                                                {writers.current} در حال نوشتن...
                                            </p>
                                        ) : (
                                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                                {onlineCount > 1 && <MdFiberManualRecord size={12} color={greenColor} style={{ marginTop: 4, position: 'absolute', top: 24 }} />}
                                                <p className="ThirdText" style={{ marginBottom: 2, marginRight: onlineCount > 1 ? 14 : 0, width: 100, position: 'absolute', fontSize: 10, top: 26 }}>
                                                    {onlineCount > 1 ? onlineCount + ' نفر آنلاین' : 'فقط شما آنلاین هستید'}
                                                </p>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                        {!isLoading && <div className={"row float-left" + (canAnimate == true ? " animate-flicker" : "")} style={{ marginLeft: 12, paddingTop: 4 }}>
                            <p className="PrimaryText" style={{ color: oneDayChange > 0 ? greenColor : accentcolor, textAlign: 'left', direction: 'ltr', fontSize: 16 }}>{oneDayChange > 0 ? '+' : ''}{(oneDayChange * 100 / price).toFixed(0)}%</p>
                            <p className="PrimaryText" style={{ position: 'absolute', top: 28, left: 12, fontSize: 10 }}>${price}</p>
                        </div>}
                    </div>
                </Toolbar>
            </AppBar>
            <Popover
                id={popoverId}
                open={openPopover}
                anchorEl={anchorEl}
                onClose={handleClosePopover}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'left',
                }}
            >
                <div style={{ padding: 4, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', textAlign: 'right' }}>
                    <div
                        onClick={() => {
                            handleClosePopover()
                            // setIsSendBlockMes(true)
                        }}
                        style={{ display: 'flex', alignItems: 'center', paddingRight: 8, paddingLeft: 8 }}
                    >
                        <MdBlock size={20} />
                        <Button variant="text">{isBlockedByMe ? 'آنبلاک' : 'بلاک'}</Button>
                    </div>
                </div>
            </Popover>
            {isShowingActions && (
                <Dialog open={isShowingActions} onClose={() => setIsShowingActions(false)} aria-labelledby="addeditcat">
                    <div style={{ display: 'flex', flexDirection: 'column', width: 200, padding: 24 }}>
                        <p className="ThirdLightText" style={{ marginBottom: 10 }}>
                            پیام : {selectedMessage.message.length > 0 ? selectedMessage.message : 'بدون متن'}
                        </p>
                        <div
                            className="clickable"
                            onClick={() => {
                                setIsShowingActions(false)
                                setReplyMessage(selectedMessage.message)
                                setReplyMessageId(selectedMessage.id)
                            }}
                            style={{ textAlign: 'right', display: 'flex', alignItems: 'center', paddingTop: 6, paddingBottom: 6 }}
                        >
                            <MdReply size={24} />
                            <p className="ThirdBoldText" style={{ marginRight: 8 }}>
                                پاسخ
                            </p>
                        </div>
                        <div
                            className="clickable"
                            onClick={() => {
                                setIsShowingActions(false)
                                copy(selectedMessage.message)
                                notifyCopied()
                            }}
                            style={{ textAlign: 'right', display: 'flex', alignItems: 'center', paddingTop: 6, paddingBottom: 6 }}
                        >
                            <MdContentCopy size={24} />
                            <p className="ThirdBoldText" style={{ marginRight: 8 }}>
                                کپی متن
                            </p>
                        </div>
                        {isUser && selectedMessage.senderUserId == account.id && (<div
                            className="clickable"
                            onClick={() => {
                                setIsShowingActions(false)
                                setIsSendDeleteMes(true)
                            }}
                            style={{ textAlign: 'right', display: 'flex', alignItems: 'center', paddingTop: 6, paddingBottom: 6 }}
                        >
                            <MdDelete size={24} />
                            <p className="ThirdBoldText" style={{ marginRight: 8 }}>
                                حذف پیام
                            </p>
                        </div>)}
                        {isUser && selectedMessage.senderUserId == account.id && (
                            <div
                                className="clickable"
                                onClick={() => {
                                    setIsShowingActions(false)
                                    setNewTextMessage(selectedMessage.message)
                                    setIsEditingMode(true)
                                }}
                                style={{ textAlign: 'right', display: 'flex', alignItems: 'center', paddingTop: 6, paddingBottom: 6 }}
                            >
                                <MdEdit size={24} />
                                <p className="ThirdBoldText" style={{ marginRight: 8 }}>
                                    ویرایش پیام
                                </p>
                            </div>
                        )}
                    </div>
                </Dialog>
            )}
            {isBlockedByFriend || isBlockedByMe ? (
                <div
                    style={{
                        width: '100%',
                        height: window.innerHeight,
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <MdBlock size={80} />
                    <p className="PrimaryLightText" style={{ marginBottom: 56 }}>
                        {isBlockedByMe ? 'بلاک کردید' : 'بلاک شدید'}
                    </p>
                </div>
            ) : (
                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%', height: '100%', minHeight: window.innerHeight < 500 ? 500 : window.innerHeight }}>
                    <div style={{ width: '100%', paddingTop: 40, paddingBottom: 48, height: '100%', overflow: 'hidden' }}>
                        <div style={{ width: '100%', height: '100%', display: 'flex', justifyContent: 'center' }}>
                            <List style={{ display: 'flex', flexDirection: 'column-reverse', flexWrap: 'nowrap', overflow: 'hidden', margin: 0, paddingBottom: 56, width: '100%', maxWidth: 500, height: '100%' }}>
                                {messages.map((item, index) => (
                                    <div
                                        className="clickable"
                                        id={item.id}
                                        onClick={(e) => {
                                            e.stopPropagation()
                                            setSelectedMessage(item)
                                            setSelectedMessageIndex(index)
                                            setIsShowingActions(true)
                                        }}
                                        style={{ margin: 0, padding: 0 }}
                                        key={item.id}
                                    >
                                        {index == messages.length - 1 && isLoading ? (
                                            <div style={{ top: 0 }}>
                                                <div style={{ width: '100%' }}>
                                                    <LinearProgress color="secondary" />
                                                </div>
                                            </div>
                                        ) : (
                                            <div></div>
                                        )}
                                        {index == messages.length - 1 && <div style={{ height: 16 }}></div>}
                                        <div>
                                            <div style={{ display: 'flex' }}>
                                                {isUser && account.id != item.senderUserId && <div style={{ width: '100%' }} />}
                                                <div style={{ width: 300 }}>
                                                    <div style={{ backgroundColor: isUser && account.id == item.senderUserId ? itemColor : '#11233F', marginTop: 8, marginBottom: 4, marginLeft: 8, marginRight: 8, boxShadow: '0 1px 2px 0px #111111', borderRadius: 4 }} elevation={1}>
                                                        <div style={{ maxWidth: 300, margin: 0, padding: 0 }}>
                                                            {(item.imagePath ?? '').length > 0 && <img className="image" src={webUrl + item.imagePath} style={{ width: 280, height: 300, margin: 0, padding: 0, zIndex: 202, borderTopRightRadius: 4, borderTopLeftRadius: 4 }} onClick={() => pushToImageViewer(item.imagePath)} />}
                                                            <div className="SecondaryText multi-line" style={{ padding: 8, whiteSpace: 'pre-line', marginBottom: 8, fontSize: 13, lineHeight: 1.8, width: getMessageWidth(item.message.length, item.voicePath, item.imagePath) }}>
                                                                {item.message.length > 0 && (
                                                                    <ReactTextFormat >
                                                                        {item.message}
                                                                    </ReactTextFormat>
                                                                )}
                                                                {(item.voicePath ?? '').length > 0 && (
                                                                    <div style={{ width: 267, marginBottom: 4 }}>
                                                                        <audio controls style={{ width: '100%' }}>
                                                                            <source src={webUrl + item.voicePath} type="audio/aac" />
                                                                        </audio>
                                                                    </div>
                                                                )}
                                                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                                                    {isUser && account.id != item.senderUserId ? (
                                                                        <div />
                                                                    ) : item.isLoadingToSend ? (
                                                                        <CircularProgress color="secondary" size={16} style={{ marginLeft: 8 }} />
                                                                    ) : item.isSeen ? (
                                                                        <MdDoneAll size={16} style={{ marginLeft: 6 }} color={greenColor} />
                                                                    ) : (
                                                                        <MdDone size={16} style={{ marginLeft: 6 }} color="grey" />
                                                                    )}
                                                                    <p className="ThirdLightText" >
                                                                        {parseDate(item.dateCreate)}{isUser && account.id != item.senderUserId ? ' از ' + item.senderName : ''}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {isUser && account.id != item.senderUserId && <img onClick={(e) => {
                                                    e.stopPropagation()
                                                    history.push({ pathname: 'user', search: '?id=' + item.senderUserId })
                                                    setSelectedUserId(item.senderUserId)
                                                    // setShowingProfileDialog(true)
                                                }} style={{ width: 50, height: 50, color: 'white', objectFit: 'cover', borderRadius: 25, marginLeft: 12 }} src={item.senderAvatar ? webUrl + item.senderAvatar : AvatarPNG} />}
                                                {isUser && account.id == item.senderUserId && <div style={{ width: 'calc(100% - 50px)' }} />}
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </List>
                        </div>
                        <div
                            style={{
                                visibility: isRecording || isVoiceFileGet ? 'visible' : 'hidden',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                                borderRadius: 8,
                                backgroundColor: 'white',
                                width: 60,
                                height: 30,
                                position: 'fixed',
                                bottom: (selectedImageFile != null ? 164 + messageHeight - (isAndroid || isIOS ? 30 : 15) : 56 + messageHeight - 30) + (replyMessage.length > 0 ? 30 : 0),
                                left: selectedImageFile != null ? isAndroid || isIOS ? 8 : props.width/4 : 32,
                                fontFamily: 'iransansMedium',
                            }}
                        >
                            <Timer
                                startImmediately="false"
                                ref={(node) => {
                                    timerRef = node
                                }}
                            >
                                <Timer.Seconds />
                                <p> &nbsp;:&nbsp; </p>
                                <Timer.Minutes />
                            </Timer>
                        </div>
                        {selectedImageFile != null && (
                            <div
                                style={{
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    borderRadius: 8,
                                    backgroundColor: 'white',
                                    width: 100,
                                    height: 100,
                                    position: 'fixed',
                                    bottom: 56 + messageHeight - (isAndroid || isIOS ? 30 : 15) + (replyMessage.length > 0 ? 30 : 0),
                                    left: isAndroid || isIOS ? 8 : props.width/4,
                                }}
                            >
                                {selectedImageFile && (
                                    <div className="StackContainer">
                                        <MdClose size={20} onClick={removeImage} className="StackElement" style={{ backgroundColor: 'white', borderRadius: 4 }} />
                                        <img className="image" src={imagePreview} style={{ borderRadius: 4, width: 100, height: 100 }} />
                                    </div>
                                )}
                            </div>
                        )}
                    </div>
                    <div style={{
                        display: 'flex', justifyContent: 'center',
                        width: '100%',
                        right: 0,
                        left: 0,
                        bottom: messageHeight + (isAndroid || isIOS ? 20 : 40),
                        position: 'fixed'
                    }}>
                        <div
                            style={{
                                visibility: replyMessage.length > 0 || isEditingMode ? 'visible' : 'hidden',
                                display: 'flex',
                                alignItems: 'center',
                                backgroundColor: '#4e6994',
                                maxWidth: 554,
                                borderRadius: (isAndroid || isIOS ? 0 : 8),
                                width: '100%',
                                height: 30,
                                fontFamily: 'iransansMedium',
                                paddingRight: 16,
                                paddingLeft: 16,
                            }}
                        >
                            <MdClose
                                onClick={() => {
                                    setNewTextMessage('')
                                    setReplyMessage('')
                                    setReplyMessageId(0)
                                    setIsEditingMode(false)
                                }}
                                size={24}
                                color="#eeeeee"
                                style={{ marginLeft: 8 }}
                            />
                            <p className="ThirdLightText" style={{ color: '#eeeeee' }}>
                                {isEditingMode ? 'ویرایش : ' : 'در پاسخ به : '}
                                {selectedMessage != null ? selectedMessage.message : ''}
                            </p>
                        </div>
                    </div>
                    {isUser ? (
                        <div style={{ position: 'fixed', width: '100%', bottom: (isAndroid || isIOS) ? 0 : 16, display: 'flex', justifyContent: 'center' }}>
                            <div style={{ display: 'flex', alignItems: 'center', borderRadius: (isAndroid || isIOS) ? 0 : 8, backgroundColor: '#212121', width: '100%', maxWidth: 600, height: messageHeight + 10, minHeight: 48, position: 'relative' }}>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', width: 40, marginRight: 8, height: 48 }}>
                                    {isLoadingForUpload ? <CircularProgress color="secondary" size={16} /> : <MdSend size={24} color="white" onClick={() => (isEditingMode ? setIsSendEditMes(true) : setIsSendMessage(true))} />}
                                </div>
                                <div style={{ width: '100%', marginLeft: 24 }}>
                                    <textarea placeholder="متن پیام" value={newTextMessage} onChange={changeNewTextMessage} style={{ color: 'white', backgroundColor: 'transparent', height: messageHeight, width: '75%', fontSize: '16px', border: 0, borderWidth: 0, marginRight: 8 }} />
                                </div>
                                <div style={{ display: 'flex', left: 0, position: 'absolute', marginLeft: 16 }}>
                                    <IconButton edge="end" color="white">
                                        {isRecording ? <MdPauseCircleFilled onClick={startStopRecording} /> : isVoiceFileGet ? <MdStop onClick={resetVoiceRecorder} /> : <MdKeyboardVoice onClick={startStopRecording} />}
                                    </IconButton>
                                    <IconButton edge="end" color="white">
                                        <input id="fileButton" type="file" hidden onChange={onSelectFile} />
                                        <MdImage onClick={fileUploadButton} />
                                    </IconButton>
                                </div>
                            </div>
                        </div>) : (<div onClick={() => {
                            if (account && account.id) {
                                history.replace('/#5')
                            } else {
                                history.push('/login')
                            }

                        }} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundColor: '#212121', width: '100%', height: messageHeight + 10, minHeight: 48, position: 'fixed', bottom: 0 }}>
                            <p className="ThirdLightText" style={{ fontSize: 10 }}>کاربر گرامی؛ لطفا اطلاعات حساب خود را تکمیل نمایید.</p><Button>ورود | عضویت</Button></div>)}
                </div>
            )
            }
            {
                messages.length == 0 && isLoading ? (
                    <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                        <CircularProgress color="white" />
                    </Backdrop>
                ) : (
                    <div />
                )
            }
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div >
    )
}

export default CoinRoomMessages
