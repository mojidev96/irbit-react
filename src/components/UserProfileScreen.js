import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    Card,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    GridListTile,
    InputBase,
} from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdClose, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew, MdSearch } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { accentcolor, account, notifySuccessful, backgroundColor, blueColor, globalTabIndex, greenColor, itemColor, lightIconColor, notifyErrorMessage, token, webUrl } from '../App'
import { parseDistance, parseDate, parseNumber } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'
import ProfileScreen from './ProfileScreen'
import TryAgainWidget from '../widgets/TryAgainWidget'
import { isAndroid, isIOS } from 'react-device-detect'
import VideoThumbnail from 'react-video-thumbnail';
import AvatarPNG from '../icons/avatar.png'
import ArroDownIcon from '../icons/down-arrow.png'
import ViewIcon from '../icons/view.png'
import ArroUpOutlineIcon from '../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../icons/down-arrow-outline.png'
import LoadingDialog from './dialogs/LoadingDialog'
import PullToRefresh from 'react-simple-pull-to-refresh'
import NewsDialog from './dialogs/NewsDialog'
import NewPostDialog from './dialogs/NewPostDialog'

function UserProfileScreen(props) {
    let history = useHistory()
    const [isLoading, setIsLoading] = useState(true)
    const posts = useRef([])
    const userId = useRef(0)
    const profileData = useRef()
    const [cellWidth, setCellWidth] = useState(200)
    const isUser = useRef(false)
    const [isFollowed, setIsFollowed] = useState(false)

    useEffect(() => {
        const search = props.location.search
        const params = new URLSearchParams(search)
        userId.current = params.get('id')
        isUser.current = account != null && account.id != null && token != null && account.name != null
        getProfileData()
    }, [])

    useEffect(() => {
        setCellWidth(props.width > 600 ? (600 / 3) : (props.width / 3))
    }, [props.width])

    const getProfileData = async () => {
        setItHasProblem(false)
        setIsLoading(true)

        try {
            const response = await Axios.post('users/getById', {
                userId: userId.current,
            }, { headers: { token: token } })

            if (response.status == 201) {
                profileData.current = response.data.profile
                setIsFollowed(response.data.profile.isFollowed ?? false)
                posts.current.push(...response.data.posts)
            } else {
                setItHasProblem(true)
            }
        } catch (error) {
            setItHasProblem(true)
        }
        setIsLoading(false)
    }

    const [itHasProblem, setItHasProblem] = useState(false)
    const [isLoadingFollowUnfollow, setIsLoadingFollowUnFollow] = useState(false)

    const goBack = () => {
        if (history.length < 3) {
            history.replace('/')
        } else {
            history.goBack()
        }
    }

    const followUnfollow = async () => {
        setIsLoadingFollowUnFollow(true)
        try {
            //1=follow, 2=unfollow
            const response = await Axios.post('follows', {
                id: userId.current,
                type: isFollowed ? 2 : 1
            })

            if (response.status == 201) {
                profileData.current.followersCount = (!isFollowed ? profileData.current.followersCount + 1 : profileData.current.followersCount - 1)
                setIsFollowed(!isFollowed)
                notifySuccessful()
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            notifyErrorMessage('اشکال در برقراری ارتباط')
        }
        setIsLoadingFollowUnFollow(false)
    }

    // handle showing newPost dialog screen
    const [isShowingNewPostDialog, setShowingNewPostDialog] = useState(false)
    const showNewPostDialog = (isShowing) => {
        if (isShowing) {
            return <NewPostDialog data={{ onNewPostDone, onCloseFunc: closeNewPostDialog }} />
        } else {
            return <div />
        }
    }

    const onNewPostDone = (data) => {
        posts.current.insert(0, data)
        ++profileData.current.postsCount
        setShowingNewPostDialog(false)
        notifySuccessful()
    }

    const closeNewPostDialog = () => {
        setShowingNewPostDialog(false)
    }
    // end of newPost dialog screen

    const pushMessages = (byUserId, name, avatar, lastOnline) => {
        history.push({
            pathname: '/messages',
            data: {
                chatId: 0,
                byUserId: byUserId,
                avatar: avatar,
                name: name,
                lastOnline: lastOnline,
            },
        })
    }

    return (
        <div style={{ overflow: 'hidden', direction: 'rtl', display: 'flex', justifyContent: 'center' }}>
            {showNewPostDialog(isShowingNewPostDialog)}
            <AppBar position="fixed" style={{ backgroundColor: itemColor }}>
                <Toolbar variant="dense" style={{ marginLeft: 0, paddingLeft: 0 }}>
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={goBack}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography className="PrimaryText" variant="h6" color="inherit">
                        {!isLoading && profileData.current != undefined ? profileData.current.username : 'در حال بارگزاری'}
                    </Typography>
                </Toolbar>
            </AppBar>
            {!isLoading && itHasProblem ? (
                <TryAgainWidget onTryAgain={getProfileData} />
            ) : isLoading ? (
                <LoadingDialog />
            ) : (
                <div style={{ paddingTop: 48, width: '100%', maxWidth: 600, overflowX: 'hidden', paddingRight: 16, paddingLeft: 16 }}>
                    <div style={{ width: '100%', display: 'flex', marginTop: 16, marginBottom: 24, alignItems: 'center' }}>
                        <img className="avatar_image" src={profileData.current.avatar ? webUrl + profileData.current.avatar : AvatarPNG} style={{ width: 100, height: 100, borderRadius: 50 }} />
                        <div style={{ flex: 1, marginRight: 8 }}>
                            <div style={{ display: 'flex', alignItems: 'center' }}>
                                <div className="clickable" onClick={() => {
                                    if (isUser.current == true) {
                                        history.push({ pathname: 'follows/list', search: '?id=' + userId.current + '&type=' + 1 })
                                    } else {
                                        history.push('login')
                                    }
                                }} style={{ flex: 1 }}>
                                    <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 6, flex: 1, fontSize: 12 }}>
                                        {profileData.current.followersCount}
                                    </p>
                                    <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 6, flex: 1, fontSize: 12 }}>
                                        دنبال کننده
                                    </p>
                                </div>
                                <div className="clickable" onClick={() => {
                                    if (isUser.current == true) {
                                        history.push({ pathname: 'follows/list', search: '?id=' + userId.current + '&type=' + 2 })
                                    } else {
                                        history.push('login')
                                    }
                                }} style={{ flex: 1 }}>
                                    <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 0, flex: 1, fontSize: 12 }}>
                                        {profileData.current.followingCount}
                                    </p>
                                    <p className="SecondaryText" style={{ textAlign: 'center', paddingLeft: 0, flex: 1, fontSize: 12 }}>
                                        دنبال می کند
                                    </p>
                                </div>
                                <div style={{ flex: 1 }}>
                                    <p className="SecondaryText" style={{ textAlign: 'center', fontSize: 12 }}>
                                        {profileData.current.postsCount}
                                    </p>
                                    <p className="SecondaryText" style={{ textAlign: 'center', fontSize: 12 }}>
                                        پست
                                    </p>
                                </div>
                            </div>
                            {isUser.current === true && account.id == userId.current ? <Button varia="contained" fullwidth onClick={() => {
                                if (localStorage.getItem('token') == null) {
                                    history.push('/login')
                                } else {
                                    setShowingNewPostDialog(true)
                                }
                            }} style={{ width: '100%', marginRight: 4, backgroundColor: blueColor, color: 'white', marginTop: 12 }}>پست جدید</Button> : <div style={{ display: 'flex', marginTop: 12, alignItems: 'center' }}>
                                <Button varia="contained" fullwidth onClick={() => pushMessages(userId.current, profileData.current.name, profileData.current.avatar, profileData.current.dateLastOnline)} style={{ width: '100%', marginLeft: 4, backgroundColor: greenColor, color: 'white' }}>ارسال پیام</Button>
                                <Button disabled={isLoadingFollowUnfollow} variant={isFollowed ? 'outlined' : 'contained'} fullwidth onClick={(e) => {
                                    if (isUser.current == true) {
                                        followUnfollow()
                                    } else {
                                        history.push('login')
                                    }
                                }} style={{ width: '100%', marginRight: 4, backgroundColor: isFollowed ? backgroundColor : blueColor, color: 'white' }}>{isLoadingFollowUnfollow ? <CircularProgress size={24} style={{ color: 'white' }} /> : isFollowed ? 'آنفالو' : 'دنبال کردن'}</Button>
                            </div>
                            }
                        </div>
                    </div>
                    <p className="PrimaryText">{profileData.current.name}</p>
                    <p className="ThirdText">{profileData.current.about}</p>

                    <div className="StackContainer" style={{ overflow: 'hidden', width: '100%' }}>
                        <div style={{ overflow: 'hidden', paddingTop: 16, width: '100%', display: 'flex', justifyContent: 'center' }}>
                            <GridList id="gridPostList" cellHeight={cellWidth} cols={3} style={{ width: '100%', paddingTop: 4, paddingBottom: 32, overflow: 'hidden', height: (Math.ceil(posts.current.length / 3)) * (cellWidth) }}>
                                {posts.current.map((item, index) => (
                                    <Grid container> <div onClick={() => {
                                        history.push({ pathname: 'post', search: '?id=' + item.id })
                                    }} style={{ width: '100%', height: cellWidth, overflow: 'hidden' }} >
                                        <img className="image clickable" src={webUrl + item.posterUrl} style={{ width: '100%', height: cellWidth }} />
                                    </div></Grid>
                                ))}
                            </GridList>
                        </div>
                    </div>
                    {isLoading && posts.current.length > 0 ? (
                        <div style={{ width: '100%', zIndex: 200 }}>
                            <div style={{ width: '100%' }}>
                                <LinearProgress color="secondary" />
                            </div>
                        </div>
                    ) : (
                        <div></div>
                    )}
                </div>
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default UserProfileScreen
