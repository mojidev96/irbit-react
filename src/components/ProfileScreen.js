import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, IconButton, TextField, Checkbox, RadioGroup, Radio, Button, CircularProgress, Divider } from '@material-ui/core'
import { Bar } from 'react-chartjs-2'
import { accentcolor, account, websiteUrl, blueColor, greenColor, notifyCopied, webUrl, token } from '../App'
import Axios from 'axios'
import { MdArrowForward, MdBookmarkBorder, MdCheckCircle, MdChat, MdExitToApp, MdInfo, MdMoreVert, MdOpenInNew, MdCall, MdStore, MdReport, MdShare, MdClose } from 'react-icons/md'
import { parseDistance, parsePrice, parseLikeOrDislikes, parseDate } from '../tools/extentions'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import copy from 'copy-to-clipboard'

function ProfileScreen(props) {
    let history = useHistory()
    const userId = useRef(0)
    const data = useRef(0)
    const [isLoading, setLoading] = useState()
    const isFromLocalScreens = useRef(false)

    const [anchorEl, setAnchorEl] = React.useState(null)

    const handleClickOpenPopover = (event) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClosePopover = () => {
        setAnchorEl(null)
    }

    const openPopover = Boolean(anchorEl)
    const popoverId = openPopover ? 'simple-popover' : undefined

    useEffect(() => {
        // //console.log('wg3:: ' +' --- ' + ' --- ' + props.location.state.detail)
        var id = 0
        if (props.data == null) {
            const search = props.location.search
            const params = new URLSearchParams(search)
            id = params.get('id')
        } else {
            isFromLocalScreens.current = true
            id = props.data.id
        }

        userId.current = id
        getUserById(id)
    }, [])

    const getUserById = async (id) => {

        setLoading(true)
        try {
            const response = await Axios.post('users/getById', {
                userId: id,
            },
                { headers: { token: token } })
            data.current = response.data
        } catch (error) {
            // console.log('gfgergerg', error)
        }
        setLoading(false)
    }

    const onCloseScreen = () => {
        isFromLocalScreens ? props.data.onCloseFunc() : history.goBack()
    }

    const pushMessages = (byUserId, name, avatar, lastOnline) => {
        history.push({
            pathname: '/messages',
            data: {
                chatId: 0,
                byUserId: byUserId,
                avatar: avatar,
                name: name,
                lastOnline: lastOnline,
            },
        })
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="sm" aria-labelledby="profileDialog">
            <div>
                {isLoading ? (
                    <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                        <CircularProgress color="white" />
                    </Backdrop>
                ) : (
                    <div style={{ display: 'flex', flexDirection: 'column' }}>
                        <div className="StackContainer">
                            <img className="image" src={data.current.avatar != null && data.current.avatar.length > 0 ? webUrl + data.current.avatar : null} style={{ width: '100%', minWidth: 250, maxHeight: 500, minHeight: 150 }} />
                            <MdClose className="StackElement" size={20} onClick={onCloseScreen} style={{ position: 'fixed', marginRight: 8, marginTop: 8 }} />
                        </div>
                        <div style={{ display: 'flex', flexDirection: 'column', paddingLeft: 8, paddingRight: 16 }}>
                            <div className="rows" style={{ display: 'inline', alignItems: 'center', marginTop: 8 }}>
                                <p className="float-right PrimaryText">{data.current.name}</p>
                                <div className="float-left">
                                    <MdMoreVert aria-describedby={popoverId} onClick={handleClickOpenPopover} size={24} style={{ marginTop: 4 }} />
                                </div>
                            </div>
                            <ul style={{ paddingBottom: 0, marginBottom: 0, paddingTop: 0, marginTop: 0, paddingRight: 0, paddingLeft: 16, alignItems: 'flex-start', alignContent: 'flex-start', display: 'flex', flexWrap: 'wrap', listStyleType: 'none' }}>
                                <li>
                                    <p className="ThirdLightText">{parseDate(data.current.dateLastOnline)}</p>
                                </li>
                                {data.current.job != null && (
                                    <li>
                                        <p className="ThirdBoldText" style={{ color: accentcolor, marginRight: 8 }}>
                                            {data.current.job}
                                        </p>
                                    </li>
                                )}
                                {data.current.city != null && (
                                    <li>
                                        <p className="ThirdLightText" style={{ marginRight: 8 }}>
                                            {data.current.city}
                                        </p>
                                    </li>
                                )}
                                <li>
                                    <p className="ThirdLightText" style={{ marginRight: 8 }}>
                                        {parseDistance(data.current.distance ?? 0)}
                                    </p>
                                </li>
                            </ul>
                            <p className="SecondaryLightText" style={{ marginLeft: 16, marginTop: 2 }}>
                                {data.current.about}
                            </p>

                            <ul style={{ paddingRight: 16, paddingLeft: 8, alignItems: 'flex-start', alignContent: 'flex-start', flexWrap: 'wrap', listStyleType: 'none' }}>
                                {(localStorage.getItem('account') == null || account.id != data.current.id) && data.current.phone != null ? (
                                    <li>
                                        <Button variant="contained" onClick={() => (window.location = 'tel://' + data.current.phone)} style={{ background: greenColor, color: 'white', marginRight: 6 }}>
                                            <MdCall size={20} color="white" />
                                        </Button>
                                    </li>
                                ) : (
                                    <div />
                                )}
                                {localStorage.getItem('account') == null || account.id != data.current.id ? (
                                    <li>
                                        <Button variant="contained" onClick={() => pushMessages(data.current.id, data.current.name, data.current.avatar, data.current.dateLastOnline)} style={{ background: greenColor, color: 'white', marginRight: 6 }}>
                                            <MdChat size={20} color="white" />
                                        </Button>
                                    </li>
                                ) : (
                                    <div />
                                )}
                            </ul>
                            <Popover
                                id={popoverId}
                                open={openPopover}
                                anchorEl={anchorEl}
                                onClose={handleClosePopover}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                }}
                                transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                            >
                                <div style={{ padding: 4, display: 'flex', flexDirection: 'column', alignItems: 'flex-start', alignContent: 'flex-start', textAlign: 'right' }}>
                                    {/* <div
                                        onClick={() => {
                                            handleClosePopover()
                                        }}
                                        style={{ display: 'flex', alignItems: 'center', paddingRight: 8, paddingLeft: 8 }}
                                    >
                                        <MdReport size={20} />
                                        <Button variant="text">گزارش کاربر</Button>
                                    </div> */}
                                    <div
                                        onClick={() => {
                                            copy(websiteUrl + 'users?id=' + userId.current)
                                            notifyCopied()
                                            handleClosePopover()
                                        }}
                                        style={{ display: 'flex', alignItems: 'center', paddingRight: 8, paddingLeft: 8 }}
                                    >
                                        <MdShare size={20} />
                                        <Button variant="text">اشتراک گذاری</Button>
                                    </div>
                                </div>
                            </Popover>
                        </div>
                    </div>
                )}
                <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
            </div>
        </Dialog>
    )
}

export default ProfileScreen
