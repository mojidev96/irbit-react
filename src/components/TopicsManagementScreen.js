import React, { useEffect, useState, useRef, createRef } from 'react'
import { AppBar, Toolbar, Typography, Backdrop, Fab, IconButton, Button, CircularProgress, Divider, Popover, Card, LinearProgress } from '@material-ui/core'
import { MdAdd, MdArrowBack, MdArrowForward, MdBookmarkBorder, MdDelete, MdEdit, MdExitToApp, MdKeyboardArrowLeft, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { backgroundColor, greenColor, token, account, notifyWarnMessage, webUrl, accentcolor, notifyErrorMessage, notifyError } from '../App'
import { parsePrice, parseDate, parseNumber } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import EmptyView from '../widgets/EmptyView'
import { isIOS, isAndroid } from 'react-device-detect'
import ArroUpIcon from '../icons/up-arrow.png'
import ArroDownIcon from '../icons/down-arrow.png'
import ViewIcon from '../icons/view.png'
import ChatIcon from '../icons/chat.png'
import ArroUpOutlineIcon from '../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../icons/down-arrow-outline.png'
import ConfirmationDialog from './dialogs/ConfirmationDialog'

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function TopicsManagementScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const page = useRef(1)
    const wordToFind = useRef('')
    const [wordToFindShow, setWordToFindShow] = useState('')

    const [isLoading, setLoading] = useState(true)
    const isAllGot = useRef(false)
    const [topics, setTopics] = useState([])

    useEffect(() => {
        if (props.location.data == null) {
            wordToFind.current = localStorage.getItem('ca-nam-sea-tps-y43h')
            page.current = localStorage.getItem('tps-pa-oi904t4')
            setTopics(JSON.parse(localStorage.getItem('tps-reusable-78ygg34')))
            setLoading(false)
            try {
                setTimeout(() => {
                    window.scrollTo({ top: localStorage.getItem('hgwg4533434hg4') > 100 ? localStorage.getItem('hgwg4533434hg4') : 0 })
                }, 100)
            } catch (e) {}
        } else {
            localStorage.setItem('ghwo-883-3ff-ggtg433', props.location.data.title)
            localStorage.setItem('ca-nam-sea-tps-y43h', '')
            localStorage.setItem('jg38f2ow389', 0)
            localStorage.setItem('tps-pa-oi904t4', 1)
            localStorage.setItem('tps-reusable-78ygg34', '[]')
            getTopics()
        }
        return () => {
            // Anything in here is fired on component unmount.
            window.onscroll = ''
        }
    }, [])

    const getTopics = async () => {
        if (isAllGot.current) {
            return
        }
        setLoading(true)
        try {
            const response = await Axios.post('societyTopics/mine', {
                page: page.current,
                word: wordToFind.current,
            })

            // console.log(response)
            if (response.data instanceof Array && response.data.length > 0) {
                topics.push(...response.data)
                setTopics(topics)
                ++page.current
                localStorage.setItem('tps-reusable-78ygg34', JSON.stringify(topics))
                localStorage.setItem('tps-pa-oi904t4', page.current)
            } else {
                isAllGot.current = true
            }

            setLoading(false)
        } catch (error) {
            //console.log('err filters :: ', error)
            setLoading(false)
        }
    }

    const [idToRemove, setIdToRemove] = useState(0)
    const deleteTopic = async () => {
        // console.log('egww:: ' + idToRemove)
        setLoading(true)
        try {
            const response = await Axios.delete('societyTopics', { data: { id: idToRemove } })

            if (response.status == 200) {
                reloadList()
            } else {
                notifyError()
            }

            setLoading(false)
        } catch (error) {
            // console.log('err filters :: ', error)
            notifyError()
            setLoading(false)
        }
    }

    const reloadList = () => {
        page.current = 1
        isAllGot.current = false
        setTopics([])
        topics.length = 0
        getTopics()
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        localStorage.setItem('hgwg4533434hg4', window.pageYOffset)
        try {
            if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridTopics').offsetHeight) {
                isWorkingScroll.current = 1
                getTopics()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    const [upDownLoadingId, setUpDownLoadingId] = useState(0)
    const upOrDown = async (index, id, isUp, isForgive) => {
        setUpDownLoadingId(id)
        console.log('eeeqq: ' + isUp + ' -- ' + isForgive)
        try {
            const response = await Axios.post('societyUpAndDowns', {
                isUp,
                isForgive,
                contentId: id,
                contentType: 1,
            })
            if (response.status == 201) {
                if (isUp == true) {
                    if (topics[index].iDown != null && topics[index].iDown == true) {
                        topics[index].iDown = false
                        topics[index].downs -= 1
                    }
                } else {
                    if (topics[index].iUp != null && topics[index].iUp == true) {
                        topics[index].iUp = false
                        topics[index].ups -= 1
                    }
                }

                if (isUp == true) {
                    topics[index].iUp = isForgive == false
                } else {
                    topics[index].iDown = isForgive == false
                }

                if (isUp == true) {
                    if (isForgive == false) {
                        topics[index].ups += 1
                    } else {
                        topics[index].ups -= 1
                    }
                } else {
                    if (isForgive == false) {
                        topics[index].downs += 1
                    } else {
                        topics[index].downs -= 1
                    }
                }
                setTopics(topics)
            } else {
                notifyErrorMessage('نشد که بشه... لطفا دوباره امتحان کنید.')
            }
        } catch (error) {}
        setUpDownLoadingId(0)
    }

    // handle confirmation delete topic dialog ::::::::
    const [isShowingConfirmationDialog, setShowingConfirmationDialog] = useState(false)
    const showConfirmationDialog = (isShowing) => {
        if (isShowing) {
            return (
                <ConfirmationDialog
                    data={{
                        onCloseFunc: closeConfirmationDialog,
                        ok,
                        cancel,
                        title: 'حذف موضوع',
                        message: 'این عملیات غیرقابل بازگشت است. اطمینان دارید؟',
                    }}
                />
            )
        } else {
            return <div />
        }
    }

    const cancel = () => {
        setShowingConfirmationDialog(false)
    }

    const ok = () => {
        deleteTopic()
        setShowingConfirmationDialog(false)
    }

    const closeConfirmationDialog = () => {
        setShowingConfirmationDialog(false)
    }
    // end handle confirmation delete topic dialog ----------

    return (
        <div className={classes.root} style={{ overflow: 'hidden', display: 'flex', justifyContent: 'center' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        پست های انجمن من
                    </Typography>
                </Toolbar>
            </AppBar>

            {showConfirmationDialog(isShowingConfirmationDialog)}
            {topics && topics.length == 0 && !isLoading ? (
                <EmptyView />
            ) : (
                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%', backgroundColor: backgroundColor }}>
                    <div style={{ paddingTop: 56, width: '100%', overflow: 'hidden' }}>
                        <div
                            className="masonry-container"
                            id="gridTopics"
                            style={{
                                width: '100%',
                                display: 'inline-block',
                                alignContent: 'center',
                                justifyContent: 'center',
                                minHeight: props.height,
                                columnCount: isAndroid || isIOS ? 1 : Math.round(props.width / 300),
                                paddingRight: !isAndroid && !isIOS ? 4 : 0,
                                paddingLeft: !isAndroid && !isIOS ? 4 : 0,
                            }}
                        >
                            {topics.map((item, index) => (
                                <Card
                                    className={'masonry-item clickable ' + (!isAndroid && !isIOS ? 'cardHover' : '')}
                                    key={item.id}
                                    variant="elevation"
                                    elevation={3}
                                    style={{
                                        width: isAndroid || isIOS ? '100%' : 284,
                                        height: '100%',
                                        marginTop: 8,
                                        borderRadius: isAndroid || isIOS ? 0 : 4,
                                        marginRight: isAndroid || isIOS ? '16px !important' : 0,
                                        marginLeft: isAndroid || isIOS ? '16px !important' : 0,
                                        marginBottom: 8,
                                        padding: 0,
                                        position: 'relative',
                                        boxShadow: '0 1px 3px 0px #000000',
                                    }}
                                    onClick={() => {
                                        history.push({ pathname: '/society/topics/messages', search: '?id=' + item.id, data: { title: item.title } })
                                    }}
                                >
                                    {item.banner != null && item.banner.length > 0 && <img src={webUrl + item.banner} style={{ width: '100%', maxHeight: 300, marginTop: -2, borderTopLeftRadius: 4, borderTopRightRadius: 4, objectFit: 'cover' }} />}
                                    <div style={{ marginTop: item.banner != null && item.banner.length > 0 ? 4 : 12 }}>
                                        <div style={{ marginRight: 12, marginLeft: 12 }}>
                                            <h1 className="PrimaryText" style={{ marginTop: 8 }}>
                                                {item.title}
                                            </h1>
                                            <p className="multi-line-truncate" style={{ fontFamily: 'iransansLight', fontSize: 12, margin: 0, padding: 0, color: '#CECECE', textAlign: 'justify' }}>
                                                {item.description}
                                            </p>

                                            <div style={{ display: 'flex', alignItems: 'center', direction: 'ltr', marginTop: 12, marginBottom: 4 }}>
                                                {upDownLoadingId != item.id ? (
                                                    <img
                                                        className="clickHoverJustTooZoom"
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            upOrDown(index, item.id, false, item.iDown != null && item.iDown == true)
                                                        }}
                                                        src={item.iDown && item.iDown == true ? ArroDownIcon : ArroDownOutlineIcon}
                                                        style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16 }}
                                                    />
                                                ) : (
                                                    <CircularProgress size={16} style={{ marginRight: 4 }} color="secondary" />
                                                )}
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.downs)}
                                                </p>
                                                {upDownLoadingId != item.id ? (
                                                    <img
                                                        className="clickHoverJustTooZoom"
                                                        onClick={(e) => {
                                                            e.stopPropagation()
                                                            upOrDown(index, item.id, true, item.iUp != null && item.iUp == true)
                                                        }}
                                                        src={item.iUp && item.iUp == true ? ArroUpIcon : ArroUpOutlineIcon}
                                                        style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }}
                                                    />
                                                ) : (
                                                    <CircularProgress size={16} style={{ marginRight: 4, marginLeft: 4 }} color="secondary" />
                                                )}
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.ups)}
                                                </p>

                                                <img src={ViewIcon} style={{ colorAdjust: 'red', opacity: 0.5, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.seenCount)}
                                                </p>

                                                <img src={ChatIcon} style={{ colorAdjust: 'red', opacity: 0.5, width: 16, height: 16, marginLeft: 12 }} />
                                                <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                    {parseNumber(item.messagesCount)}
                                                </p>

                                                <MdDelete
                                                    size={18}
                                                    onClick={(e) => {
                                                        e.stopPropagation()
                                                        setIdToRemove(item.id)
                                                        setShowingConfirmationDialog(true)
                                                    }}
                                                    style={{ float: 'right', opacity: 0.3, marginLeft: 8, direction: 'rtl' }}
                                                />
                                            </div>
                                            <Divider style={{ marginTop: 10, marginBottom: -2 }} />
                                        </div>
                                    </div>
                                </Card>
                            ))}
                        </div>
                    </div>
                </div>
            )}
            {topics.length == 0 && isLoading ? (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            ) : (
                <div />
            )}
            {isLoading && topics.length > 0 ? (
                <div style={{ position: 'fixed', width: '100%', bottom: 0 }}>
                    <div style={{ width: '100%' }}>
                        <LinearProgress color="secondary" />
                    </div>
                </div>
            ) : (
                <div></div>
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
            <div style={{ position: 'fixed', bottom: 16, left: 16 }}>
                <Fab color="secondary" aria-label="addTopic" onClick={() => history.push('/society')}>
                    <MdAdd size={24} />
                </Fab>
            </div>
        </div>
    )
}

export default TopicsManagementScreen
