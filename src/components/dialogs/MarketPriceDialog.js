import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, IconButton, TextField, Checkbox, RadioGroup, Radio, Button, CircularProgress, Divider } from '@material-ui/core'
import { Bar } from 'react-chartjs-2'
import { notifySuccessful, notifyError, notifyLoginNeeded, notifyErrorMessage, accentcolor, greenColor, blueColor, backgroundColor } from '../../App'
import Axios from 'axios'
import { MdArrowForward, MdBookmarkBorder, MdCheckCircle, MdChat, MdExitToApp, MdInfo, MdMoreVert, MdOpenInNew, MdCall, MdStore, MdReport, MdShare, MdClose } from 'react-icons/md'
import { parseDistance, parsePrice, parseLikeOrDislikes, parseDate, parseNumber } from '../../tools/extentions'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import copy from 'copy-to-clipboard'

function MarketPriceDialog(props) {
    const [data, setData] = useState()
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        setData(props.data.data)
        setTimeout(() => {
            setLoading(false)
        }, 200);
    }, [])

    const onCloseScreen = () => {
        props.data.onCloseFunc()
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="lg" aria-labelledby="coinDialog" style={{borderRadius: 16}}>
            {isLoading ? <div style={{ width: '100%', height: '100%' }}>
                <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            </div> : <div style={{ width: 400, backgroundColor: backgroundColor, padding: 24, display: 'flex' }}>
                <img src={data.logoUrl} style={{ height: 80, width: 80 }} />
                <div style={{ flex: 1, paddingTop: 8, paddingRight: 8, paddingLeft: 8, marginRight: 4, marginLeft: 4 }}>
                    <div style={{ display: 'flex', alignItems: 'start' }}>
                        <h4 className="PrimaryText">{data.symbol}</h4>
                        <p className="ThirdText" style={{ marginTop: 6, marginRight: 2, fontSize: 10 }}>{data.name}</p>
                        {data.oneDayChange && <p className={"PrimaryText" + (data.canAnimate && data.canAnimate == true ? " animate-flicker" : "")} style={{ color: data.oneDayChange > 0 ? greenColor : accentcolor, flex: 1, textAlign: 'left', direction: 'ltr', fontSize: 18 }}>{data.oneDayChange > 0 ? '+' : ''}{(data.oneDayChange * 100 / data.price).toFixed(2)}%</p>}
                    </div>
                    <h1 className={"PrimaryText" + (data.canAnimate && data.canAnimate == true ? " animate-flicker" : "")} style={{ color: '#DADADA', marginTop: 6, fontSize: 16 }}>{data.price} $</h1>
                    <div style={{ zIndex: 900, width: '100%', marginLeft: 8 }}>
                        <p className="ThirdENText" style={{ fontSize: 12, marginTop: -4 }}>ارزش بازار : {parseNumber(data.marketCap)}</p>
                        <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>حداکثر قیمت : {data.high} $</p>
                        <p className="ThirdText" style={{ color: '#A5A5A5', fontSize: 12, marginTop: 0 }}>تاریخ حداکثر قیمت : {parseDate(data.highTimestamp)}</p>

                        {data.info['1d'] && data.info['1d']['volume'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 8 }}>حجم ۱ روزه : {data.info['1d']['volume']}</p>}
                        {data.info['1d'] && data.info['1d']['price_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات قیمت ۱ روزه : {data.info['1d']['price_change']} $</p>}
                        {data.info['1d'] && data.info['1d']['volume_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات حجم ۱ روزه : {data.info['1d']['volume_change']}</p>}

                        {data.info['7d'] && data.info['7d']['volume'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 8 }}>حجم ۷ روزه : {data.info['7d']['volume']}</p>}
                        {data.info['7d'] && data.info['7d']['price_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات قیمت ۷ روزه : {data.info['7d']['price_change']} $</p>}
                        {data.info['7d'] && data.info['7d']['volume_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات حجم ۷ روزه : {data.info['7d']['volume_change']}</p>}

                        {data.info['30d'] && data.info['30d']['volume'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 8 }}>حجم ۳۰ روزه : {data.info['30d']['volume']}</p>}
                        {data.info['30d'] && data.info['30d']['price_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات قیمت ۳۰ روزه : {data.info['30d']['price_change']} $</p>}
                        {data.info['30d'] && data.info['30d']['volume_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات حجم ۳۰ روزه : {data.info['30d']['volume_change']}</p>}
                    </div>
                </div>
            </div>}
        </Dialog>
    )
}

export default MarketPriceDialog
