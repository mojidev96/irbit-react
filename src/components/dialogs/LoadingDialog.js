import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, CircularProgress } from '@material-ui/core'

function LoadingDialog(props) {

    const ss = () => { }
    return (
        <Dialog open={true} onClose={ss} maxWidth="sm" aria-labelledby="loadingDialog">
            <div>
                <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            </div>
        </Dialog>
    )
}

export default LoadingDialog
