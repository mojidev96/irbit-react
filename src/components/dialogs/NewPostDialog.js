import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, IconButton, TextField, Checkbox, RadioGroup, Radio, Button, CircularProgress, Divider } from '@material-ui/core'
import { Bar } from 'react-chartjs-2'
import { notifySuccessful, notifyError, notifyLoginNeeded, notifyErrorMessage, accentcolor, greenColor, blueColor, account, notifyWarnMessage, itemColor } from '../../App'
import Axios from 'axios'
import { MdArrowForward, MdBookmarkBorder, MdCheckCircle, MdChat, MdExitToApp, MdInfo, MdMoreVert, MdOpenInNew, MdCall, MdStore, MdReport, MdShare, MdClose, MdEdit, MdDelete, MdAdd } from 'react-icons/md'
import { parseDistance, parsePrice, isImageFile, isVideoFile } from '../../tools/extentions'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import copy from 'copy-to-clipboard'
import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { isAndroid, isIOS } from 'react-device-detect'

const Compress = require('compress.js')

function LinearProgressWithLabel(props) {
    return (
        <Box display="flex" alignItems="center" style={{ color: greenColor }}>
            <Box minWidth={40}>
                <Typography variant="body2" color="textSecondary">{`${Math.round(
                    props.value,
                )}%`}</Typography>
            </Box>
            <Box className="flipLayout" style={{ flex: 1 }} mr={1}>
                <LinearProgress variant="determinate" {...props} />
            </Box>
        </Box>
    );
}

LinearProgressWithLabel.propTypes = {
    /**
     * The value of the progress indicator for the determinate and buffer variants.
     * Value between 0 and 100.
     */
    value: PropTypes.number.isRequired,
};

function NewPostDialog(props) {
    let history = useHistory()
    const [isLoading, setLoading] = useState(false)

    const isFromLocalScreens = useRef(false)
    const [responseMessage, setResponseMessage] = useState('')
    const [isShowingResponse, setShowingResponse] = useState('')
    const [responseColor, setResponseColor] = useState(greenColor)

    const [title, setTitle] = useState('')
    const [caption, setCaption] = useState('')

    const [file, setFile] = useState()
    const [preview, setPreview] = useState()
    const [videoPreview, setVideoPreview] = useState()
    const catId = useRef(0)
    const [uploadProgress, setUploadProgress] = useState(0)
    const [fileIsVideo, setFileIsVideo] = useState(false)

    useEffect(() => {
        catId.current = props.data.catId
        setTitle(localStorage.getItem('gti83712211234') ?? '')
        setCaption(localStorage.getItem('gde8371221124') ?? '')
    }, [])

    useEffect(() => {
        if (!file) {
            setPreview(undefined)
            return
        }

        var objectUrl
        objectUrl = URL.createObjectURL(file)
        setPreview(objectUrl)
        // free memory when ever this component is unmounted
        return () => URL.revokeObjectURL(objectUrl)
    }, [file])

    const pickFile = () => {
        document.getElementById('pickFile').click()
    }

    const onSelectFile = async (e) => {
        if (!e.target.files || e.target.files.length === 0) {
            return
        }

        const fileData = e.target.files[0]

        const isImage = isImageFile(fileData.name)
        const isVideo = isVideoFile(fileData.name)
        setFileIsVideo(isVideo)
        if (isImage) {
            if (fileData.size > 100000) {
                try {
                    const compress = new Compress()
                    await compress
                        .compress([fileData], {
                            size: 0.2,
                            quality: 0.75,
                            maxWidth: 1024,
                            maxHeight: 1024,
                            resize: true,
                        })
                        .then((data) => {
                            let mimeType = 'image/png'
                            const bstr = atob(data[0].data)
                            let n = bstr.length
                            const u8arr = new Uint8Array(n)
                            while (n--) {
                                u8arr[n] = bstr.charCodeAt(n)
                            }
                            setFile(new File([u8arr], data.alt, { type: mimeType }))
                        })
                } catch (error) {
                    //console.log(error)
                    notifyErrorMessage('خطا در فشرده سازی تصویر.')
                }
            } else {
                setFile(fileData)
            }
        } else {
            setFileIsVideo(false)
            setFile(fileData)
            setVideoPreview(URL.createObjectURL(fileData))
            setTimeout(() => {
                setFileIsVideo(true)
            }, 500);
        }


    }

    useEffect(() => { }, [])

    const sendPost = async () => {
        if (title.length === 0 || caption.length === 0) {
            return
        }
        setLoading(true)
        closeResponseMessage()

        const formData = new FormData()
        formData.append('title', title)
        formData.append('caption', caption)
        if (file != null) {
            if (fileIsVideo) {
                formData.append('video', file)
            } else {
                formData.append('poster', file)
            }
        }

        var config = {
            onUploadProgress: function (progressEvent) {
                var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
                setUploadProgress(percentCompleted)
            }
        };

        await Axios.put('posts', formData, config)
            .then((response) => {
                if (response.status == 200) {
                    localStorage.setItem('gti83712211234', '')
                    localStorage.setItem('gde8371221124', '')
                    props.data.onNewPostDone(response.data)
                } else if (response.status == 401) {
                    setResponseMessage('لطفا ابتدا وارد حساب کاربری خود شوید.')
                }
            })
            .catch((err) => {
                setResponseMessage(err.toString())
                setResponseColor(accentcolor)
                openResponseMessage()
            })

        setLoading(false)
    }

    const onCloseScreen = () => {
        isFromLocalScreens ? props.data.onCloseFunc() : history.goBack()
    }

    const openResponseMessage = () => {
        setShowingResponse(true)
    }

    const closeResponseMessage = () => {
        setShowingResponse(false)
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} fullWidth maxWidth="sm" aria-labelledby="newPostDialog">
            <div >
                <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 24, paddingRight: 24, paddingLeft: 24, paddingBottom: 16 }}>
                    <p className="PrimaryText">پست جدید</p>
                    <TextField
                        label="عنوان پست"
                        value={title}
                        onChange={(e) => {
                            localStorage.setItem('gti83712211234', e.target.value)
                            setTitle(e.target.value)
                        }}
                        style={{ direction: 'rtl', textAlign: 'right', marginTop: 16, marginBottom: 8 }}
                    />
                    <TextField
                        label="متن پست"
                        multiline
                        value={caption}
                        onChange={(e) => {
                            localStorage.setItem('gde8371221124', e.target.value)
                            setCaption(e.target.value)
                        }}
                        style={{ direction: 'rtl', textAlign: 'right', marginTop: 0, marginBottom: 8 }}
                    />
                    <div className="StackContainer" onClick={pickFile} style={{ marginTop: 12, width: '100%', height: isAndroid || isIOS ? 200 : 250 }}>
                        <MdAdd size={24} color="black" onClick={(e) => {
                            e.stopPropagation()
                            console.log('ehehwe')
                            pickFile()
                        }} className="StackElement" style={{ zIndex: 999, backgroundColor: 'white', borderBottomLeftRadius: 4 }} />
                        <input id="pickFile" type="file" hidden onChange={onSelectFile} />
                        {fileIsVideo ? <video
                            autoPlay loop muted playsInline style={{ width: '100%', height: isAndroid || isIOS ? 200 : 250 }} controls>
                            <source src={videoPreview} />
                        </video> : <img src={preview} className="image_file" style={{ borderRadius: 0, width: '100%', height: 200 }} />}
                    </div>
                    <p className="ThirdLightText" style={{ marginBottom: 16 }}>حداکثر ۵۶ مگابایت</p>
                    {isLoading ? <div>
                        <LinearProgressWithLabel value={uploadProgress} />
                        {/* <Button variant="contained" onClick={() => {
                            setTitle('')
                            setCaption('')
                            localStorage.setItem('gti83712211234', '')
                            localStorage.setItem('gde8371221124', '')
                            setFile(null)
                            props.data.onCloseFunc()
                        }} fullWidth={true} style={{ color: 'white', backgroundColor: itemColor, marginTop: 8 }}>
                            انصراف
                        </Button> */}
                    </div> : <Button variant="contained" onClick={sendPost} fullWidth={true} style={{ backgroundColor: blueColor, color: 'white' }}>
                        ثبت
                    </Button>}
                    <div style={{ height: isShowingResponse ? 'auto' : 0, display: 'flex', alignItems: 'center', backgroundColor: responseColor, marginTop: 8, paddingRight: 8, paddingLeft: 8, visibility: isShowingResponse ? 'visible' : 'hidden' }}>
                        <div style={{ width: 20, height: 20 }}>
                            <MdClose size={20} color="white" onClick={closeResponseMessage} />
                        </div>
                        <p className="ThirdBoldText" style={{ color: 'white', padding: 8 }}>
                            {responseMessage}
                        </p>
                    </div>
                </div>
            </div>
        </Dialog>
    )
}

export default NewPostDialog
