import React, { useRef, useEffect, useState } from 'react'
import { Popover, Dialog, Backdrop, IconButton, TextField, Checkbox, RadioGroup, Radio, Button, CircularProgress, Divider } from '@material-ui/core'
import { Bar } from 'react-chartjs-2'
import { notifySuccessful, notifyError, notifyLoginNeeded, notifyErrorMessage, accentcolor, greenColor, blueColor } from '../../App'
import Axios from 'axios'
import { MdArrowForward, MdBookmarkBorder, MdCheckCircle, MdChat, MdExitToApp, MdInfo, MdMoreVert, MdOpenInNew, MdCall, MdStore, MdReport, MdShare, MdClose } from 'react-icons/md'
import { parseDistance, parsePrice, parseLikeOrDislikes, parseDate } from '../../tools/extentions'
import { useHistory } from 'react-router-dom'
import { Slide, toast, ToastContainer } from 'react-toastify'
import copy from 'copy-to-clipboard'

function WhyPricesDifferentDialog(props) {
    const [title, setTitle] = useState('')
    const [message, setMessage] = useState('')

    useEffect(() => {
        setTitle(props.data.title)
        setMessage(props.data.message)
    }, [])

    const onCloseScreen = () => {
        props.data.onCloseFunc()
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="sm" fullWidth aria-labelledby="confirmationDialog">
            <div style={{ display: 'flex', flexDirection: 'column', paddingTop: 24, paddingRight: 16, paddingLeft: 16, paddingBottom: 8 }}>
                <p className="PrimaryText">چرا قیمت های بازار ما اندکی متفاوت است؟</p>
                <p className="ThirdLightText" style={{ marginLeft: 16, marginTop: 6 }}>
                    {message}
                </p>
                <div style={{ display: 'flex', height: 40, alignItems: 'center', textAlign: 'left', direction: 'ltr', marginTop: 12 }}>
                    <Button onClick={() => props.data.cancel()} color="secondary">
                        انصراف
                    </Button>
                    <Button onClick={() => props.data.ok()} style={{ marginRight: 8, color: 'white' }}>
                        انجام
                    </Button>
                </div>
            </div>
        </Dialog>
    )
}

export default WhyPricesDifferentDialog
