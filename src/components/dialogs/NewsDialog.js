import React, { useEffect, useState } from 'react'
import { Dialog, Backdrop, CircularProgress, IconButton } from '@material-ui/core'
import { accentcolor, backgroundColor, greenColor, webUrl } from '../../App'
import { isAndroid, isIOS } from 'react-device-detect'
import { parseDate } from '../../tools/extentions'
import { MdClose } from 'react-icons/md'

function NewsDialog(props) {
    const [data, setData] = useState()
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        setData(props.data.data)
        setTimeout(() => {
            setLoading(false)
        }, 200);
    }, [])

    const onCloseScreen = () => {
        props.data.onCloseFunc()
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="xl" fullScreen aria-labelledby="newsDialog">
            {isLoading ? <div style={{ width: '100%', height: '100%' }}>
                <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            </div> : <div key={data.id} style={{ display: 'flex', justifyContent: 'center', marginLeft: (!isAndroid && !isIOS) ? 4 : 0, marginRight: (!isAndroid && !isIOS) ? 4 : 0, padding: 0, borderRadius: isAndroid || isIOS ? 0 : 4 }}>
                <IconButton onClick={(e) => {
                    e.stopPropagation()
                    onCloseScreen()
                }} style={{ backgroundColor: '#00111155', position: 'fixed', top: 8, right: 8 }}><MdClose size={24} /></IconButton>
                <div style={{ maxWidth: 700 }}>
                    {(data.poster && data.poster.length > 0) && <img className="image" src={webUrl + data.poster} style={{ width: '100%', borderTopLeftRadius: (isAndroid || isIOS) ? 0 : 4, borderTopRightRadius: (isAndroid || isIOS) ? 0 : 4 }} />}
                    <div style={{ height: (isAndroid || isIOS) ? "auto" : 210, padding: 8, paddingBottom: 8, marginRight: 4, marginLeft: 4 }}>
                        <h4 className="PrimaryText two-line-truncate" style={{ fontSize: 24 }}>{data.title}</h4>
                        <div onClick={(e) => {
                            e.stopPropagation()
                            props.data.history.push({
                                pathname: '/comments',
                                search: '?contentId=' + data.id + '&contentType=2',
                            })
                        }} style={{ display: 'flex', marginBottom: 14, right: 8, left: 8, alignItems: 'center', marginTop: 8 }}>
                            <p className="ThirdBoldText clickable" style={{ backgroundColor: accentcolor, color: 'white', paddingTop: 4, paddingBottom: 4, paddingRight: 8, paddingLeft: 8, marginTop: 6, borderRadius: 16, fontSize: 10, marginLeft: 6 }}>{data.commentsCount > 0 ? data.commentsCount + ' نظر' : 'بدون نظر'}</p>
                            <p className="ThirdBoldText clickable" onClick={(e) => {
                                e.stopPropagation()
                                window.open(data.sourceUrl)
                            }} style={{ backgroundColor: greenColor, color: 'white', paddingTop: 4, paddingBottom: 4, paddingRight: 8, paddingLeft: 8, marginTop: 6, borderRadius: 16, fontSize: 10, marginLeft: 6 }}>منبع خبر</p>
                            <p className="ThirdBoldText clickable" onClick={(e) => {
                                e.stopPropagation()
                                props.data.onSelectCategory(data.categoryName)
                            }} style={{ backgroundColor: backgroundColor, paddingTop: 4, paddingBottom: 4, paddingRight: 8, paddingLeft: 8, marginTop: 6, borderRadius: 16, fontSize: 10, marginLeft: 6 }}>{data.categoryName}</p>
                            <p className="ThirdLightText" style={{ marginTop: 6, fontSize: 12 }}>{parseDate(data.dateCreate)}</p>
                        </div>
                        {data.description && data.description.length > 0 && <h6 className="ThirdText multi-line" style={{ paddingBottom: 100, whiteSpace: 'pre-line', marginTop: 4, fontSize: 18 }}>{data.description}</h6>}
                    </div>
                </div>
            </div>}
        </Dialog>
    )
}

export default NewsDialog
