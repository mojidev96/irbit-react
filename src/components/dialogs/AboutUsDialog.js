import React, { useEffect, useState } from 'react'
import { Dialog, Backdrop, CircularProgress, IconButton } from '@material-ui/core'
import { accentcolor, backgroundColor, blueColor, greenColor, webUrl } from '../../App'
import ReactTextFormat from 'react-text-format';
import { isAndroid, isIOS } from 'react-device-detect';
import { MdClose } from 'react-icons/md';
import MoshtebaSign from '../../icons/moshteba_sign.png'
function AboutUsDialog(props) {

    const onCloseScreen = () => {
        props.data.onCloseFunc()
    }

    return (
        <Dialog open={true} onClose={onCloseScreen} maxWidth="xl" fullScreen aria-labelledby="aboutUsDialog">
            <div style={{ overflowX: 'hidden', display: 'flex', justifyContent: 'center', marginLeft: (!isAndroid && !isIOS) ? 4 : 0, marginRight: (!isAndroid && !isIOS) ? 4 : 0, padding: 0, borderRadius: isAndroid || isIOS ? 0 : 4 }}>
                <div style={{ display: 'flex', justifyContent: 'center' }}>
                    <IconButton onClick={(e) => {
                        e.stopPropagation()
                        onCloseScreen()
                    }} style={{ backgroundColor: '#00111155', position: 'fixed', top: 8, right: 8 }}><MdClose size={24} /></IconButton>
                    <div style={{ width: '100%', justifyContent: 'center', maxWidth: 500, paddingTop: 46, paddingBottom: 100, paddingRight: 24, paddingLeft: 24 }}>
                        <p className="PrimaryText" style={{ fontSize: 24, marginTop: 32 }}>درباره ما؟! :)</p>
                        <p className="SecondaryText" style={{ direction: 'rtl', textAlign: 'justify', fontSize: 14, marginTop: 24 }}>
                            وبسایت irBit یه وبسایت شخصی متن باز هست که آخرای اسفند ۹۹ راه اندازی شده. این وبسایت در حال حاضر منبع درآمد و پلنی برای اون نداره.
                            این وبسایت با هدف ایجاد یک شبکه اجتماعی برای کسایی طراحی شده که طالب آزادی هستن. پس؛ سورس کد این وبسایت رو هم در اختیار شما میزارم (لینک در انتها). من، مالک این وبسایت یک برنامه نویس هستم و متعاقبا هنوز فرصت نکردم برای سورس ها داکیومنت درستی بنویسم و چون پروژه کوچکی هست احتمالا از ماهیت عمومی کدهاش به راحتی سردرمیارید.
                            <br />
                            اگه سوال یا نقطه نظری داشتید می تونید بطور مستقیم با من از طریق ایمیل iranCoolKids@gmail در ارتباط باشید. اگه کارتون اونقدر مهم نیست که نمی تونید ایمیل بزنید پس حتما مهم نیست!
                            <br />
                            اگه پیشنهاد همکاری دارین و می تونین در این پروژه متن باز کمک کنین (چه کدی، چه پولی و چه تولید محتوایی) خوشحال می شم به تیم ۱ نفره ما اضافه شید.
                            <br />
                            هزینه های جاری این وبسایت رو بطور شخصی میدم ولی اگه گرفت و شلوغ شد احتمالا مجبور شم ازتون گدایی کنم چون سرورا گرونه. این شماره حسابا رو هم اینجا میزارم اگه وبسایت بدردتون خورد یه پولی بزنین منم خوشحال شم:
                            <br />
                        </p>
                        <p className="SecondaryText" style={{ color: greenColor, direction: 'ltr', textAlign: 'left', fontSize: 14, marginTop: 24 }}>
                            ETC:
                            0x50F6B96335d411a731E09DEA626111D77b993c67
                            BTC:
                            bc1q47a0e8803cte928ce3dlzu8k26cnfq2q2ds2ly
                            TRON:
                            TKSjJ7KzbVnrrMFjrVMBPTZm5upjvTEZYh
                            BNB:
                            bnb10anud9d8cjmdlvzps9zf3ej0zkr8na4xx4e9rl
                            TET-ERC20:
                            0x50F6B96335d411a731E09DEA626111D77b993c67
                            TET-TRC20:
                            TKSjJ7KzbVnrrMFjrVMBPTZm5upjvTEZYh
                        </p>
                        <p className="SecondaryText" style={{ direction: 'rtl', textAlign: 'right', fontSize: 14, marginTop: 24 }}>
                            پروژه: این وبسایت با NestJs دیتابیس Postgresql و ReactJs نوشته شده. یه بکاپ دیتابیس داخل ریپوی Nest گذاشتم:
                        </p>
                        <div className="SecondaryText" style={{ color: blueColor, direction: 'ltr', textAlign: 'left', fontSize: 14, marginTop: 12 }}>
                            <ReactTextFormat>
                                https://gitlab.com/mojidev96/irbit-nestjs
                                https://gitlab.com/mojidev96/irbit-react
                            </ReactTextFormat>
                        </div>
                        <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                            <img src={MoshtebaSign} style={{ width: 100, height: 100, marginTop: 16, marginBottom: 100 }} />
                        </div>
                    </div>
                </div></div>
        </Dialog>
    )
}

export default AboutUsDialog
