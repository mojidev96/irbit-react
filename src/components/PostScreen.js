import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    LinearProgress,
    IconButton,
    CircularProgress,
} from '@material-ui/core'
import { MdAccountBox, MdAccountCircle, MdAdd, MdArrowBack, MdArrowForward, MdBookmarkBorder, MdComment, MdDelete, MdEdit, MdExitToApp, MdKeyboardArrowLeft, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { backgroundColor, greenColor, token, account, notifyWarnMessage, webUrl, accentcolor, notifyErrorMessage, itemColor } from '../App'
import { parsePrice, parseDate, parseNumber, parseJustDate, parseLikeOrDislikes } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'
import { isIOS, isAndroid } from 'react-device-detect'
import ArroUpIcon from '../icons/up-arrow.png'
import ArroDownIcon from '../icons/down-arrow.png'
import ArroUpOutlineIcon from '../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../icons/down-arrow-outline.png'
import ReactTextFormat from 'react-text-format';
import LoadingDialog from './dialogs/LoadingDialog'
import CommentsScreen from './CommentsScreen'
import LikeFillPNG from '../icons/like_fill.png'
import LikePNG from '../icons/like.png'

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function PostScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const postId = useRef(0)

    const [isLoading, setLoading] = useState(true)
    const isAllGot = useRef(false)
    const [post, setPost] = useState(null)

    useEffect(() => {
        const search = props.location.search
        const params = new URLSearchParams(search)
        postId.current = params.get('id')

        getPost()

        return () => {
            window.onscroll = ''
        }
    }, [])

    const getPost = async () => {
        if (isAllGot.current) {
            return
        }
        setLoading(true)
        try {
            const response = await Axios.post(
                'posts/byId',
                {
                    id: postId.current,
                }, { headers: { token } }
            )

            if (response.status == 201) {
                setPost(response.data)
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            notifyErrorMessage('اشکال در برقراری ارتباط')
        }
        setLoading(false)
    }

    const [isLoadingLike, setIsLoadingLike] = useState(false)
    const likeUnlike = async () => {
        setIsLoadingLike(true)
        try {
            const response = await Axios.post(
                'postLikes',
                {
                    id: postId.current,
                }, { headers: { token } }
            )

            if (response.status == 201) {
                setPost({ ...post, iLike: !post.iLike, likesCount: post.iLike ? post.likesCount - 1 : post.likesCount + 1 })
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            notifyErrorMessage('اشکال در برقراری ارتباط')
        }
        setIsLoadingLike(false)
    }

    const [isShowingComDialog, setIsShowingComDialog] = useState(false)

    return (
        <div className={classes.root} style={{ overflow: 'hidden', display: 'flex', justifyContent: 'center', backgroundColor: backgroundColor }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography className="single-line-truncate" variant="h6" color="inherit">
                        پست
                    </Typography>
                    {/* <section style={{ marginRight: 'auto', marginLeft: 0, paddingLeft: 0, display: 'flex', alignItems: 'center' }}>
                        <IconButton aria-describedby={popoverId} onClick={handleClickOpenPopover}>
                            <p className="ThirdBoldText">{sortTypeTitle}</p>
                        </IconButton>
                    </section> */}
                </Toolbar>
            </AppBar>

            <div className="StackContainer" style={{ overflow: 'hidden', width: '100%', maxWidth: 700, minHeight: props.height }}>
                {isLoading ? <LoadingDialog /> : <div style={{ paddingTop: 56, width: '100%', overflow: 'hidden' }}>
                    <div onClick={(e) => e.stopPropagation()} className="SecondaryText" style={{ whiteSpace: 'pre-line', paddingTop: 0, marginRight: 16, marginLeft: 16, marginTop: 8, marginBottom: 8 }}>
                        {post.videoUrl == null ? <img className="image" src={webUrl + post.posterUrl} style={{ width: '100%', maxHeight: 400 }} /> : <video style={{ width: '100%', maxHeight: 400 }} controls>
                            <source src={webUrl + post.videoUrl} />
                        </video>}
                        <div className="SecondaryText multi-line" style={{ marginTop: 8, marginRight: 4, marginLeft: 4 }}>
                            <div style={{ display: 'flex', alignItems: 'top', marginBottom: 8 }}>
                                <h1 className="PrimaryText multi-line" style={{ flex: 1, fontSize: 18, marginLeft: 8 }}>{post.title}</h1>

                                <div style={{ marginBottom: 2, display: 'flex', alignItems: 'center' }}>
                                    {post.commentsCount > 0 && <p className="ThirdText" style={{ marginLeft: 8 }}>{parseLikeOrDislikes(post.commentsCount)}</p>}
                                    <MdComment onClick={() => setIsShowingComDialog(true)} style={{ marginLeft: 12, width: 24, height: 24 }} />

                                    {post.likesCount > 0 && <p className="ThirdText" style={{ marginLeft: 8 }}>{parseLikeOrDislikes(post.likesCount)}</p>}
                                    {isLoadingLike ? <CircularProgress size={16} style={{ color: 'white' }} /> : <img src={post.iLike ? LikeFillPNG : LikePNG} onClick={likeUnlike} style={{ width: 24, height: 24 }} />}
                                </div>

                            </div>
                            <ReactTextFormat >
                                {post.caption}
                            </ReactTextFormat>
                            <p className="ThirdLightText">{parseDate(post.dateCreate)}</p>
                        </div>
                    </div>
                    <CommentsScreen showingDialog={isShowingComDialog} changeDialogState={() => setIsShowingComDialog(false)} id={postId.current} type="3" />
                </div>}
            </div>

            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default PostScreen
