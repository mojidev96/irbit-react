import React, { useEffect, useState, useRef, createRef } from 'react'
import { AppBar, Toolbar, Dialog, Typography, DialogTitle, Backdrop, DialogContent, DialogContentText, DialogActions, GridList, Fab, Box, LinearProgress, IconButton, TextField, Checkbox, RadioGroup, Radio, MenuItem, FormControlLabel, Button, CircularProgress, Grid, Divider } from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import 'react-toastify/dist/ReactToastify.css'
import { webUrl } from '../App'
const useStyles = makeStyles(() => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function ImageViewerScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const [image, setImage] = useState('')
    const [localImage, setLocalImage] = useState()

    useEffect(() => {
        if(props.location.localImage != null){
            setLocalImage(props.location.localImage)
        } else if (props.location.image == null) {
            setImage(JSON.parse(localStorage.getItem('last-imv-reuse') ?? ''))
        } else {
            setImage(props.location.image)
            localStorage.setItem('last-imv-reuse', JSON.stringify(props.location.image))
        }
    }, [])

    return (
        <div className={classes.root} style={{ overflow: 'hidden' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        تصویر
                    </Typography>
                </Toolbar>
            </AppBar>
            <img
                style={{
                    paddingTop: 48,
                    height: '100%',
                    width: '100%',
                    objectFit: 'contain',
                    borderRadius: 0,
                }}
                src={localImage == null ? (webUrl + image) : localImage}
                alt="تصویر"
            />
        </div>
    )
}

export default ImageViewerScreen
