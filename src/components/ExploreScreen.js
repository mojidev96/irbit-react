import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    Card,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    GridListTile,
    InputBase,
    Accordion,
    AccordionSummary,
    AccordionDetails,
} from '@material-ui/core'

import AnimatedNumber from "animated-number-react";
import { MdArrowForward, MdBookmarkBorder, MdClose, MdComment, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew, MdRoom, MdSearch } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import { useSpring, animated } from 'react-spring'
import Axios from 'axios'
import { accentcolor, backgroundColor, globalTabIndex, greenColor, itemColor, lightIconColor, notifyErrorMessage, token, webUrl } from '../App'
import { parseDistance, parseDate, parseNumber, parseLikeOrDislikes } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import EmptyView from '../widgets/EmptyView'
import TryAgainWidget from '../widgets/TryAgainWidget'
import { isAndroid, isIOS } from 'react-device-detect'
import LoadingDialog from './dialogs/LoadingDialog'
import PullToRefresh from 'react-simple-pull-to-refresh'
import LikeFillPNG from '../icons/like_fill.png'
import LikePNG from '../icons/like.png'

function ExploreScreen(props) {
    let history = useHistory()
    const [isLoading, setIsLoading] = useState(true)
    const page = useRef(1)
    const isAllGot = useRef(false)
    const isWorkingScroll = useRef(0)

    const posts = useRef([])

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem('exploreData'))
        page.current = Number(localStorage.getItem('explorePage'))

        if (data.length > 0) {
            posts.current.push(...data)
            setIsLoading(false)
            setItHasProblem(false)
            window.scrollTo({ top: localStorage.getItem('exploreScroll') })
        } else {
            getExplore()
        }

    }, [])

    window.onscroll = () => {
        if (window.pageYOffset != 0 && posts.current.length > 0) {
            localStorage.setItem('exploreScroll', window.pageYOffset)
        }
        try {
            if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridList').offsetHeight) {
                isWorkingScroll.current = 1
                getExplore()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) { }
    }

    const getExplore = async () => {
        if (isAllGot.current) {
            return
        }

        setIsLoading(true)
        setItHasProblem(false)

        try {
            const response = await Axios.post('posts/explore', { page: page.current }, { headers: { token } })
            if (response.data instanceof Array && response.data.length > 0) {
                posts.current.push(...response.data)
                ++page.current
                localStorage.setItem('exploreData', JSON.stringify(posts.current))
                localStorage.setItem('explorePage', page.current)
            }
        } catch (error) {
            setItHasProblem(true)
            // console.log('err filters :: ', error)
        }
        setIsLoading(false)
    }

    const [itHasProblem, setItHasProblem] = useState(false)

    const handleRefresh = () => {
        setIsLoading(true)
        page.current = 1
        posts.current = []
        posts.current.length = 0
        setTimeout(() => {
            getExplore()
        }, 100)
    }

    const [isLoadingLike, setIsLoadingLike] = useState(false)
    const likeUnlike = async (postId, index) => {
        setIsLoadingLike(true)
        try {
            const response = await Axios.post(
                'postLikes',
                {
                    id: postId,
                }, { headers: { token } }
            )

            if (response.status == 201) {
                posts.current[index] = { ...posts.current[index], iLike: !posts.current[index].iLike, likesCount: posts.current[index].iLike ? posts.current[index].likesCount - 1 : posts.current[index].likesCount + 1 }
            } else {
                notifyErrorMessage('اشکال در برقراری ارتباط')
            }
        } catch (error) {
            notifyErrorMessage('اشکال در برقراری ارتباط')
        }
        setIsLoadingLike(false)
    }

    return (
        <div style={{ direction: 'rtl' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        فعالیتها
                    </Typography>
                </Toolbar>
            </AppBar>
            {!isLoading && itHasProblem ? (
                <TryAgainWidget marginBottom={32} onTryAgain={getExplore} />
            ) : posts.current.length == 0 && !isLoading ? (
                <EmptyView marginBottom={24} />
            ) : (<div style={{ overflowY: 'hidden', paddingTop: 48 }} role="tabpanel" id={`full-width-tabpanel-${props.index}`} aria-labelledby={`full-width-tab-${props.index}`}>
                {isLoading && posts.current.length == 0 ? <LoadingDialog /> : <div />}
                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%' }}>
                    <PullToRefresh pullingContent="بروزرسانی" isPullable={true} pullDownThreshold={50} onRefresh={async () => {
                        handleRefresh()
                        const p = new Promise((resolve, reject) => {
                            setTimeout(() => {
                                resolve('ok')
                            }, 1500);
                        })
                        const resp = await p
                        return console.log(resp)
                    }}> <div style={{ paddingTop: 0, width: '100%', overflow: 'hidden', display: 'flex', justifyContent: 'center', minHeight: window.innerHeight }}>
                            <GridList cellHeight="auto" id="gridList" cols={1} style={{ width: '100%', maxWidth: 500, paddingTop: (isAndroid || isIOS) ? 0 : 4, justifyContent: 'center', paddingBottom: 100 }}>
                                {posts.current.map((item, index) => (
                                    <div className="clickable" key={item.id} onClick={(e) => {
                                    }} style={{ width: '100%', marginLeft: (!isAndroid && !isIOS) ? 4 : 0, marginRight: (!isAndroid && !isIOS) ? 4 : 0, padding: 0, marginTop: 8 }}>
                                        <img className="image" src={webUrl + item.posterUrl} style={{ width: '100%', height: "auto", borderRadius: 4 }} />
                                        <div style={{ position: 'relative', height: "auto", padding: 8, paddingBottom: 8, marginRight: 4, marginLeft: 4 }}>
                                            <div style={{ display: 'flex', alignItems: 'top', marginBottom: 8 }}>
                                                <h1 className="PrimaryText multi-line" style={{ flex: 1, fontSize: 18, marginLeft: 8 }}>{item.title}</h1>

                                                <div style={{ marginBottom: 2, display: 'flex', alignItems: 'center' }}>
                                                    {item.commentsCount > 0 && <p className="ThirdText" style={{ marginLeft: 8 }}>{parseLikeOrDislikes(item.commentsCount)}</p>}
                                                    <MdComment color="#e1e1e1" onClick={() => history.push({ pathname: 'comments', search: '?contentId=' + item.id + '&contentType=3' })} style={{ marginLeft: 12, width: 24, height: 24 }} />

                                                    {item.likesCount > 0 && <p className="ThirdText" style={{ marginLeft: 8 }}>{parseLikeOrDislikes(item.likesCount)}</p>}
                                                    {isLoadingLike ? <CircularProgress size={16} style={{ color: 'white' }} /> : <img src={item.iLike ? LikeFillPNG : LikePNG} onClick={() => likeUnlike(item.id, index)} style={{ width: 24, height: 24 }} />}
                                                </div>

                                            </div>
                                            {item.caption && item.caption.length > 0 && <h6 className="ThirdText multi-line" style={{ whiteSpace: 'pre-line', marginTop: 4, fontSize: 13 }}>{item.caption}</h6>}
                                        </div>
                                    </div>
                                ))}
                            </GridList>
                        </div>
                    </PullToRefresh>
                </div>
                {isLoading && posts.current.length > 0 ? (
                    <div style={{ width: '100%', zIndex: 200 }}>
                        <div style={{ width: '100%' }}>
                            <LinearProgress color="secondary" />
                        </div>
                    </div>
                ) : (
                    <div></div>
                )}
            </div>)}
        </div>

    )
}

export default ExploreScreen
