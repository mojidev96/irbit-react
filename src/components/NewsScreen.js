import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    Card,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    GridListTile,
    InputBase,
} from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdClose, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew, MdSearch } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { accentcolor, backgroundColor, globalTabIndex, greenColor, itemColor, lightIconColor, notifyErrorMessage, token, webUrl } from '../App'
import { parseDistance, parseDate, parseNumber } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'
import ProfileScreen from './ProfileScreen'
import TryAgainWidget from '../widgets/TryAgainWidget'
import { isAndroid, isIOS } from 'react-device-detect'

import ArroUpIcon from '../icons/up-arrow.png'
import ArroDownIcon from '../icons/down-arrow.png'
import ViewIcon from '../icons/view.png'
import ArroUpOutlineIcon from '../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../icons/down-arrow-outline.png'
import LoadingDialog from './dialogs/LoadingDialog'
import PullToRefresh from 'react-simple-pull-to-refresh';
import NewsDialog from './dialogs/NewsDialog'

function NewsScreen(props) {
    let history = useHistory()
    const [isLoadingNews, setIsLoadingNews] = useState(true)
    const page = useRef(1)
    const isAllNewsGot = useRef(false)
    const news = useRef([])
    const wordToServer = useRef('')
    const isWorkingScroll = useRef(0)
    const [word, setWord] = useState('')
    const [colsNumber, setColsNumber] = useState(300)

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem('newsData'))
        page.current = Number(localStorage.getItem('newsPage'))
        wordToServer.current = localStorage.getItem('newsWord')
        // globalTabIndex = 1
        setWord(wordToServer.current)
        if (data.length > 0) {
            news.current.push(...data)
            setIsLoadingNews(false)
            setItHasProblem(false)
        } else {
            getNews()
        }

    }, [])

    useEffect(() => {
        setColsNumber((props.width / 350).toFixed(0))
    }, [props.width])

    useEffect(() => {
        if (globalTabIndex == 1 && news.current.length > 0) {
            window.scrollTo({ top: localStorage.getItem('newsScroll') })
        }
        window.onscroll = () => {
            if (window.pageYOffset != 0 && globalTabIndex == 1 && news.current.length > 0) {
                localStorage.setItem('newsScroll', window.pageYOffset)
            }
            try {
                if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridNewsList').offsetHeight) {
                    isWorkingScroll.current = 1
                    getNews()
                    setTimeout(function () {
                        isWorkingScroll.current = 0
                    }, 500)
                }
            } catch (e) { }
        }

    }, [globalTabIndex, news.current.length])

    const getNews = async () => {
        if (isAllNewsGot.current) {
            return
        }
        setItHasProblem(false)
        setIsLoadingNews(true)


        try {
            const response = await Axios.post('news', {
                page: page.current,
                categoryId: 0,
                sortType: 0,
                word: wordToServer.current,
            })

            if (response.data instanceof Array && response.data.length > 0) {
                news.current.push(...response.data)
                localStorage.setItem('newsData', JSON.stringify(news.current))
                ++page.current
                localStorage.setItem('newsPage', page.current)
            } else {
                isAllNewsGot.current = true
            }
        } catch (error) {
            setItHasProblem(true)
            //console.log('err filters :: ', error)
        }
        setIsLoadingNews(false)
    }

    //contentType: 1=news, 2=tutorials, 3=comments    
    const [upDownLoadingId, setUpDownLoadingId] = useState(0)
    const upOrDown = async (index, contentType, id, isUp, isForgive) => {
        if (localStorage.getItem('token') == null) {
            history.push('/login')
            return
        }

        setUpDownLoadingId(id)
        try {
            const response = await Axios.post('upAndDowns', {
                isUp,
                isForgive,
                contentId: id,
                contentType,
            })
            if (response.status == 201) {
                if (isUp == true) {
                    if (news.current[index].iDown != null && news.current[index].iDown == true) {
                        news.current[index].iDown = false
                        news.current[index].downs -= 1
                    }
                } else {
                    if (news.current[index].iUp != null && news.current[index].iUp == true) {
                        news.current[index].iUp = false
                        news.current[index].ups -= 1
                    }
                }

                if (isUp == true) {
                    news.current[index].iUp = isForgive == false
                } else {
                    news.current[index].iDown = isForgive == false
                }

                if (isUp == true) {
                    if (isForgive == false) {
                        news.current[index].ups += 1
                    } else {
                        news.current[index].ups -= 1
                    }
                } else {
                    if (isForgive == false) {
                        news.current[index].downs += 1
                    } else {
                        news.current[index].downs -= 1
                    }
                }
                // setNews(news)
            } else {
                notifyErrorMessage('نشد که بشه... لطفا دوباره امتحان کنید.')
            }
        } catch (error) { }
        setUpDownLoadingId(0)
    }

    const [itHasProblem, setItHasProblem] = useState(false)
    const handleRefresh = () => {
        setIsLoadingNews(true)
        page.current = 1
        news.current = []
        news.current.length = 0
        isAllNewsGot.current = false
        setTimeout(() => {
            getNews()
        }, 100)
    }

    // handle showing profile dialog screen
    const selectedNewsData = useRef()
    const [isShowingNewsDialog, setShowingNewsDialog] = useState()
    const showNewsDialog = (isShowing) => {
        if (isShowing) {
            return <NewsDialog data={{ data: selectedNewsData.current, history, onCloseFunc: closeNewsDialog, onSelectCategory }} />
        } else {
            return <div />
        }
    }

    const onSelectCategory = (word) => {
        setShowingNewsDialog(false)
        setWord(word)
        wordToServer.current = word
        page.current = 1
        isAllNewsGot.current = false
        news.current = []
        news.current.length = 0
        getNews()
    }

    const closeNewsDialog = () => {
        setShowingNewsDialog(false)
    }
    // end of profile dialog screen

    return (

        <div style={{ overflow: 'hidden', direction: 'rtl' }}>
            {showNewsDialog(isShowingNewsDialog)}
            {!isLoadingNews && itHasProblem ? (
                <TryAgainWidget onTryAgain={getNews} />
            ) : news.current.length == 0 && !isLoadingNews && word.length == 0 ? (
                <EmptyView />
            ) : (<div style={{ paddingTop: 48 }} >
                <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                    {isLoadingNews && news.current.length == 0 ? <LoadingDialog /> : <div />}
                    <Card style={{ display: 'flex', alignItems: 'center', width: '100%', marginTop: 14, maxWidth: 500, borderRadius: (isAndroid || isIOS) ? 0 : 4 }}>
                        <InputBase
                            placeholder="جستجو"
                            onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                    page.current = 1
                                    isAllNewsGot.current = false
                                    news.current = []
                                    news.current.length = 0
                                    setTimeout(() => {
                                        getNews()
                                    }, 100)
                                }
                            }}
                            value={word}
                            style={{ width: '100%', paddingTop: 6, paddingRight: 12, paddingLeft: 8 }}
                            onChange={(e) => {
                                localStorage.setItem('sea-man-vi-word', e.target.value)
                                wordToServer.current = e.target.value
                                setWord(e.target.value)
                            }}
                        />
                        <IconButton
                            onClick={() => {
                                page.current = 1
                                isAllNewsGot.current = false
                                if (word.length > 0) {
                                    setWord('')
                                    wordToServer.current = ''
                                    news.current = []
                                    news.current.length = 0
                                    setTimeout(() => {
                                        getNews()
                                    }, 100)
                                } else {
                                    getNews()
                                }
                            }}
                        >
                            {word.length > 0 ? <MdClose color="red" /> : <MdSearch color={lightIconColor} />}
                        </IconButton>
                    </Card>

                </div>

                <div className="StackContainer" style={{ overflow: 'hidden', height:'100%', width: '100%' }}>
                    <PullToRefresh pullingContent="بروزرسانی" isPullable={true} pullDownThreshold={50} onRefresh={async () => {
                        handleRefresh()
                        const p = new Promise((resolve, reject) => {
                            setTimeout(() => {
                                resolve('ok')
                            }, 1500);
                        })
                        const resp = await p
                        return console.log(resp)
                    }}>
                        <div style={{ overflow: 'hidden', paddingTop: 0, width: '100%', display: 'flex', justifyContent: 'center', minHeight: window.innerHeight }}>
                            <GridList cellHeight={isAndroid || isIOS ? "auto" : 480} id="gridNewsList" cols={(isAndroid || isIOS ? 1 : colsNumber)} style={{ width: '100%', minHeight: window.innerHeight, justifyContent: 'center', paddingTop: 4, paddingBottom: 32 }}>
                                {news.current.map((item, index) => (
                                    <div className="clickable" key={item.id} onClick={(e) => {
                                        e.stopPropagation()
                                        selectedNewsData.current = item
                                        setShowingNewsDialog(true)
                                    }} style={{ width: (isAndroid || isIOS) ? '100%' : 350, marginLeft: (!isAndroid && !isIOS) ? 4 : 0, marginRight: (!isAndroid && !isIOS) ? 4 : 0, backgroundColor: itemColor, padding: 0, borderRadius: isAndroid || isIOS ? 0 : 4, marginTop: 8 }}>
                                        {(item.poster && item.poster.length > 0) && <img className="image" src={webUrl + item.poster} style={{ width: '100%', height: (isAndroid || isIOS) ? 200 : 270, borderTopLeftRadius: (isAndroid || isIOS) ? 0 : 4, borderTopRightRadius: (isAndroid || isIOS) ? 0 : 4 }} />}
                                        <div style={{ position: 'relative', height: (isAndroid || isIOS) ? "auto" : 210, padding: 8, paddingBottom: 8, marginRight: 4, marginLeft: 4 }}>
                                            <h4 className="PrimaryText two-line-truncate">{item.title}</h4>
                                            {item.description && item.description.length > 0 && <h6 className="ThirdText multi-line-4lines" style={{ whiteSpace: 'pre-line', marginTop: 4, fontSize: 13 }}>{item.description}</h6>}
                                            <div onClick={(e) => {
                                                e.stopPropagation()
                                                history.push({
                                                    pathname: '/comments',
                                                    search: '?contentId=' + item.id + '&contentType=2',
                                                })
                                            }} style={{ display: 'flex', position: (isAndroid || isIOS) ? 'initial' : 'absolute', bottom: 26, right: 8, left: 8, alignItems: 'center', marginTop: 8 }}>
                                                <p className="ThirdBoldText clickable" style={{ backgroundColor: accentcolor, color: 'white', paddingTop: 4, paddingBottom: 4, paddingRight: 8, paddingLeft: 8, marginTop: 6, borderRadius: 16, fontSize: 10, marginLeft: 6 }}>{item.commentsCount > 0 ? item.commentsCount + ' نظر' : 'بدون نظر'}</p>
                                                <p className="ThirdBoldText clickable" onClick={(e) => {
                                                    e.stopPropagation()
                                                    setWord(item.categoryName)
                                                    wordToServer.current = item.categoryName
                                                    page.current = 1
                                                    isAllNewsGot.current = false
                                                    news.current = []
                                                    news.current.length = 0
                                                    getNews()
                                                }} style={{ backgroundColor: backgroundColor, paddingTop: 4, paddingBottom: 4, paddingRight: 8, paddingLeft: 8, marginTop: 6, borderRadius: 16, fontSize: 10, marginLeft: 6 }}>{item.categoryName}</p>
                                                <p className="ThirdLightText" style={{ marginTop: 6, fontSize: 12 }}>{parseDate(item.dateCreate)}</p>
                                                <div style={{ flex: 1, display: 'flex', alignItems: 'center', direction: 'ltr', marginTop: 12, marginBottom: 4 }}>
                                                    {upDownLoadingId != item.id ? (
                                                        <img
                                                            className="clickHoverJustTooZoom"
                                                            onClick={(e) => {
                                                                e.stopPropagation()
                                                                upOrDown(index, 1, item.id, false, item.iDown != null && item.iDown == true)
                                                            }}
                                                            src={item.iDown && item.iDown == true ? ArroDownIcon : ArroDownOutlineIcon}
                                                            style={{ color: lightIconColor, opacity: 0.7, width: 16, height: 16 }}
                                                        />
                                                    ) : (
                                                        <CircularProgress size={16} style={{ marginRight: 4 }} color="secondary" />
                                                    )}
                                                    <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                        {parseNumber(item.downs)}
                                                    </p>
                                                    {upDownLoadingId != item.id ? (
                                                        <img
                                                            className="clickHoverJustTooZoom"
                                                            onClick={(e) => {
                                                                e.stopPropagation()
                                                                upOrDown(index, 1, item.id, true, item.iUp != null && item.iUp == true)
                                                            }}
                                                            src={item.iUp && item.iUp == true ? ArroUpIcon : ArroUpOutlineIcon}
                                                            style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }}
                                                        />
                                                    ) : (
                                                        <CircularProgress size={16} style={{ marginRight: 4, marginLeft: 4 }} color="secondary" />
                                                    )}
                                                    <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                        {parseNumber(item.ups)}
                                                    </p>

                                                    <img src={ViewIcon} style={{ colorAdjust: 'red', opacity: 0.5, width: 16, height: 16, marginLeft: 12 }} />
                                                    <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                                                        {parseNumber(item.seenCount)}
                                                    </p>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </GridList>
                        </div>
                    </PullToRefresh>
                </div>
                {isLoadingNews && news.current.length > 0 ? (
                    <div style={{ width: '100%', zIndex: 200 }}>
                        <div style={{ width: '100%' }}>
                            <LinearProgress color="secondary" />
                        </div>
                    </div>
                ) : (
                    <div></div>
                )}
            </div>)
            }
        </div >

    )
}

export default NewsScreen
