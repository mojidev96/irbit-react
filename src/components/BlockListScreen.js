import React, { useEffect, useState, useRef, createRef } from 'react'
import { AppBar, Toolbar, Dialog, Typography, Divider, Backdrop, Popover, DialogContentText, Select, GridList, Fab, Box, LinearProgress, IconButton, TextField, Checkbox, RadioGroup, Radio, MenuItem, FormControl, Button, CircularProgress, Grid, InputLabel } from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdAdd, MdExitToApp, MdMoreVert, MdOpenInNew } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { notifySuccessful, greenColor, token, notifyErrorMessage, webUrl } from '../App'
import { parsePrice, parseDate } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function BlockListScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const page = useRef(1)

    const [isLoadingList, setLoadingList] = useState(true)
    const [isLoading, setLoading] = useState(false)
    const [isAllGot, setAllGot] = useState(false)
    const [blocks, setBlocks] = useState([])

    useEffect(() => {
        props.socket.on('blockUnblock', (data) => {
            setLoading(false)
            var indexToRemove = null
            blocks.forEach((item, index) => {
                if (item.id === data.toUserId && !data.isBlocked) {
                    indexToRemove = index
                    //console.log('wcdes:: ' + indexToRemove)
                }
            })

            if (indexToRemove != null) {
                setBlocks((prv) => {
                    const d = prv
                    d.splice(indexToRemove, 1)
                    return [...d]
                })
            }
        })

        getBlockList()
        return () => {
            // Anything in here is fired on component unmount.
            window.onscroll = ''
        }
    }, [])

    const [isSendBlockUnblock, setIsSendBlockUnblock] = useState(false)
    const [selectedUserId, setSelectedUserId] = useState(0)
    useEffect(() => {
        if (isSendBlockUnblock) {
            setLoading(true)
            props.socket.emit('blockUnblock', { token: token, blockUserId: selectedUserId })
            setIsSendBlockUnblock(false)
        }
    }, [isSendBlockUnblock])

    const getBlockList = async () => {
        //console.log('wgbweg23:: ' + page.current + ' --- ' + isAllGot)
        if (isAllGot) {
            return
        }
        setLoadingList(true)
        try {
            const response = await Axios.get('blockList', {
                page: page.current,
            })

            //console.log(response)
            if (response.data instanceof Array && response.data.length > 0) {
                blocks.push(...response.data)
                ++page.current
                setBlocks(blocks)
            } else {
                setAllGot(true)
            }

            setLoadingList(false)
        } catch (error) {
            //console.log('err tickets :: ', error)
            setLoadingList(false)
        }
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        try {
            if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridLIst').offsetHeight) {
                //console.log('gwee')
                isWorkingScroll.current = 1
                getBlockList()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    return (
        <div className={classes.root} style={{ overflow: 'hidden' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        بلاک شده ها
                    </Typography>
                </Toolbar>
            </AppBar>
            {isLoading && (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            )}
            <div className="StackContainer" style={{ overflow: 'hidden' }}>
                <div style={{ paddingTop: 48, overflow: 'hidden' }}>
                    <GridList cellHeight="auto" id="gridLIst" cols={1}>
                        {blocks.map((item, index) => (
                            <div key={item.id}>
                                <Grid container>
                                    <div style={{ paddingRight: 16, paddingLeft: 16, marginTop: 16 }}>
                                        <div style={{ display: 'flex', alignItems: 'center' }}>
                                            <img className="avatar_image" style={{ width: 50, height: 50, borderRadius: 25 }} src={item.avatar != null && item.avatar.length > 0 ? webUrl + item.avatar : ''} />
                                            <div style={{ width: '100%', marginRight: 8, marginLeft: 16 }}>
                                                <p className="PrimaryText">{item.name}</p>
                                                <p className="ThirdLightText">{item.username}</p>
                                            </div>
                                            <Button
                                                variant="contained"
                                                onClick={() => {
                                                    setSelectedUserId(item.id)
                                                    setIsSendBlockUnblock(true)
                                                }}
                                                style={{ width: 70 }}
                                            >
                                                آنبلاک
                                            </Button>
                                        </div>
                                        <Divider style={{ width: window.innerWidth - 32, marginTop: 8 }} />
                                    </div>
                                </Grid>
                                {index == blocks.length - 1 && <div style={{ height: 16 }}></div>}
                                {index == blocks.length - 1 && isLoadingList ? (
                                    <div style={{ bottom: 0 }}>
                                        <div style={{ width: window.innerWidth }}>
                                            <LinearProgress color="secondary" />
                                        </div>
                                    </div>
                                ) : (
                                    <div></div>
                                )}
                            </div>
                        ))}
                    </GridList>
                </div>
            </div>
            {blocks.length == 0 && !isLoadingList ? <EmptyView marginBottom={0} /> : <div />}
            {blocks.length == 0 && isLoadingList ? (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="secondary" />
                </Backdrop>
            ) : (
                <div />
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default BlockListScreen
