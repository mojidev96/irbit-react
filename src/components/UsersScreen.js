import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    Card,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    GridListTile,
    InputBase,
} from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdClose, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew, MdSearch } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { accentcolor, backgroundColor, globalTabIndex, greenColor, lightIconColor, token, webUrl } from '../App'
import { parseDistance, parseDate } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'
import ProfileScreen from './ProfileScreen'
import noImage from '../icons/no_image_light.png'
import TryAgainWidget from '../widgets/TryAgainWidget'
import { isAndroid, isIOS } from 'react-device-detect'
import LoadingDialog from './dialogs/LoadingDialog'
import PullToRefresh from 'react-simple-pull-to-refresh'

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function UsersScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const page = useRef(1)

    const [isLoading, setLoading] = useState(true)
    const isAllGot = useRef(false)
    const users = useRef([])
    const [itHasProblem, setItHasProblem] = useState(false)

    const isWorkingScroll = useRef(0)

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem('usersData'))
        page.current = Number(localStorage.getItem('usersPage'))
        wordToServer.current = localStorage.getItem('usersWord')
        setWord(wordToServer.current)
        if (data.length > 0) {
            users.current.push(...data)
            setLoading(false)
            setItHasProblem(false)
        } else {
            getUsers()
        }

    }, [])

    useEffect(() => {
        if (globalTabIndex == 3 && users.current.length > 0) {
            window.scrollTo({ top: localStorage.getItem('usersScroll') })
        }
        window.onscroll = () => {
            if (window.pageYOffset != 0 && globalTabIndex == 3 && users.current.length > 0) {
                localStorage.setItem('usersScroll', window.pageYOffset)
            }
            try {
                if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridUsersList').offsetHeight) {
                    isWorkingScroll.current = 1
                    getUsers()
                    setTimeout(function () {
                        isWorkingScroll.current = 0
                    }, 500)
                }
            } catch (e) { }
        }

    }, [globalTabIndex, users.current.length])

    const wordToServer = useRef('')
    const [word, setWord] = useState('')

    const getUsers = async () => {
        if (isAllGot.current) {
            return
        }
        setLoading(true)
        setItHasProblem(false)
        try {
            const response = await Axios.post(
                'users/list',
                {
                    word: wordToServer.current,
                    page: page.current,
                },
                { headers: { token: token } }
            )

            //console.log(response)
            if (response.data instanceof Array && response.data.length > 0) {
                users.current.push(...response.data)
                ++page.current
                localStorage.setItem('usersData', JSON.stringify(users.current))
                localStorage.setItem('usersPage', page.current)
            } else {
                isAllGot.current = true
            }
        } catch (error) {
            //console.log('err filters :: ', error)
            setItHasProblem(true)
        }
        setLoading(false)
    }

    // handle showing profile dialog screen
    const [selectedUserId, setSelectedUserId] = useState(0)
    const [isShowingProfileDialog, setShowingProfileDialog] = useState(false)
    const showProfileScreen = (isShowing) => {
        if (isShowing) {
            return <ProfileScreen data={{ id: selectedUserId, onCloseFunc: closeProfileScreen }} />
        } else {
            return <div />
        }
    }

    const closeProfileScreen = () => {
        setShowingProfileDialog(false)
    }
    // end of profile dialog screen

    const handleRefresh = () => {
        setLoading(true)
        page.current = 1
        users.current = []
        users.current.length = 0
        isAllGot.current = false
        setTimeout(() => {
            getUsers()
        }, 100)
    }

    return (
        <div className={classes.root} style={{ overflow: 'hidden', direction: 'rtl', display: 'flex', justifyContent: 'center' }}>
            <div>
                {isLoading && users.current.length == 0 ? <LoadingDialog /> : <div />}
                {showProfileScreen(isShowingProfileDialog)}
                {!isLoading && itHasProblem ? (
                    <TryAgainWidget onTryAgain={getUsers} />
                ) : users.current.length == 0 && !isLoading && word.length == 0 ? (
                    <EmptyView />
                ) : (
                    <div style={{ display: 'flex', paddingTop: 46, flexWrap: 'wrap', justifyContent: 'space-around', overflow: 'hidden', width: '100%', maxWidth: 500 }}>
                        <div style={{ width: '100%', minWidth: (isAndroid || isIOS) ? 0 : 500, display: 'flex', justifyContent: 'center' }}>
                            <Card style={{ display: 'flex', alignItems: 'center', width: '100%', marginTop: 14, maxWidth: 500, borderRadius: (isAndroid || isIOS) ? 0 : 4 }}>
                                <InputBase
                                    placeholder="جستجو"
                                    onKeyDown={(e) => {
                                        if (e.key === 'Enter') {
                                            page.current = 1
                                            isAllGot.current = false
                                            users.current = []
                                            users.current.length = 0
                                            setTimeout(() => {
                                                getUsers()
                                            }, 100)
                                        }
                                    }}
                                    value={word}
                                    style={{ width: '100%', paddingTop: 6, paddingRight: 12, paddingLeft: 8 }}
                                    onChange={(e) => {
                                        localStorage.setItem('sea-man-vi-word', e.target.value)
                                        wordToServer.current = e.target.value
                                        setWord(e.target.value)
                                    }}
                                />
                                <IconButton
                                    onClick={() => {
                                        page.current = 1
                                        isAllGot.current = false
                                        if (word.length > 0) {
                                            setWord('')
                                            wordToServer.current = ''
                                            users.current = []
                                            users.current.length = 0
                                            setTimeout(() => {
                                                getUsers()
                                            }, 100)
                                        } else {
                                            getUsers()
                                        }
                                    }}
                                >
                                    {word.length > 0 ? <MdClose color="red" /> : <MdSearch color={lightIconColor} />}
                                </IconButton>
                            </Card>

                        </div>
                        <PullToRefresh pullingContent="بروزرسانی" isPullable={true} pullDownThreshold={50} onRefresh={async () => {
                            handleRefresh()
                            const p = new Promise((resolve, reject) => {
                                setTimeout(() => {
                                    resolve('ok')
                                }, 1500);
                            })
                            const resp = await p
                            return console.log(resp)
                        }}>
                            <GridList cellHeight="auto" id="gridUsersList" cols={2} style={{ paddingTop: 12, minWidth: (isAndroid || isIOS) ? 0 : 500, paddingBottom: 16, paddingLeft: (isAndroid || isIOS) ? 4 : 0, paddingRight: (isAndroid || isIOS) ? 4 : 0 }}>
                                {users.current.map((item) => (
                                    <GridListTile
                                        onClick={() => {
                                            setSelectedUserId(item.id)
                                            localStorage.setItem('filterUserId', 'uid-' + item.id)
                                            // setShowingProfileDialog(true)
                                            history.push({ pathname: 'user', search: '?id=' + item.id })
                                        }}
                                        id={'uid-' + item.id}
                                        key={item.id}
                                        style={{ minWidth: users.current.length == 1 ? 200 : 0 }}
                                    >
                                        <Card variant="elevation" elevation={2} style={{ boxShadow: '0 0.5px 3px 0px #0C0C0C', marginBottom: 8, marginRight: 4, marginLeft: 4 }}>
                                            <div className="StackContainer" style={{ height: (isAndroid || isIOS) ? 150 : 200 }}>
                                                <img className="image" src={item.avatar != null && item.avatar.length > 0 ? webUrl + item.avatar : noImage} style={{ width: '100%', height: (isAndroid || isIOS) ? 150 : 200 }}></img>
                                                <div
                                                    style={{
                                                        width: '100%',
                                                        position: 'absolute',
                                                        display: 'flex',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-start',
                                                    }}
                                                >
                                                    <p
                                                        className="ThirdText"
                                                        style={{
                                                            marginRight: 4,
                                                            marginLeft: 4,
                                                            marginTop: 4,
                                                            borderRadius: 4,
                                                            paddingTop: 1,
                                                            backgroundColor: backgroundColor,
                                                            marginTop: 4,
                                                            fontSize: 10,
                                                            paddingRight: 8,
                                                            paddingLeft: 8,
                                                            zIndex: 999,
                                                        }}
                                                    >
                                                        {parseDistance(item.distance)}
                                                    </p>
                                                </div>
                                            </div>
                                            <p className="SecondaryText" style={{ marginTop: 4, marginLeft: 8, marginRight: 8 }}>
                                                {item.name}
                                            </p>
                                            <div style={{ display: 'flex', flexWrap: 'wrap', paddingRight: 8, paddingLeft: 8, float: 'right', marginBottom: 4 }}>
                                                <p className="ThirdText" style={{ fontSize: 10, color: accentcolor, marginLeft: 4 }}>
                                                    {item.job}
                                                </p>
                                                <p className="ThirdText" style={{ marginLeft: 4, fontSize: 10, }}>
                                                    {item.city}
                                                </p>
                                                <p className="ThirdText" style={{ direction: 'rtl', fontSize: 10, textAlign: 'right', marginLeft: 4 }}>
                                                    {parseDate(item.dateLastOnline)}
                                                </p>
                                            </div>
                                        </Card>
                                    </GridListTile>
                                ))}
                            </GridList>
                        </PullToRefresh>
                        {isLoading && users.current.length > 0 ? (
                            <div style={{ width: '100%', zIndex: 200 }}>
                                <div style={{ width: '100%' }}>
                                    <LinearProgress color="secondary" />
                                </div>
                            </div>
                        ) : (
                            <div></div>
                        )}
                    </div>
                )}
            </div>
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div >
    )
}

export default UsersScreen
