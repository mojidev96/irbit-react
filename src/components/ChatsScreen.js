import React, { useEffect, useState, createRef, useRef } from 'react'
import PropTypes from 'prop-types'
import SwipeableViews from 'react-swipeable-views'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Typography, GridList, Grid, GridListTile, List, AppBar, Tabs, Tab, Box, IconButton, ListSubheader, Card, Button, CircularProgress, LinearProgress, Backdrop } from '@material-ui/core'
import Axios from 'axios'
import { accentcolor, greenColor, backgroundColor, userIdTestol, webUrl, account, globalTabIndex } from '../App'
import { MdAcUnit, MdFiberManualRecord } from 'react-icons/md'
import { parseDate, parseDistance, parsePrice } from '../tools/extentions'
import { token } from './../App'
import Whirligig from 'react-whirligig'
import LoginNeededView from './../widgets/LoginNeededView'
import { useHistory } from 'react-router-dom'
import NeedCompleteAccountView from './../widgets/NeedCompleteAccountView'
import EmptyView from './../widgets/EmptyView'
import TryAgainWidget from './../widgets/TryAgainWidget'
import LoadingDialog from './dialogs/LoadingDialog'
import PullToRefresh from 'react-simple-pull-to-refresh'

function ChatsScreen(props) {
    let history = useHistory()
    const [isLoadingList, setLoadingList] = useState(true)
    const isAllGot = useRef(false)
    const chats = useRef([])
    const page = useRef(1)
    const getChats = async () => {
        if (isAllGot.current) {
            return
        }
        setItHasProblem(false)
        setLoadingList(true)
        await Axios.post('messages/chats', {
            page: page.current,
        })
            .then((response) => {
                //console.log(response)
                if (response.data instanceof Array && response.data.length > 0) {
                    response.data.forEach((item) => {
                        props.socket.emit('join-online-users', { userId: item.senderUserId === account.id ? item.getterUserId : item.senderUserId, getBackResult: true })
                    })
                    chats.current.push(...response.data)
                    ++page.current
                    localStorage.setItem('chatsData', JSON.stringify(chats.current))
                    localStorage.setItem('chatsPage', page.current)
                } else {
                    isAllGot.current = true
                }
            })
            .catch((err) => {
                //console.log('getchats err : ' + JSON.stringify(err))
                setItHasProblem(true)
            })
        setLoadingList(false)
    }
    useEffect(() => {
        const data = JSON.parse(localStorage.getItem('chatsData'))
        page.current = Number(localStorage.getItem('chatsPage'))
        if (data.length > 0) {
            chats.current.push(...data)
            setLoadingList(false)
            setItHasProblem(false)
        } else {
            getChats()
        }

        props.socket.on('messages', (data) => {
            if (data.senderUserId != account.id) {
                chats.current.forEach((item) => {
                    if (data.chatId === item.id) {
                        item.newMessageCount = item.newMessageCount + 1
                        item.message = data.message
                        item.dateCreate = data.dateCreate
                    }
                })
            }
        })
    }, [])

    useEffect(() => {
        if (globalTabIndex == 4 && chats.current.length > 0) {
            window.scrollTo({ top: localStorage.getItem('chatsScroll') })
        }
        window.onscroll = () => {
            if (window.pageYOffset != 0 && globalTabIndex == 4 && chats.current.length > 0) {
                localStorage.setItem('chatsScroll', window.pageYOffset)
            }
            try {
                if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridChats').offsetHeight) {
                    isWorkingScroll.current = 1
                    getChats()
                    setTimeout(function () {
                        isWorkingScroll.current = 0
                    }, 500)
                }
            } catch (e) { }
        }

    }, [globalTabIndex, chats.current.length])

    const isWorkingScroll = useRef(0)
    const [itHasProblem, setItHasProblem] = useState(false)

    const pushToMessages = (chatId, byUserId, avatar, name, lastOnline) => {
        history.push({
            pathname: '/messages',
            data: {
                chatId: chatId,
                byUserId: byUserId,
                avatar: avatar,
                name: name,
                lastOnline: lastOnline,
            },
        })
    }

    const handleRefresh = () => {
        setLoadingList(true)
        page.current = 1
        chats.current = []
        chats.current.length = 0
        isAllGot.current = false
        setTimeout(() => {
            getChats()
        }, 100)
    }

    return (
        <div style={{ display: 'flex', direction: 'rtl', justifyContent: 'center' }}>
            {!isLoadingList && chats.current.length == 0 && localStorage.getItem('token') == null ? (
                <LoginNeededView data={{ minusHeight: 0 }} />
            ) : !isLoadingList && chats.current.length == 0 && JSON.parse(localStorage.getItem('account')).name == null ? (
                <NeedCompleteAccountView marginBottom={0} />
            ) : !isLoadingList && itHasProblem ? (
                <TryAgainWidget onTryAgain={getChats} style={{ marginTop: 8 }} />
            ) : !isLoadingList && chats.current.length === 0 ? (
                <EmptyView />
            ) : (
                <div style={{ width: '100%', maxWidth: 500 }}>
                    {isLoadingList && chats.current.length == 0 ? <LoadingDialog /> : <div />}
                    <PullToRefresh pullingContent="بروزرسانی" isPullable={true} pullDownThreshold={50} onRefresh={async () => {
                        handleRefresh()
                        const p = new Promise((resolve, reject) => {
                            setTimeout(() => {
                                resolve('ok')
                            }, 1500);
                        })
                        const resp = await p
                        return console.log(resp)
                    }}> <div style={{ paddingTop: 42, width: '100%', maxWidth: 500 }}>
                            <List id="gridChats" style={{ overflowX: 'hidden', width: '100%' }}>
                                {chats.current.map((data, index) => (
                                    <GridListTile key={data.id} className="clickable" style={{ width: '100%', maxWidth: 500 }} onClick={() => pushToMessages(data.id, account.id == data.senderUserId ? data.getterUserId : data.senderUserId, data.avatar, data.name, data.dateLastOnline)}>
                                        <div style={{ display: 'flex', width: '100%', maxWidth: 500, paddingRight: 16, paddingLeft: 16, marginTop: 16 }}>
                                            <div style={{ width: 80, height: 80 }}>
                                                <div className="StackContainer" style={{ width: 80, height: 80 }}>
                                                    <img src={data.avatar != null && data.avatar.length > 0 ? webUrl + data.avatar : null} className="avatar_image" style={{ width: 80, height: 80, borderRadius: 40, objectFit: 'cover' }}></img>
                                                    {data.newMessageCount > 0 && (
                                                        <p className="ThirdBoldText StackElement" style={{ textAlign: 'center', paddingTop: 4, backgroundColor: accentcolor, width: 28, height: 24, borderRadius: 15, color: 'white', top: 16, right: 16 }}>
                                                            {data.newMessageCount > 99 ? '+99' : data.newMessageCount}
                                                        </p>
                                                    )}
                                                </div>
                                            </div>
                                            <div style={{ width: '100%', marginRight: 12, marginLeft: 24, display: 'flex', flexDirection: 'column' }}>
                                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                                    {data.isOnline == true && <MdFiberManualRecord size={12} color={greenColor} style={{ marginLeft: 4, marginTop: 8 }} />}
                                                    <p className="PrimaryText" style={{ marginTop: 8 }}>
                                                        {data.name}
                                                    </p>
                                                </div>
                                                <p className="ThirdText" style={{ marginTop: 0 }}>
                                                    {data.message.length > 100 ? data.message.substring(0, 99) + ' ...' : data.message}
                                                </p>
                                                <p className="ThirdText" style={{ marginTop: 2 }}>
                                                    {data.city} - {data.district} - {parseDate(data.dateCreate)}
                                                </p>
                                            </div>
                                            {index == chats.current.length - 1 && <div style={{ height: 100 }} />}
                                        </div>
                                    </GridListTile>
                                ))}
                            </List>
                        </div>
                    </PullToRefresh>

                    {isLoadingList && chats.current.length > 0 ? (
                        <div style={{ width: '100%', zIndex: 200 }}>
                            <div style={{ width: '100%' }}>
                                <LinearProgress color="secondary" />
                            </div>
                        </div>) : (
                        <div></div>
                    )}
                </div>
            )}
        </div>
    )
}

export default ChatsScreen
