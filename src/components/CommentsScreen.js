import React, { useEffect, useState, useRef, createRef } from 'react'
import { AppBar, Toolbar, Dialog, Typography, DialogTitle, Backdrop, DialogContent, DialogContentText, DialogActions, GridList, Fab, Box, LinearProgress, IconButton, TextField, Checkbox, RadioGroup, Radio, MenuItem, FormControlLabel, Button, CircularProgress, Grid, Divider } from '@material-ui/core'
import { MdArrowForward, MdBookmarkBorder, MdDelete, MdEdit, MdExitToApp, MdExpandMore, MdMoreVert, MdOpenInNew, MdReply } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import { account, notifyWarnMessage, blueColor, webUrl, greenColor, notifyErrorMessage, token, lightIconColor, itemColor, notifySuccessful } from '../App'
import { parsePrice, parseDate, parseNumber } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import { cssTransition } from 'react-toastify'
import EmptyView from '../widgets/EmptyView'
import ProfileScreen from './ProfileScreen'

import ArroUpIcon from '../icons/up-arrow.png'
import ArroDownIcon from '../icons/down-arrow.png'
import ViewIcon from '../icons/view.png'
import TitleIcon from '../icons/title.png'
import ChatIcon from '../icons/chat.png'
import ArroUpOutlineIcon from '../icons/up-arrow-outline.png'
import ArroDownOutlineIcon from '../icons/down-arrow-outline.png'

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: 0,
        },
    },
    extendedIcon: {
        marginRight: 0,
    },
}))

function CommentsScreen(props) {
    let history = useHistory()
    const classes = useStyles()

    const contentId = useRef(null)
    const contentType = useRef(null)
    const commentsPage = useRef(1)

    const [isLoadingComments, setLoadingComments] = useState(true)
    const comments = useRef([])

    const [isOpenVoteDialog, setOpenVoteDialog] = useState(false)
    const [isLoadingSendVote, setLoadingSendVote] = useState(false)

    const [isOpenErrorDialog, setOpenErrorDialog] = useState(false)
    const [errorDialogTitle, setErrorDialogTitle] = useState()
    const [errorDialogMessage, setErrorDialogMessage] = useState()

    useEffect(() => {
        const search = props.location ? props.location.search : undefined
        if (search != undefined) {
            const params = new URLSearchParams(search)
            const conId = params.get('contentId')
            const conType = params.get('contentType')
            contentId.current = conId
            contentType.current = conType
        } else {
            contentId.current = props.id
            contentType.current = props.type
        }

        getComments()
        return () => {
            // Anything in here is fired on component unmount.
            window.onscroll = ''
        }
    }, [])

    useEffect(() => {
        setOpenVoteDialog(props.showingDialog ?? false)
    }, [props.showingDialog ?? false])

    const replyPages = useRef([])

    const getComments = async (isForSub, comId) => {

        setLoadingComments(true)

        var replyPage = null
        if (isForSub && replyPages.current.length > 0) {
            replyPage = replyPages.current.find(x => x.commentId === comId)
        }

        try {
            const response = await Axios.post('comments/list', {
                contentType: contentType.current,
                contentId: contentId.current,
                page: (isForSub ?? false) ? replyPage.page : commentsPage.current,
                isForSub: isForSub ?? false,
                commentId: comId ?? 0,
            }, { headers: { token: token } })

            if (response.data instanceof Array && response.data.length > 0) {
                if (isForSub && replyPages.current.length > 0) {
                    if (replyPage.page == 1) {
                        comments.current[replyPage.index].replies = []
                    }
                    comments.current[replyPage.index].replies.push(...response.data)
                    const i = replyPages.current.findIndex(x => x.commentId === comId)
                    replyPages.current[i].page = replyPages.current[i].page + 1
                    // console.log('egwgwe::: ' + replyPages.current[i].page + ' ---- ' + i)
                } else {
                    comments.current.push(...response.data)
                }
                ++commentsPage.current
            }

            setLoadingComments(false)
        } catch (error) {
            //console.log('err comments :: ', error)
            setLoadingComments(false)
        }
    }

    const deleteComment = async (comId, index, parentIndex) => {
        setLoadingComments(true)

        try {
            const response = await Axios.delete('comments', {
                data: { id: comId }
            })

            if (response.status == 200) {
                if (parentIndex > -1) {
                    comments.current[parentIndex].replies.splice(index, 1)
                } else {
                    comments.current.splice(index, 1)
                }
                notifySuccessful()
            } else {
                notifyErrorMessage(response.data.message ?? 'خطای نامشخص')
            }
        } catch (error) {
            notifyErrorMessage(error.response.data.message ?? 'خطای نامشخص')
        }
        setLoadingComments(false)
    }

    const [commentText, setCommentText] = useState('')
    const sendComment = async () => {
        if (commentText.length > 300 || commentText.length < 2) {
            notifyMinAndMaxForComment()
            return
        }
        setLoadingSendVote(true)
        try {

            const response = await Axios.post('comments', {
                comment: commentText,
                contentId: contentId.current,
                contentType: contentType.current,
                mainCommentId: mainCommentId.current ?? 0,
                isReply: isReply.current ?? false,
                replyId: replyId.current ?? 0
            })

            if (response.status == 201) {
                notifyIsRekeaseAfterAccepted()
                setOpenVoteDialog(false)
                setCommentText('')
            } else if (response.status == 401) {
                showErrorDialog('اشکال', 'لطفا وارد حساب کاربری خود شوید. اگر قبلا وارد شده بودید ممکن است دیتای شما پاک شده باشد.')
            } else {
                showErrorDialog('خطای نامشخص', 'با عرض پوزش لطفا دوباره تلاش کنید.')
            }
        } catch (error) {
            //console.log('err sendvote :: ', error)
        }
        setLoadingSendVote(false)
    }

    const isWorkingScroll = useRef(0)
    window.onscroll = () => {
        try {
            if (isWorkingScroll.current == 0 && commentsPage.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridVotes').offsetHeight) {
                //console.log('gwee')
                isWorkingScroll.current = 1
                getComments()
                setTimeout(function () {
                    isWorkingScroll.current = 0
                }, 500)
            }
        } catch (e) {
            //console.log('iosbuggy')
        }
    }

    const notifyIsRekeaseAfterAccepted = () => toast.success('سپاس؛ پس از تایید ناظر منتشر می شود.')
    const notifyMinAndMaxForComment = () => toast.error('لطفا بین ۲ تا ۳۰۰ کاراکتر وارد نمایید.', { autoClose: 3500 })

    const showErrorDialog = (title, message) => {
        setErrorDialogTitle(title)
        setErrorDialogMessage(message)
        setOpenErrorDialog(true)
    }

    const changeRateComment = (event) => {
        setCommentText(event.target.value)
    }

    // handle showing profile dialog screen
    const [selectedUserId, setSelectedUserId] = useState(0)
    const [isShowingProfileDialog, setShowingProfileDialog] = useState(false)
    const showProfileScreen = (isShowing) => {
        if (isShowing) {
            return <ProfileScreen data={{ id: selectedUserId, onCloseFunc: closeProfileScreen }} />
        } else {
            return <div />
        }
    }

    const closeProfileScreen = () => {
        setShowingProfileDialog(false)
    }
    // end of profile dialog screen

    const isReply = useRef(false)
    const replyId = useRef(0) // آیدی کسیکه داریم تو جواب کامنتش کامنت میدیم
    const mainCommentId = useRef(0) // کامنتی که داریم بهش پاسخ میدیم
    const replyToMessageText = useRef('')

    const [upDownLoadingId, setUpDownLoadingId] = useState(0)
    // age reply bashe bayad parentIndex sho befrestim
    const upOrDown = async (index, contentType, contentId, isUp, isForgive, parentIndex) => {
        if (localStorage.getItem('token') == null) {
            history.push('/login')
            return
        }

        setUpDownLoadingId(contentId)
        try {
            const response = await Axios.post('upAndDowns/', {
                isUp,
                isForgive,
                contentId,
                contentType,
            })

            if (response.status == 201) {
                if (isUp == true) {
                    if (parentIndex > -1) {
                        if (comments.current[parentIndex].replies[index].iDown != null && comments.current[parentIndex].replies[index].iDown == true) {
                            comments.current[parentIndex].replies[index].iDown = false
                            comments.current[parentIndex].replies[index].downs -= 1
                        }
                    } else {
                        if (comments.current[index].iDown != null && comments.current[index].iDown == true) {
                            comments.current[index].iDown = false
                            comments.current[index].downs -= 1
                        }
                    }
                } else {
                    if (parentIndex > -1) {
                        if (comments.current[parentIndex].replies[index].iUp != null && comments.current[parentIndex].replies[index].iUp == true) {
                            comments.current[parentIndex].replies[index].iUp = false
                            comments.current[parentIndex].replies[index].ups -= 1
                        }
                    } else {
                        if (comments.current[index].iUp != null && comments.current[index].iUp == true) {
                            comments.current[index].iUp = false
                            comments.current[index].ups -= 1
                        }
                    }
                }

                if (isUp == true) {
                    if (parentIndex > -1) {
                        comments.current[parentIndex].replies[index].iUp = isForgive == false
                    } else {
                        comments.current[index].iUp = isForgive == false
                    }
                } else {
                    if (parentIndex > -1) {
                        comments.current[parentIndex].replies[index].iDown = isForgive == false
                    } else {
                        comments.current[index].iDown = isForgive == false
                    }
                }

                if (isUp == true) {
                    if (isForgive == false) {
                        if (parentIndex > -1) {
                            comments.current[parentIndex].replies[index].ups += 1
                        } else {
                            comments.current[index].ups += 1
                        }
                    } else {
                        if (parentIndex > -1) {
                            comments.current[parentIndex].replies[index].ups -= 1
                        } else {
                            comments.current[index].ups -= 1
                        }

                    }
                } else {
                    if (isForgive == false) {
                        if (parentIndex > -1) {
                            comments.current[parentIndex].replies[index].downs += 1
                        } else {
                            comments.current[index].downs += 1
                        }
                    } else {
                        if (parentIndex > -1) {
                            comments.current[parentIndex].replies[index].downs -= 1
                        } else {
                            comments.current[index].downs -= 1
                        }
                    }
                }

            } else {
                notifyErrorMessage('نشد که بشه... لطفا دوباره امتحان کنید.')
            }
        } catch (error) { }
        setUpDownLoadingId(0)
    }

    const commentRowView = (item, isNotReply, index, parentIndex) => {
        return <div style={{ display: 'flex', width: '100%', paddingRight: 16, paddingLeft: 16, marginTop: 16 }}>
            <div style={{ float: 'right' }}>
                <img src={item.avatar != null && item.avatar.length > 0 ? webUrl + item.avatar : null} className="image" onClick={(e) => {
                    e.stopPropagation()
                    setSelectedUserId(item.commenterId)
                    // setShowingProfileDialog(true)
                    history.push({ pathname: 'user', search: '?id=' + item.commenterId })
                }} style={{ width: isNotReply ? 50 : 40, height: isNotReply ? 50 : 40, borderRadius: isNotReply ? 25 : 20 }}></img>
            </div>
            <div style={{ marginRight: 12, display: 'flex', flex: 1, flexDirection: 'column' }}>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <p className="PrimaryText" style={{ marginTop: 2 }}>
                        {item.name}
                    </p>
                    {(item.replyToId && item.replyToId > 0) && <div onClick={(e) => {
                        e.stopPropagation()
                        setSelectedUserId(item.replyToId)
                        // setShowingProfileDialog(true)
                        history.push({ pathname: '/user', search: '?id=' + item.replyToId })
                    }} style={{ marginRight: 8, marginTop: 16, height: 30 }}>
                        <p className="ThirdLightText" style={{ fontSize: 10, left: 16 }}>در پاسخ {item.replyToName}</p>
                    </div>}
                </div>
                <div style={{ marginTop: 4, marginLeft: 16 }}>
                    <p className="ThirdText">
                        {item.comment}
                    </p>

                </div>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <p className="ThirdLightText" >
                        {parseDate(item.dateCreate)}
                    </p>
                    <div className="clickable" onClick={(e) => {
                        e.stopPropagation()
                        isReply.current = true
                        replyId.current = item.id
                        replyToMessageText.current = item.comment
                        mainCommentId.current = item.id
                        if (account != null && account.name != null) {
                            setOpenVoteDialog(true)
                        } else {
                            notifyWarnMessage('لطفا اطلاعات حساب کاربری خود را تکمیل نمایید.')
                        }
                    }} style={{ display: 'flex', marginRight: 8, alignItems: 'center', height: 30, width: 50 }}>
                        <p className="ThirdBoldText">
                            پاسخ
                        </p>
                        <MdReply size={14} color="white" />
                    </div>
                    <div style={{ display: 'flex', alignItems: 'center', direction: 'ltr', marginTop: 4, marginBottom: 4 }}>
                        {upDownLoadingId != item.id ? (
                            <img
                                className="clickHoverJustTooZoom"
                                onClick={(e) => {
                                    e.stopPropagation()
                                    upOrDown(index, 3, item.id, false, item.iDown != null && item.iDown == true, parentIndex)
                                }}
                                src={item.iDown && item.iDown == true ? ArroDownIcon : ArroDownOutlineIcon}
                                style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16 }}
                            />
                        ) : (
                            <CircularProgress size={16} style={{ marginRight: 4 }} color="secondary" />
                        )}
                        <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                            {parseNumber(item.downs)}
                        </p>
                        {upDownLoadingId != item.id ? (
                            <img
                                className="clickHoverJustTooZoom"
                                onClick={(e) => {
                                    e.stopPropagation()
                                    upOrDown(index, 3, item.id, true, item.iUp != null && item.iUp == true, parentIndex)
                                }}
                                src={item.iUp && item.iUp == true ? ArroUpIcon : ArroUpOutlineIcon}
                                style={{ colorAdjust: 'red', opacity: 0.7, width: 16, height: 16, marginLeft: 12 }}
                            />
                        ) : (
                            <CircularProgress size={16} style={{ marginRight: 4, marginLeft: 4 }} color="secondary" />
                        )}
                        <p className="ThirdBoldText" style={{ marginLeft: 4, opacity: 0.7 }}>
                            {parseNumber(item.ups)}
                        </p>
                    </div>
                    {account && account.id && item.commenterId == account.id && <MdDelete size={24} onClick={() => deleteComment(item.id, index, parentIndex)} style={{ color: itemColor, marginRight: 8 }} />}
                </div>
            </div>
        </div>
    }

    return (
        <div className={classes.root} style={{ width: '100%', overflow: 'hidden', display: 'flex', justifyContent: 'center' }}>
            {props.location && props.location.search && <AppBar position="fixed" fullWidth style={{ width: '100%' }}>
                <Toolbar variant="dense" style={{ width: '100%' }}>
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        نظرات
                    </Typography>
                </Toolbar>
            </AppBar>}

            {showProfileScreen(isShowingProfileDialog)}
            <Dialog open={isOpenVoteDialog} maxWidth="lg" onClose={() => {
                setOpenVoteDialog(false)
                if(props.changeDialogState){
                    props.changeDialogState()
                }
            }} aria-labelledby="commentDialog">
                <div style={{ padding: 16, justifyContent: 'center' }}>
                    <p className="PrimaryText" style={{ direction: 'rtl', fontSize: isReply.current ? 16 : 20, textAlign: 'center' }}>{isReply.current ? 'در پاسخ: ' + replyToMessageText.current : 'ثبت نظر'}</p>
                    {isLoadingSendVote ? (
                        <div style={{ display: 'flex', minWidth: 200, marginTop: 12, marginBottom: 12, justifyContent: 'center' }}>
                            <CircularProgress color="secondary" />
                        </div>
                    ) : (
                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
                            <TextField multiline label="متن" value={commentText} onChange={changeRateComment} style={{ width: '100%', marginTop: 16 }} />
                        </div>
                    )}
                    <Button variant="contained" disabled={isLoadingSendVote} onClick={sendComment} fullWidth={true} style={{ marginTop: 12, backgroundColor: blueColor, color: 'white' }}>
                        ثبت
                    </Button>
                </div>
            </Dialog>
            <Dialog fullScreen={false} open={isOpenErrorDialog} onClose={() => setOpenErrorDialog(false)} aria-labelledby="responsive-dialog-error">
                <DialogTitle id="responsive-dialog-error">{errorDialogTitle}</DialogTitle>
                <DialogContent>
                    <DialogContentText>{errorDialogMessage}</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setOpenErrorDialog(false)} style={{ backgroundColor: blueColor, color: 'white' }}>
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>

            <div style={{ width: '100%', position: 'fixed', zIndex: 200, bottom: 16, left: 16, direction: 'ltr' }}>
                <Fab
                    onClick={() => {
                        isReply.current = false
                        if (account != null && account.name != null) {
                            setOpenVoteDialog(true)
                        } else {
                            notifyWarnMessage('لطفا اطلاعات حساب کاربری خود را تکمیل نمایید.')
                        }
                    }}
                    color="secondary"
                    aria-label="addVote"
                >
                    <MdEdit size={24} />
                </Fab>
            </div>
            {comments.current.length == 0 && !isLoadingComments && contentType.current != 3 ? (
                <EmptyView />
            ) : (
                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%', maxWidth: 500 }}>
                    <div style={{ width: '100%', paddingTop: props.location && props.location.search ? 48 : 8, overflow: 'hidden' }}>
                        <GridList cellHeight="auto" id="gridVotes" cols={1} style={{ width: '100%', paddingBottom: 200, overflowX: 'hidden' }}>
                            {comments.current.map((item, index) => (
                                <div key={item.id} style={{ width: '100%', }}>
                                    <Grid container style={{ width: '100%', }}>
                                        {commentRowView(item, true, index)}
                                        {item.repliesCount > 0 && <div>
                                            {item.replies.map((reply, replyIndex) => (<div style={{ width: '80%', marginRight: 50 }}>
                                                {commentRowView(reply, false, replyIndex, index)}
                                            </div>))}
                                            {item.repliesCount > 2 && <div onClick={(e) => {
                                                e.stopPropagation()
                                                const replyPage = replyPages.current.find(x => x.commentId === item.id)

                                                if (replyPage == null) {
                                                    replyPages.current.push({ commentId: item.id, page: 1, index })
                                                }

                                                getComments(true, item.id)
                                            }} className="clickable" style={{ width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: 8, }}>
                                                <p className="ThirdBoldText" style={{ textAlign: 'center', marginLeft: 4, color: greenColor, marginRight: 55 }}>نمایش پاسخ های بیشتر</p>
                                                <MdExpandMore size={16} color={greenColor} />
                                            </div>}
                                        </div>}
                                    </Grid>
                                </div>
                            ))}
                        </GridList>
                    </div>
                </div>
            )}
            {comments.current.length > 0 && isLoadingComments ? (
                <div style={{ bottom: 0, zIndex: 200, position: 'fixed', bottom: 0, right: 0, left: 0, width: '100%' }}>
                    <div style={{ width: '100%' }}>
                        <LinearProgress color="secondary" />
                    </div>
                </div>
            ) : (
                <div></div>
            )}
            {comments.current.length == 0 && isLoadingComments ? (
                <Backdrop style={{ zIndex: 5, color: '#fff', paddingTop: 48 }} open={true}>
                    <CircularProgress color="white" />
                </Backdrop>
            ) : (
                <div />
            )}
            <ToastContainer rtl closeOnClick position="top-center" transition={Slide} />
        </div>
    )
}

export default CommentsScreen
