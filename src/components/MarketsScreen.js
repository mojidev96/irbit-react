import React, { useEffect, useState, useRef, createRef } from 'react'
import {
    AppBar,
    Toolbar,
    Dialog,
    Typography,
    Card,
    Backdrop,
    DialogContent,
    DialogContentText,
    DialogActions,
    GridList,
    Fab,
    Box,
    LinearProgress,
    IconButton,
    TextField,
    Checkbox,
    RadioGroup,
    Radio,
    MenuItem,
    FormControlLabel,
    Button,
    CircularProgress,
    Grid,
    Divider,
    GridListTile,
    InputBase,
    Accordion,
    AccordionSummary,
    AccordionDetails,
} from '@material-ui/core'

import AnimatedNumber from "animated-number-react";
import { MdArrowForward, MdBookmarkBorder, MdClose, MdEdit, MdExitToApp, MdMoreVert, MdOpenInNew, MdRoom, MdSearch } from 'react-icons/md'
import Rating from '@material-ui/lab/Rating'
import { useHistory } from 'react-router-dom'
import { useSpring, animated } from 'react-spring'
import Axios from 'axios'
import { accentcolor, backgroundColor, globalTabIndex, greenColor, itemColor, lightIconColor, notifyErrorMessage, token, webUrl } from '../App'
import { parseDistance, parseDate, parseNumber } from '../tools/extentions'
import { makeStyles } from '@material-ui/core/styles'
import { ToastContainer, toast, Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import EmptyView from '../widgets/EmptyView'
import TryAgainWidget from '../widgets/TryAgainWidget'
import { isAndroid, isIOS } from 'react-device-detect'
import LoadingDialog from './dialogs/LoadingDialog'
import ChatRoomPNG from '../icons/chatroom.png'
import PullToRefresh from 'react-simple-pull-to-refresh'
import MarketPriceDialog from './dialogs/MarketPriceDialog'
import UpChartPNG from '../icons/upchart.png'
import DownChartPNG from '../icons/downchart.png'

function MarketsScreen(props) {
    let history = useHistory()
    const [isLoadingMarkets, setIsLoadingMarkets] = useState(true)
    const [isPriceChanged, setIsPriceChanged] = useState(true)
    const page = useRef(1)
    const isAllMarketsGot = useRef(false)
    const isWorkingScroll = useRef(0)

    const [colsNumber, setColsNumber] = useState(300)

    const markets = useRef([])
    const wordToServer = useRef('')
    const [word, setWord] = useState('')

    useEffect(() => {
        const data = JSON.parse(localStorage.getItem('marketsData'))
        wordToServer.current = localStorage.getItem('marketsWord')
        page.current = Number(localStorage.getItem('marketsPage'))
        setWord(wordToServer.current)

        if (data.length > 0) {
            markets.current.push(...data)
            setIsLoadingMarkets(false)
            setItHasProblem(false)
        } else {
            getMarkets()
        }

        props.socket.on('coinMessages', (data) => {
            if (data.price) {
                setIsPriceChanged(false)
                const index = markets.current.map(e => e.id).indexOf(Number(data.coinId));
                if (index > -1 && index < markets.current.length && data.price != markets.current[index].price) {
                    markets.current[index] = { ...markets.current[index], isUp: data.price < markets.current[index].price, price: data.price, oneDayChange: data.oneDayChange, canAnimate: true }
                    // setIsPriceChanged(true)
                    // setTimeout(() => {
                    //     markets.current[index] = { ...markets.current[index], canAnimate: false }
                    //     setIsPriceChanged(true)
                    // }, 100);

                }
                setTimeout(() => {
                    markets.current.forEach(element => {
                        element = { ...element, canAnimate: false }
                    });
                    // markets.current[index] = { ...markets.current[index], canAnimate: false }
                    setIsPriceChanged(true)
                }, 3000);
            }
        })

    }, [])
    useEffect(() => {
        setColsNumber((props.width / 350).toFixed(0))
    }, [props.width])

    useEffect(() => {
        if (globalTabIndex == 0 && markets.current.length > 0) {
            window.scrollTo({ top: localStorage.getItem('marketsScroll') })
        }

        window.onscroll = () => {
            if (window.pageYOffset != 0 && globalTabIndex == 0 && markets.current.length > 0) {
                localStorage.setItem('marketsScroll', window.pageYOffset)
            }
            try {
                if (isWorkingScroll.current == 0 && page.current > 1 && window.pageYOffset + window.innerHeight + 200 >= window.document.getElementById('gridMarketsList').offsetHeight) {
                    isWorkingScroll.current = 1
                    getMarkets()
                    setTimeout(function () {
                        isWorkingScroll.current = 0
                    }, 500)
                }
            } catch (e) { }
        }
    }, [globalTabIndex])

    const getMarkets = async () => {
        if (isAllMarketsGot.current) {
            return
        }

        setIsLoadingMarkets(true)
        setItHasProblem(false)

        try {
            const response = await Axios.post('coins', { page: page.current, name: wordToServer.current })
            if (response.data instanceof Array && response.data.length > 0) {
                markets.current.push(...response.data)
                response.data.forEach((elem) => props.socket.emit('joinPrices', { coinId: elem.id }))
                ++page.current
                localStorage.setItem('marketsData', JSON.stringify(markets.current))
                localStorage.setItem('marketsPage', page.current)
            }
        } catch (error) {
            setItHasProblem(true)
            // console.log('err filters :: ', error)
        }
        setIsLoadingMarkets(false)
    }

    const [itHasProblem, setItHasProblem] = useState(false)

    const handleRefresh = () => {
        setIsLoadingMarkets(true)
        page.current = 1
        markets.current = []
        markets.current.length = 0
        setTimeout(() => {
            getMarkets()
        }, 100)
    }

    // handle showing profile dialog screen
    const selectedCoinData = useRef()
    const [isShowingCoinDialog, setShowingCoinDialog] = useState()
    const showCoinDialog = (isShowing) => {
        if (isShowing) {
            return <MarketPriceDialog data={{ data: selectedCoinData.current, onCloseFunc: closeCoinDialog }} />
        } else {
            return <div />
        }
    }

    const closeCoinDialog = () => {
        setShowingCoinDialog(false)
    }
    // end of profile dialog screen

    const formatValue = (value) => value.toFixed(8);
    const formatValue2 = (value) => value.toFixed(2);

    return (
        <div style={{ direction: 'rtl' }}>
            {showCoinDialog(isShowingCoinDialog)}
            {!isLoadingMarkets && itHasProblem ? (
                <TryAgainWidget marginBottom={32} onTryAgain={getMarkets} />
            ) : markets.current.length == 0 && !isLoadingMarkets && word.length == 0 ? (
                <EmptyView marginBottom={24} />
            ) : (<div style={{ overflowY: 'hidden', paddingTop: 48 }} role="tabpanel" id={`full-width-tabpanel-${props.index}`} aria-labelledby={`full-width-tab-${props.index}`}>
                {isLoadingMarkets && markets.current.length == 0 ? <LoadingDialog /> : <div />}
                <div style={{ width: '100%', display: 'flex', justifyContent: 'center' }}>
                    <Card style={{ backgroundColor: itemColor, display: 'flex', alignItems: 'center', marginTop: 14, width: '100%', maxWidth: 500, borderRadius: (isAndroid || isIOS) ? 0 : 4 }}>
                        <InputBase
                            placeholder="جستجو"
                            onKeyDown={(e) => {
                                if (e.key === 'Enter') {
                                    page.current = 1
                                    isAllMarketsGot.current = false
                                    markets.current = []
                                    markets.current.length = 0
                                    setTimeout(() => {
                                        getMarkets()
                                    }, 100)
                                }
                            }}
                            value={word}
                            style={{ width: '100%', paddingTop: 6, paddingRight: 16, paddingLeft: 8 }}
                            onChange={(e) => {
                                localStorage.setItem('marketsWord', e.target.value)
                                wordToServer.current = e.target.value
                                setWord(e.target.value)
                            }}
                        />
                        <IconButton
                            onClick={() => {
                                page.current = 1
                                isAllMarketsGot.current = false
                                if (word.length > 0) {
                                    setWord('')
                                    wordToServer.current = ''
                                    markets.current = []
                                    markets.current.length = 0
                                    setTimeout(() => {
                                        getMarkets()
                                    }, 100)
                                } else {
                                    getMarkets()
                                }
                            }}
                        >
                            {word.length > 0 ? <MdClose color="red" /> : <MdSearch color={lightIconColor} />}
                        </IconButton>
                    </Card>

                </div>

                <div className="StackContainer" style={{ overflow: 'hidden', width: '100%' }}>

                    <PullToRefresh pullingContent="بروزرسانی" isPullable={true} pullDownThreshold={50} onRefresh={async () => {
                        handleRefresh()
                        const p = new Promise((resolve, reject) => {
                            setTimeout(() => {
                                resolve('ok')
                            }, 1500);
                        })
                        const resp = await p
                        return console.log(resp)
                    }}> <div style={{ paddingTop: 0, width: '100%', overflow: 'hidden', display: 'flex', justifyContent: 'center', minHeight: window.innerHeight }}>
                            {isPriceChanged ? <div></div> : <div style={{ height: 0, width: 1 }}></div>}
                            <GridList cellHeight={(isAndroid || isIOS) ? "auto" : 125} id="gridMarketsList" cols={(isAndroid || isIOS) ? 1 : colsNumber} style={{ width: '100%', paddingTop: (isAndroid || isIOS) ? 0 : 4, justifyContent: 'center', paddingBottom: 100 }}>
                                {markets.current.map((item, index) => (
                                    <div className="clickable" key={item.id} onClick={() => window.open('https://www.tradingview.com/symbols/' + item.symbol + 'USDT/')} style={{ width: (isAndroid || isIOS) ? '100%' : 350, height: (isAndroid || isIOS) ? "auto" : 125, backgroundColor: itemColor, marginLeft: (!isAndroid && !isIOS) ? 4 : 0, marginRight: (!isAndroid && !isIOS) ? 4 : 0, paddingTop: 16, paddingRight: 16, paddingLeft: 4, borderRadius: isAndroid || isIOS ? 0 : 4, marginTop: (isAndroid || isIOS) ? 12 : 8, display: 'flex' }}>
                                        <img src={item.logoUrl} style={{ height: 80, width: 80, padding: 8, backgroundColor: '#646464', borderRadius: 4 }} />
                                        <div style={{ flex: 1, paddingTop: 8, paddingRight: 8, paddingLeft: 8, marginRight: 4, marginLeft: 4 }}>
                                            <div style={{ display: 'flex', alignItems: 'start' }}>
                                                <div style={{ display: 'flex', alignItems: 'center' }}>
                                                    <p className="PrimaryText" style={{ color: '#646464', marginLeft: 4 }}>{index + 1}. </p>
                                                    <h4 className="PrimaryText">{item.symbol}</h4>
                                                </div>
                                                <p className="ThirdText" style={{ marginTop: 6, marginRight: 2, fontSize: 10 }}>{item.name}</p>


                                                {item.oneDayChange && <AnimatedNumber className={item.oneDayChange > 0 ? "animated-number-green" : "animated-number-red"}
                                                    style={{ color: item.oneDayChange > 0 ? greenColor : accentcolor, flex: 1, textAlign: 'left', direction: 'ltr', fontSize: 18 }}
                                                    value={(item.oneDayChange * 100 / item.price).toFixed(2)}
                                                    formatValue={formatValue2}
                                                />}
                                                {item.oneDayChange > 0 && <p className="PrimaryText" style={{ color: greenColor }}>+</p>}
                                                {/* {item.oneDayChange && <p className={"PrimaryText" + (item.canAnimate && item.canAnimate == true ? " animate-flicker" : "")} style={{ color: item.oneDayChange > 0 ? greenColor : accentcolor, flex: 1, textAlign: 'left', direction: 'ltr', fontSize: 18 }}>{item.oneDayChange > 0 ? '+' : ''}{(item.oneDayChange * 100 / item.price).toFixed(2)}%</p>} */}
                                            </div>
                                            <div style={{ display: 'flex', alignItems: 'center', direction: 'rtl' }}>
                                                <img src={(item.isUp === true || item.oneDayChange > 0) ? UpChartPNG : DownChartPNG} style={{ width: 16, height: 16, marginLeft: 4 }} />
                                                <AnimatedNumber className="animated-number-price"
                                                    value={item.price}
                                                    formatValue={formatValue}
                                                />
                                                <p className="PrimaryText" style={{ color: '#999999', marginTop: 4, marginRight: 4, fontSize: 14 }}>$</p>
                                            </div>
                                            {(!isAndroid && !isIOS) ? <div style={{ display: 'flex', alignItems: 'center', width: '100%' }}>
                                                <Button variant="outlined" fullWidth onClick={(e) => {
                                                    e.stopPropagation()
                                                    selectedCoinData.current = item
                                                    setShowingCoinDialog(true)
                                                }} style={{ flex: 1, marginTop: 4, textAlign: 'right' }}>
                                                    اطلاعات
                                                </Button>
                                                <img className="clickable" src={ChatRoomPNG} onClick={(e) => {
                                                    e.stopPropagation()
                                                    history.push({
                                                        pathname: '/coin-room',
                                                        search: '?coinId=' + item.id
                                                    })
                                                }
                                                } style={{ width: 24, height: 24, marginRight: 12 }} />
                                            </div> : <Accordion style={{ backgroundColor: (isAndroid || isIOS) ? 'transparent' : itemColor, zIndex: 200, margin: 0, padding: 0, marginTop: -6 }} onChange={(event, newExpanded) => {
                                                event.stopPropagation()
                                            }}>
                                                <AccordionSummary style={{ margin: 0, padding: 0, width: '100%' }}>
                                                    <div style={{ display: 'flex', alignItems: 'center', width: '100%' }}>
                                                        <Button variant="outlined" fullWidth style={{ flex: 1, textAlign: 'right' }}>
                                                            اطلاعات
                                                        </Button>
                                                        <img src={ChatRoomPNG} onClick={() =>
                                                            history.push({
                                                                pathname: '/coin-room',
                                                                search: '?coinId=' + item.id
                                                            }
                                                            )} style={{ width: 24, height: 24, marginRight: 12 }} />
                                                    </div>
                                                </AccordionSummary>
                                                <AccordionDetails style={{ zIndex: 900, margin: 0, paddingTop: 0, paddingRight: (isAndroid || isIOS) ? 0 : 12, paddingLeft: (isAndroid || isIOS) ? 0 : 12, paddingBottom: 16 }}>
                                                    <div style={{ zIndex: 900, width: '100%', marginLeft: 8 }}>
                                                        <p className="ThirdENText" style={{ fontSize: 12, marginTop: -4 }}>ارزش بازار : {parseNumber(item.marketCap)}</p>
                                                        <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>حداکثر قیمت : {item.high} $</p>
                                                        <p className="ThirdText" style={{ color: '#A5A5A5', fontSize: 12, marginTop: 0 }}>تاریخ حداکثر قیمت : {parseDate(item.highTimestamp)}</p>

                                                        {item.info['1d'] && item.info['1d']['volume'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 8 }}>حجم ۱ روزه : {item.info['1d']['volume']}</p>}
                                                        {item.info['1d'] && item.info['1d']['price_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات قیمت ۱ روزه : {item.info['1d']['price_change']} $</p>}
                                                        {item.info['1d'] && item.info['1d']['volume_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات حجم ۱ روزه : {item.info['1d']['volume_change']}</p>}

                                                        {item.info['7d'] && item.info['7d']['volume'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 8 }}>حجم ۷ روزه : {item.info['7d']['volume']}</p>}
                                                        {item.info['7d'] && item.info['7d']['price_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات قیمت ۷ روزه : {item.info['7d']['price_change']} $</p>}
                                                        {item.info['7d'] && item.info['7d']['volume_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات حجم ۷ روزه : {item.info['7d']['volume_change']}</p>}

                                                        {item.info['30d'] && item.info['30d']['volume'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 8 }}>حجم ۳۰ روزه : {item.info['30d']['volume']}</p>}
                                                        {item.info['30d'] && item.info['30d']['price_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات قیمت ۳۰ روزه : {item.info['30d']['price_change']} $</p>}
                                                        {item.info['30d'] && item.info['30d']['volume_change'] && <p className="ThirdENText" style={{ fontSize: 12, marginTop: 0 }}>تغیرات حجم ۳۰ روزه : {item.info['30d']['volume_change']}</p>}
                                                    </div>
                                                </AccordionDetails>
                                            </Accordion>}
                                        </div>
                                    </div>
                                ))}
                            </GridList>
                        </div>
                    </PullToRefresh>
                </div>
                {isLoadingMarkets && markets.current.length > 0 ? (
                    <div style={{ width: '100%', zIndex: 200 }}>
                        <div style={{ width: '100%' }}>
                            <LinearProgress color="secondary" />
                        </div>
                    </div>
                ) : (
                    <div></div>
                )}
            </div>)}
        </div>

    )
}

export default MarketsScreen
