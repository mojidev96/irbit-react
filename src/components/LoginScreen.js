import React, { useState, useEffect } from 'react'
import '../App.css'
import Axios from 'axios'
import Logo from '../icons/logo1024.png'
import { useHistory } from 'react-router-dom'
import { AppBar, Toolbar, Typography, IconButton, TextField, Backdrop, RadioGroup, Radio, FormControlLabel, Button, CircularProgress, Grid } from '@material-ui/core'
import { MdArrowForward, MdExitToApp } from 'react-icons/md'
import { DialogTitle, Dialog, DialogActions, DialogContentText, DialogContent } from '@material-ui/core'
import { token, blueColor, account } from '../App'
import { fixNumbers } from '../tools/extentions'

function LoginScreen() {
    let history = useHistory()
    const [isLoading, setLoading] = useState(false)
    const [isOnPhoneView, setIsOnPhoneView] = useState(true)

    const [isOpenErrorDialog, setOpenErrorDialog] = useState(false)
    const [isOpenPhoneWarnDialog, setOpenPhoneWarnDialog] = useState(false)

    const [phone, setPhone] = React.useState('')
    const [code, setCode] = React.useState('')

    const handleChange = (event) => {
        if (isOnPhoneView) {
            setPhone(event.target.value)
        } else {
            setCode(event.target.value)
        }
    }

    const sendSMS = async () => {
        if (phone.length === 0 || phone.length > 11) {
            showWarningPhoneDialog()
            return
        }
        setLoading(true)
        try {
            const response = await Axios.post('users/sendSMS', { phone: fixNumbers(phone) })
            if (response.status == 201) {
                setIsOnPhoneView(false)
            } else {
                if (response.data != null && response.data.message != null) {
                    setSignInProblemText(response.data.message)
                } else {
                    setSignInProblemText('اشکال در برقراری ارتباط لطفا دوباره تلاش کنید.')
                }
                showErrorDialog()
            }
        } catch (error) {
            if (error.response.data != null && error.response.data.message != null) {
                setSignInProblemText(error.response.data.message)
            } else {
                setSignInProblemText('اشکال در برقراری ارتباط لطفا دوباره تلاش کنید.')
            }
            showErrorDialog()
            //console.log('err near users :: ', error)
        }
        setLoading(false)
    }

    const signIn = async () => {
        if (code.length === 0 || code.length > 8) {
            return
        }
        setLoading(true)
        try {
            const fcmToken = localStorage.getItem('fcmToken')
            const response = await Axios.post('users/signIn', { phone: fixNumbers(phone), code: fixNumbers(code), isWeb: true, fcmToken: fcmToken })
            //console.log('signin:: ' + response.data)
            if (response.status == 201) {
                localStorage.setItem('account', JSON.stringify(response.data))
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('refreshDrawer', true)
                Axios.defaults.headers = {
                    Authorization: `Bearer ` + response.data.token,
                }
                history.replace('/#5')
            } else {
                if (response.data != null && response.data.message != null) {
                    setSignInProblemText(response.data.message)
                } else {
                    setSignInProblemText('اشکال در برقراری ارتباط لطفا دوباره تلاش کنید.')
                }
                showErrorDialog()
            }
        } catch (error) {
            if (error.response.data != null && error.response.data.message != null) {
                setSignInProblemText(error.response.data.message)
            } else {
                setSignInProblemText('اشکال در برقراری ارتباط لطفا دوباره تلاش کنید.')
            }
            showErrorDialog()
            //console.log('err signin :: ', error)
        }
        setLoading(false)
    }

    const handleClickSendSmsOrCode = () => {
        if (isOnPhoneView) {
            sendSMS()
        } else {
            signIn()
        }
    }

    const [signInProblemText, setSignInProblemText] = useState('اشکال در برقراری ارتباط لطفا دوباره تلاش کنید.')

    const showErrorDialog = () => {
        setOpenErrorDialog(true)
    }

    const closeErrorDialog = () => {
        setOpenErrorDialog(false)
    }

    const showWarningPhoneDialog = () => {
        setOpenPhoneWarnDialog(true)
    }

    const closeWarningPhoneDialog = () => {
        setOpenPhoneWarnDialog(false)
    }

    return (
        <div style={{ overflow: 'hidden', height: '100%' }}>
            <AppBar position="fixed">
                <Toolbar variant="dense">
                    <IconButton edge="start" color="inherit" aria-label="back" onClick={() => history.goBack()}>
                        <MdArrowForward />
                    </IconButton>
                    <Typography variant="h6" color="inherit">
                        ورود | ثبت نام
                    </Typography>
                </Toolbar>
            </AppBar>
            <Dialog fullScreen={false} open={isOpenErrorDialog} onClose={closeErrorDialog} aria-labelledby="responsive-dialog-title">
                <DialogTitle id="responsive-dialog-title">اشکال</DialogTitle>
                <DialogContent>
                    <DialogContentText style={{ fontFamily: 'iransansMedium' }}>{signInProblemText}</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeErrorDialog} style={{ backgroundColor: blueColor, color: 'white' }}>
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog fullScreen={false} open={isOpenPhoneWarnDialog} onClose={closeWarningPhoneDialog} aria-labelledby="responsive-dialog-title">
                <DialogTitle id="responsive-dialog-title">شماره موبایل</DialogTitle>
                <DialogContent>
                    <DialogContentText>لطفا یک شماره ۱۱ رقمی با اعداد لاتین وارد نمایید.</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeWarningPhoneDialog} style={{ backgroundColor: blueColor, color: 'white' }}>
                        بستن
                    </Button>
                </DialogActions>
            </Dialog>
            {isLoading ? (
                <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                    <CircularProgress color="white" style={{ marginTop: 48 }} />
                </Backdrop>
            ) : (
                <div id="loginDiv" style={{ display: 'flex', flexDirection: 'column', width: '100%', height: '100vh', justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                    <img src={Logo} style={{ width: 200, height: 200, marginBottom: 8, borderRadius: 16 }} />
                    <p className="ThirdText" style={{ maxWidth: 200, textAlign: 'center' }}>
                        {isOnPhoneView ? 'لطفا شماره موبایل خود را در قسمت زیر وارد نمایید :' : 'لطفا کد پیامک شده را در قسمت زیر وارد کنید :'}
                    </p>
                    <TextField onChange={handleChange} type="tel" label={isOnPhoneView ? 'شماره موبایل' : 'کد تایید'} style={{ width: window.innerWidth, fontSize: 14, maxWidth: 200, marginTop: 0 }} />
                    <Button onClick={handleClickSendSmsOrCode} variant="contained" fullWidth style={{ maxWidth: 200, marginTop: 12, marginBottom: 48, backgroundColor: blueColor, color: 'white' }}>
                        {isOnPhoneView ? 'ارسال کد' : 'ثبت'}
                    </Button>
                </div>
            )}
        </div>
    )
}

export default LoginScreen
