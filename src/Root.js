import React from 'react'
import { Route, Switch } from 'react-router-dom'
import App from './App'
import { create } from 'jss'
import rtl from 'jss-rtl'
import { StylesProvider, jssPreset } from '@material-ui/core/styles'
import { useWindowDimensions } from './tools/extentions'
import { isIOS } from 'react-device-detect'
import SocketContext from './SocketContext'
import SocietyCategoriesScreen from './components/society/SocietyCategoriesScreen'
import SocietyMessagesScreen from './components/society/SocietyMessagesScreen'
import SocietyTopicsScreen from './components/society/SocietyTopicsScreen'
import MessagesScreen from './components/MessagesScreen'
import CommentsScreen from './components/CommentsScreen'
import LoginScreen from './components/LoginScreen'
import BlockListScreen from './components/BlockListScreen'
import TopicsManagementScreen from './components/TopicsManagementScreen'
import ImageViewerScreen from './components/ImageViewerScreen'
import CoinRoomMessages from './components/CoinRoomMessages'
import UserProfileScreen from './components/UserProfileScreen'
import FollowsScreen from './components/FollowsScreen'
import PostScreen from './components/PostScreen'
import ExploreScreen from './components/ExploreScreen'

const jss = create({ plugins: [...jssPreset().plugins, rtl()] })

function RTL(props) {
    return <StylesProvider jss={jss}>{Root}</StylesProvider>
}

const Root = () => {
    const { height, width } = useWindowDimensions()

    return (
        <SocketContext.Consumer>
            {(value) => {
                return (
                    <StylesProvider jss={jss}>
                        <Switch>
                            <Route component={(props) => <App socket={value} width={width} height={height} {...props} />} exact path="/" />
                            <Route component={(props) => <SocietyCategoriesScreen width={width} height={height} {...props} />} exact path="/society" />
                            <Route component={(props) => <SocietyTopicsScreen width={width} height={height} {...props} />} exact path="/society/topics" />
                            <Route component={(props) => <SocietyMessagesScreen width={width} height={height} {...props} />} exact path="/society/topics/messages" />
                            <Route component={(props) => <MessagesScreen socket={value} width={width} height={height} {...props} />} exact path="/messages" />
                            <Route component={(props) => <CommentsScreen width={width} height={height} {...props} />} exact path="/comments" />
                            <Route component={(props) => <LoginScreen width={width} height={height} {...props} />} exact path="/login" />
                            <Route component={(props) => <BlockListScreen width={width} height={height} socket={value} {...props} />} exact path="/block-list" />
                            <Route component={(props) => <TopicsManagementScreen width={width} height={height} {...props} />} exact path="/society/topics-manager" />
                            <Route component={(props) => <ImageViewerScreen width={width} height={height} {...props} />} exact path="/image-viewer" />
                            <Route component={(props) => <UserProfileScreen width={width} height={height} {...props} />} exact path="/user" />
                            <Route component={(props) => <CoinRoomMessages socket={value} width={width} height={height} {...props} />} exact path="/coin-room" />
                            <Route component={(props) => <FollowsScreen width={width} height={height} {...props} />} exact path="/follows/list" />
                            <Route component={(props) => <PostScreen width={width} height={height} {...props} />} exact path="/post" />
                            <Route component={(props) => <ExploreScreen width={width} height={height} {...props} />} exact path="/explore" />

                        </Switch>
                    </StylesProvider>
                )
            }}
        </SocketContext.Consumer>
    )
}
export default Root