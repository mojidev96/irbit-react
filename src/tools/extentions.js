import moment from 'moment-jalaali'
import { Slider } from '@material-ui/core'
import { withStyles, makeStyles } from '@material-ui/core/styles'
import React, { useState, useEffect } from 'react'
import NumberFormat from 'react-number-format'
import MaskedInput from 'react-text-mask'
import PropTypes from 'prop-types'
import { isIOS, isAndroid, isSafari } from 'react-device-detect'

export const nowDate = new Date()
export function parseDate(dateToParse) {
    var date = ''
    try {
        if (dateToParse == null) {
            date = 'زمان نامشخص'
        } else {
            var date1 = new Date(dateToParse)

            const nowDate = new Date()

            var Difference_In_Time = nowDate.getTime() - date1.getTime()

            var Difference_In_Days = (Difference_In_Time / (1000 * 3600 * 24)).toFixed(0)

            if (Difference_In_Days < 1) {
                // date = moment(dateToParse, 'YYYY-MM-DDThh:mm:ss.Z').format('HH:mm')
                const insec = (Difference_In_Time / 1000).toFixed(0)
                if (insec < 60) {
                    if (insec == 0) {
                        date = 'الان'
                    } else {
                        date = (Difference_In_Time / 1000).toFixed(0) + ' ثانیه پیش'
                    }
                } else if (Difference_In_Time / (1000 * 3600) < 1) {
                    date = (Difference_In_Time / (1000 * 60)).toFixed(0) + ' دقیقه پیش'
                } else {
                    date = (Difference_In_Time / (1000 * 3600)).toFixed(0) + ' ساعت پیش'
                }
            } else if (Difference_In_Days > 30) { //if (Difference_In_Days < 15)
                date = (Difference_In_Days / 30).toFixed(0) + ' ماه پیش'
            } else if (Difference_In_Days > 365) {
                date = (Difference_In_Days / 365).toFixed(0) + ' سال پیش'
            } else {
                date = Difference_In_Days + ' روز پیش'
            }
            // else {
            //     date = 'خیلی روز پیش'
            // }
        }
        return date
    } catch (e) {
        return 'زمان نامشخص'
    }
}

export function parseFulDate(dateToParse) {
    var date = ''
    try {
        if (dateToParse == null) {
            date = 'زمان نامشخص'
        } else {
            date = date = moment(dateToParse, 'YYYY-MM-DDThh:mm:ss.Z').format('jYYYY/jM/jD HH:mm')
        }
        return date
    } catch (e) {
        return 'زمان نامشخص'
    }
}

export function parseJustDate(dateToParse) {
    var date = ''
    try {
        if (dateToParse == null) {
            date = 'زمان نامشخص'
        } else {
            date = date = moment(dateToParse, 'YYYY-MM-DDThh:mm:ss.Z').format('jYYYY/jM/jD')
        }
        return date
    } catch (e) {
        return 'زمان نامشخص'
    }
}

export function parseDistance(distance) {
    var dis = ''
    if (distance == -1 || distance == undefined || distance == null) {
        dis = 'فاصله نامشخص'
    } else if (distance <= 1000) {
        dis = 'در محدوده'
    } else {
        dis = (distance / 1000).toFixed(2) + ' کیلومتر'
    }
    return dis
}

export function parsePrice(price) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + ' ت'
}

export function parseUSDPrice(price) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') + ' $'
}

export function parseNumber(num) {
    return (num ?? '0').toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export function parseLikeOrDislikes(number) {
    var final = ''
    if (number < 1000) {
        final = number
    } else {
        final = number / 1000 + 'K'
    }
    return final
}

Array.prototype.insert = function (index, item) {
    this.splice(index, 0, item)
}

export const SmallSlider = withStyles({
    root: {
        color: '#2d89fa',
        height: 0,
    },
    thumb: {
        height: 0,
        width: 0,
    },
    rail: {
        color: '#fff',
    },
})(Slider)

export const getExtension = (filename) => {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}

export const isImageFile = (filename) => {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'jpg':
        case 'gif':
        case 'bmp':
        case 'png':
            //etc
            return true;
    }
    return false;
}

export const isVideoFile = (filename) => {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'm4v':
        case 'avi':
        case 'mpg':
        case 'mp4':
            // etc
            return true;
    }
    return false;
}

export const usePosition = () => {
    const [position, setPosition] = useState({})
    const [error, setError] = useState(null)

    const onChange = ({ coords }) => {
        setPosition({
            latitude: coords.latitude,
            longitude: coords.longitude,
        })
    }
    const onError = (error) => {
        setError(error.message)
    }
    useEffect(() => {
        const geo = navigator.geolocation
        if (!geo) {
            setError('Geolocation is not supported')
            return
        }
        const watcher = geo.watchPosition(onChange, onError)
        return () => geo.clearWatch(watcher)
    }, [])
    return { ...position, error }
}

const persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g]
const arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g]
export const fixNumbers = (str) => {
    if (typeof str === 'string') {
        for (var i = 0; i < 10; i++) {
            str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i)
        }
    }
    return str
}

export function TextMaskCustom(props) {
    const { inputRef, ...other } = props
    return (
        <MaskedInput
            {...other}
            ref={(ref) => {
                inputRef(ref ? ref.inputElement : null)
            }}
            mask={['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]}
            placeholderChar={'\u2000'}
            showMask
        />
    )
}

TextMaskCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
}

export function PriceFormatCustom(props) {
    const { inputRef, onChange, ...other } = props

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={(values) => {
                onChange({
                    target: {
                        name: props.name,
                        value: () => fixNumbers(values.value),
                    },
                })
            }}
            thousandSeparator
            suffix=" تومان"
        />
    )
}

PriceFormatCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
}

export function NumberFormatCustom(props) {
    const { inputRef, onChange, ...other } = props

    return (
        <NumberFormat
            {...other}
            getInputRef={inputRef}
            onValueChange={(values) => {
                onChange({
                    target: {
                        name: props.name,
                        value: values.value,
                    },
                })
            }}
            thousandSeparator
            isNumericString
        />
    )
}

NumberFormatCustom.propTypes = {
    inputRef: PropTypes.func.isRequired,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
}

export function getColsNumberByWidth(width) {
    return width > 1600 ? 6 : width > 1200 ? 5 : width > 900 ? 4 : width > 700 ? 3 : 2
}

export function useWindowDimensions() {
    const hasWindow = typeof window !== 'undefined'
    function getWindowDimensions() {
        const width = hasWindow ? window.innerWidth : null
        const height = hasWindow ? window.innerHeight : null
        return {
            width,
            height,
        }
    }
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions())
    useEffect(() => {
        if (!isIOS && !isAndroid && !isSafari && hasWindow) {
            function handleResize() {
                setWindowDimensions(getWindowDimensions())
            }

            window.addEventListener('resize', handleResize)
            return () => window.removeEventListener('resize', handleResize)
        }
    }, [hasWindow])
    return windowDimensions
}
