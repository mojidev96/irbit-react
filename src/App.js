import React, { createRef, useEffect, useMemo, useRef, useState } from 'react'
import './App.css'
import { defaults } from 'react-chartjs-2'
import { useHistory } from 'react-router-dom'
import { messaging } from './init-fcm'
import firebase from 'firebase/app'
import { ToastContainer, toast, Slide } from 'react-toastify'
import { FiShare } from 'react-icons/fi'
import { VscDiffAdded } from 'react-icons/vsc'
import { isAndroid, isIOS } from 'react-device-detect'
import SocietyCategoriesScreen from './components/society/SocietyCategoriesScreen'
import { AppBar, Toolbar, Card, Typography, Tab, Tabs, Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions, GridList, Backdrop, IconButton, Button, CircularProgress, Grid, Popover, GridListTile, InputBase, Accordion, AccordionSummary, AccordionDetails } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import logoType from './icons/logotype512.png'
import logo from './icons/logo512.png'

import { AiFillHome } from 'react-icons/ai'
import PropTypes from 'prop-types'
import SwipeableViews from 'react-swipeable-views'

import Axios from 'axios'
import getHistory from 'react-router-global-history'
import { parseUSDPrice, parseJustDate, parseDate, parseNumber } from './tools/extentions'
import UsersScreen from './components/UsersScreen'
import ChatsScreen from './components/ChatsScreen'
import AccountScreen from './components/AccountScreen'
import NewsScreen from './components/NewsScreen'
import MarketsScreen from './components/MarketsScreen'

export const userIdTestol = 1
export const accentcolor = '#FA3A54'
export const backgroundColor = '#333333'
export const itemColor = '#151515'
export const lightIconColor = '#838383'
export const greenColor = '#20AF6C'
export const blueColor = '#1F6DC0'
export const webUrl = 'https://api.irbit.co/'
// export const webUrl = 'http://192.168.1.131:3000/'
// export const webUrl = 'http://192.168.1.115:3000/'
export const websiteUrl = 'https://irbit.co/'
// export const websiteUrl = 'http://192.168.1.131:3000/'
// export const websiteUrl = 'http://192.168.1.115:3000/'
export var position = { latitude: 0.0, longitude: 0.0 }

export var globalTabIndex = 0

export const notifyCopied = () => toast.success('کپی شد!', { autoClose: 2000 })
export const notifySuccessful = () => toast.success('با موفقیت انجام شد', { autoClose: 3500 })
export const notifySuccessfulMessage = (message) => toast.success(message, { autoClose: 3500 })
export const notifyError = () => toast.error('اشکال در ارتباط لطفا دوباره تلاش کنید', { autoClose: 3500 })
export const notifyErrorMessage = (message) => toast.error(message, { autoClose: 3500 })
export const notifyWarnMessage = (message) => toast.warn(message, { autoClose: 4000 })
export const notifyLoginNeeded = () => toast.warning('لطفا وارد حساب کاربری خود شوید', { autoClose: 3500 })
export const notifyMessage = (message, data, click) => {
    if (data.type === 'message' && (parseInt(data.fromId ?? 0) === parseInt(localStorage.getItem('n-c-with')) || parseInt(data.fromId ?? 0) === parseInt(account.id ?? 0))) {
        return
    }
    toast(message, {
        autoClose: 4000,
        onClick: () => {
            if (getHistory().location.pathname === '/') {
                click()
            } else {
                getHistory().push({ pathname: '/', data })
            }
        },
    })
}

export var token = localStorage.getItem('token')
export var account = JSON.parse(localStorage.getItem('account'))
Axios.defaults.baseURL = 'https://api.irbit.co/'
// Axios.defaults.baseURL = 'http://192.168.1.131:3000/'
// Axios.defaults.baseURL = 'http://192.168.1.115:3000/'
Axios.defaults.headers = {
    Authorization: localStorage.getItem('token') == null ? '' : `Bearer ` + localStorage.getItem('token'),
}
defaults.global.defaultFontFamily = 'iransansMedium'


function App(props) {

    let history = useHistory()
    const tabIndexRef = useRef(0)
    const [tabIndexState, setTabIndexState] = useState(0)
    // const [isLoading, setIsLoading] = useState(false)

    useEffect(() => {
        globalTabIndex = localStorage.getItem('tabIndex')
        if (props.location.hash) {
            try {
                const index = Number(props.location.hash.replace('#', ''))
                tabIndexRef.current = index
                setTabIndexState(index)
            } catch (e) { }

        } else {
            const tabIndexLocal = Number(localStorage.getItem('tabIndex')) ?? 0
            tabIndexRef.current = tabIndexLocal
            setTabIndexState(tabIndexLocal)
        }

        position.latitude = localStorage.getItem('latitude') ?? 0.0
        position.longitude = localStorage.getItem('longitude') ?? 0.0

        if (firebase.messaging.isSupported()) {
            messaging
                .requestPermission()
                .then(async function () {
                    const token = await messaging.getToken()
                    localStorage.setItem('fcmToken', token)
                    updateFCMToken(token)
                })
                .catch(function (err) {
                    //console.log('webtoken Unable to get permission to notify.', err)
                })
            navigator.serviceWorker.addEventListener('webtoken mess :: ', (message) => { })

            messaging.onMessage((payload) => {
                //console.log('webtoken Message received. ', payload)
                notifyMessage(payload.notification.title + ' | ' + payload.notification.body, payload.data, () => {
                    if (payload.data.type === 'message') {
                        globalTabIndex = 4
                        setTabIndexState(4)
                        tabIndexRef.current = 4
                        localStorage.setItem('tabIndex', 4)
                        history.replace('/#' + 4)
                        //console.log('wefo32 2222')
                        // } else if (payload.data.type === 'order') {
                        //     if (payload.data.isForCart === 'true') {
                        //         history.push({ pathname: '/cart', data: { requestStatus: payload.data.requestStatus } })
                        //     } else if (payload.data.isForCart === 'false') {
                        //         history.push({ pathname: '/orders', data: { requestStatus: payload.data.requestStatus } })
                        //     }
                        // } else if (payload.data.type === 'store-state') {
                    }
                })
            })
        }

        token = localStorage.getItem('token')
        account = JSON.parse(localStorage.getItem('account'))

        Axios.defaults.headers = {
            Authorization: localStorage.getItem('token') == null ? '' : `Bearer ` + token,
        }

        if (props.socket && props.socket.connected && account && token) {
            props.socket.emit('join', { userId: account.id, token: token })
            props.socket.emit('join-online-users', { userId: account.id })
        }

        props.socket.on('connect', () => {
            // console.log('socket:bb: ' + props.socket.connected)
            if (props.socket.connected) {
                if (account != null) {
                    props.socket.emit('join', { userId: account.id, token: token })
                    props.socket.emit('join-online-users', { userId: account.id })
                }
            } else {
                if (token != null && account != null) {
                    props.socket.emit('online-users', { userId: account.id, token: token, isOnline: false })
                }
            }
        })

        props.socket.on('join', (data) => {
            //console.log('fgwvww:: ' + JSON.stringify(data))
            if (data.needLogin) {
                history.push('/login')
            }
        })

        navigator.geolocation.getCurrentPosition(
            function (position) {
                //console.log('posws:: ' + position)
                position.latitude = position.coords.latitude
                position.longitude = position.coords.longitude
                localStorage.setItem('latitude', position.coords.latitude)
                localStorage.setItem('longitude', position.coords.longitude)
            },
            function (error) {
                console.error('Error Code = ' + error.code + ' - ' + error.message)
            }
        )

    }, [])
    // handle add to home screen dialog screen
    const [isShowingAddToHomeDialog, setShowingAddToHomeDialog] = useState(false)
    const showAddToHomeScreen = () => {
        return (
            <div style={{ backgroundColor: backgroundColor, zIndex: 999, width: window.innerWidth, height: window.innerHeight, justifyContent: 'center', paddingTop: 80, paddingBottom: 100 }}>
                <img src={logo} style={{ width: 100, height: 100, objectFit: 'contain' }} />
                <div style={{ marginRight: 32, marginLeft: 32 }}>
                    <p className="SecondaryText" style={{ textAlign: 'center', marginTop: 8 }}>
                        وب اپ irBit را به صفحه اصلی گوشی خود اضافه کنید تا دسترسی آسان تر شود.
                    </p>
                </div>
                <div style={{ display: 'flex', alignItems: 'center', marginTop: 32 }}>
                    <FiShare size={24} style={{ color: '#239edb', marginRight: 32 }} />
                    <p style={{ marginTop: 4, marginRight: 12 }}>
                        <span style={{ fontFamily: 'iransansUltraLight', fontSize: 14 }}>۱- در نوار پایین برروی گزینه </span>
                        <span style={{ fontFamily: 'iransansBold' }}>Share </span>
                        <span style={{ fontFamily: 'iransansUltraLight', fontSize: 14 }}>کلیک نمایید.</span>
                    </p>
                </div>
                <div style={{ display: 'flex', alignItems: 'center', marginTop: 8 }}>
                    <VscDiffAdded size={32} style={{ color: '#333333', marginRight: 32 }} />
                    <p className="SecondaryText" style={{ marginTop: 4, marginRight: 12 }}>
                        <span style={{ fontFamily: 'iransansUltraLight', fontSize: 14 }}>۲- در منوی باز شده در قسمت پایین، گزینه </span>
                        <span style={{ fontFamily: 'iransansBold' }}>Add to Home Screen </span>
                        <span style={{ fontFamily: 'iransansUltraLight', fontSize: 14 }}>را انتخاب کنید.</span>
                    </p>
                </div>
                <div style={{ display: 'flex', alignItems: 'center', marginTop: 8 }}>
                    <p className="SecondaryText" style={{ color: '#239edb', marginTop: 4, marginRight: 32 }}>
                        Add
                    </p>
                    <p className="SecondaryText" style={{ marginTop: 4, marginRight: 12 }}>
                        <span style={{ fontFamily: 'iransansUltraLight', fontSize: 14 }}>۳- در مرحله بعد در قسمت بالا روی </span>
                        <span style={{ fontFamily: 'iransansBold' }}>Add </span>
                        <span style={{ fontFamily: 'iransansUltraLight', fontSize: 14 }}>بزنید.</span>
                    </p>
                </div>
                <Button
                    variant="contained"
                    fullWidth
                    onClick={() => {
                        localStorage.setItem('isIosSetupHomeScreen', true)
                        setShowingAddToHomeDialog(false)
                    }}
                    style={{ color: 'white', backgroundColor: greenColor, borderRadius: 0, height: 40, position: 'absolute', bottom: 0, left: 0, right: 0 }}
                >
                    متوجه شدم
                </Button>
            </div>
        )
    }

    const updateFCMToken = async (fcmToken) => {
        if (localStorage.getItem('token') != null) {
            try {
                const response = await Axios.post('users/updateWebFCMToken', { fcm: fcmToken })
                //console.log('gvb34gv:: ' + JSON.stringify(response))
            } catch (error) {
                //console.log('gvb34gv update webtoken error :: ', error)
            }
        }
    }

    const handleChange = (event, newValue) => {
        if (newValue < 0) {
            return
        }
        globalTabIndex = newValue
        setTabIndexState(newValue)
        tabIndexRef.current = newValue
        localStorage.setItem('tabIndex', newValue)
        history.replace('/#' + newValue)
    }

    const handleChangeIndex = (index) => {
        if (index > -1 && (isAndroid || isIOS)) {
            globalTabIndex = index
            localStorage.setItem('tabIndex', index)
            setTabIndexState(index)
            tabIndexRef.current = index
            history.replace('/#' + index)
        }
    }

    return isShowingAddToHomeDialog ? (
        <div className="App">{showAddToHomeScreen()}</div>
    ) : (
        <div className="App" style={{ backgroundColor: backgroundColor }}>
            <AppBar position="fixed">
                <Toolbar id="tabs" variant="dense">
                    <Tabs variant="scrollable" rtl style={{ overflowY: 'hidden' }} value={tabIndexState} onChange={handleChange} indicatorColor="secondary" textColor="secondary">

                        <Tab wrapper label={'بازار'} {...a11yProps(0)} />
                        <Tab wrapper label={'اخبار'} {...a11yProps(1)} />
                        <Tab wrapper label={'انجمن'} {...a11yProps(2)} />
                        <Tab wrapper label={'کاربران'} {...a11yProps(3)} />
                        <Tab wrapper label={'گفتگوها'} {...a11yProps(4)} />
                        <Tab wrapper label={'حساب کاربری'} {...a11yProps(5)} />

                    </Tabs>
                </Toolbar>
            </AppBar>
            {/* {isLoading && (
                <div style={{ width: window.innerWidth, height: window.innerHeight - 48 }}>
                    <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                        <CircularProgress color="white" style={{ marginTop: 48 }} />
                    </Backdrop>
                </div>
            )} */}

            <SwipeableViews disabled={false} axis={'x-reverse'} index={tabIndexState} onChangeIndex={handleChangeIndex}>
                {tabIndexRef.current == 0 ? <MarketsScreen socket={props.socket} value={tabIndexRef.current} history={history} index={0} dir={'rtl'} /> : <div />}
                {tabIndexRef.current == 1 ? <NewsScreen value={tabIndexRef.current} history={history} index={1} dir={'rtl'} /> : <div />}
                {tabIndexRef.current == 2 ? <SocietyCategoriesScreen value={tabIndexRef.current} history={history} index={2} dir={'rtl'} /> : <div />}
                {tabIndexRef.current == 3 ? <UsersScreen value={tabIndexRef.current} history={history} index={3} dir={'rtl'} /> : <div />}
                {tabIndexRef.current == 4 ? <ChatsScreen value={tabIndexRef.current} socket={props.socket} history={history} index={4} dir={'rtl'} /> : <div />}
                {tabIndexRef.current == 5 ? <AccountScreen value={tabIndexRef.current} socket={props.socket} history={history} index={5} dir={'rtl'} /> : <div />}
            </SwipeableViews>
            <ToastContainer rtl closeOnClick pauseOnHover position="top-center" transition={Slide} />
        </div>
    )
}

function a11yProps(index) {
    return {
        id: `full-width-tab-${index}`,
        'aria-controls': `full-width-tabpanel-${index}`,
    }
}

export default App
