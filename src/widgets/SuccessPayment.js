import React, { useEffect, useState, useRef } from 'react'
import { useHistory } from 'react-router-dom'
import { MdCheckCircle, MdPriorityHigh } from 'react-icons/md'
import { greenColor, blueColor, accentcolor } from '../../App'
import { Button, Backdrop, CircularProgress } from '@material-ui/core'

function SuccessPayment(props) {
    let history = useHistory()

    const [isLoading, setLoading] = useState(true)
    const isSended = useRef(false)
    const isSuccess = useRef(false)
    const [warnText, setWarnText] = useState('')
    const selectedId = useRef(0)

    useEffect(() => {
        selectedId.current = parseInt(localStorage.getItem('selectedAddress')) + 0
        const token = localStorage.getItem('token')
        // console.log('socket:0: ' + props.socket)
        props.socket.on('connect', () => {
            // console.log('socket:a: ' + props.socket.connected)
            if (props.socket.connected) {
                if (!isSended.current) {
                    // console.log('socket:b: ' + selectedId.current + ' --- ' + token)
                    props.socket.emit('sendToBuy', { token: token, addressId: selectedId.current })
                    isSended.current = true
                    setTimeout(() => {
                        if (!isSuccess.current) {
                            setWarnText('متاسفانه پس از پرداخت، عملیات خرید کامل نشد. اما حساب شما با موفقیت شارژ شده است. برای ثبت نهایی سفارش کافیست دوباره در صفحه سبد خرید خود بر روی گزینه "خرید" کلیک نمایید.')
                            setLoading(false)
                        }
                    }, 30000)
                }
            }
        })

        props.socket.on('sendToBuy', (data) => {
            // console.log('socket:c: ' + JSON.stringify(data))
            if (data.status != 200) {
                setWarnText('متاسفانه پس از پرداخت، عملیات خرید کامل نشد. اما حساب شما با موفقیت شارژ شده است. برای ثبت نهایی سفارش کافیست دوباره در صفحه سبد خرید خود بر روی گزینه "خرید" کلیک نمایید.')
            } else {
                isSuccess.current = true
            }
            setLoading(false)
        })
    }, [])

    return (
        <div>
            {isLoading ? (
                <Backdrop style={{ zIndex: 5, color: '#fff' }} open={true}>
                    <CircularProgress color="white" style={{ marginTop: 48 }} />
                </Backdrop>
            ) : (
                <div
                    style={{
                        width: window.innerWidth,
                        height: window.innerHeight,
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    {warnText.length > 0 ? <MdPriorityHigh size={100} color={accentcolor} /> : <MdCheckCircle size={100} color={greenColor} />}
                    <p className="PrimaryText" style={{ color: greenColor, marginTop: 16 }}>
                        {warnText.length > 0 ? 'عملیات ناموفق' : 'پرداخت موفق'}
                    </p>
                    <p className="ThirdText" style={{ marginBottom: 8, marginTop: 4, maxWidth: 300, textAlign: 'center' }}>
                        {warnText.length === 0 ? 'با تشکر از خرید شما' : warnText}
                    </p>
                    <Button onClick={() => history.replace({ pathname: '/cart', data: { requestStatus: 1 } })} variant="contained" style={{ backgroundColor: blueColor, color: 'white' }}>
                        بازگشت به سبد
                    </Button>
                </div>
            )}
        </div>
    )
}

export default SuccessPayment
