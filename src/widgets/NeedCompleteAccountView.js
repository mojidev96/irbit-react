import { Button } from '@material-ui/core'
import React, { Component } from 'react'
import { useHistory } from 'react-router-dom'
import smile from './../icons/smile.png'
function NeedCompleteAccountView(props) {
    let history = useHistory()
    const pushToAccountScreen = () => {
        history.push('/account')
    }
    return (
        <div
            style={{
                width: '100%',
                maxWidth:500,
                height: window.innerHeight - (props.data == null ? 0 : props.data.minusHeight),
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <img src={smile} style={{ width: 150, height: 150, marginBottom: 16 }} />
            <p className="PrimaryText">تکمیل ثبت نام.</p>
            <p className="ThirdText" style={{ maxWidth: 250, textAlign:'center', marginTop: 8 }}>
                کاربر گرامی؛ برای دسترسی به کلیه امکانات برنامه لطفا اطلاعات حساب کاربری خود را کامل نمایید.
            </p>
            <Button variant="contained" onClick={pushToAccountScreen} style={{ width: 200, marginBottom: 56 + props.marginBottom ?? 0, marginTop: 16 }}>تکمیل ثبت نام</Button>
        </div>
    )
}

export default NeedCompleteAccountView
