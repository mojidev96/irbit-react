import { Button } from '@material-ui/core'
import React, { Component } from 'react'
import { useHistory } from 'react-router-dom'
import tryAgainImage from './../icons/tryagain.png'
function TryAgainWidget(props) {
    let history = useHistory()

    return (
        <div
            style={{
                width: '100%',
                height: window.innerHeight - (props.data == null ? 0 : props.data.minusHeight),
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <img src={tryAgainImage} style={{ width: 100, height: 100, marginBottom: 0 }} />
            <p className="PrimaryText">اشکال در ارتباط</p>
            <p className="ThirdText" style={{ maxWidth: 250, textAlign: 'center', marginTop: 4 }}>
                لطفا ضمن بررسی اتصال اینترنت، دوباره تلاش کنید.
            </p>
            <Button variant="contained" onClick={props.onTryAgain} style={{ width: 200, marginBottom: 56 + props.marginBottom ?? 0, marginTop: 16 }}>
                تلاش دوباره
            </Button>
        </div>
    )
}

export default TryAgainWidget
