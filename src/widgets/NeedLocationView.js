import React, { Component } from 'react'
import empty from './../../icons/empty512.png'
import { Button } from '@material-ui/core'
import {position, notifyErrorMessage } from '../../App'
function NeedLocationView(props) {
    return (
        <div
            style={{
                width: '100%',
                maxWidth: 500,
                height: window.innerHeight,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <p className="PrimaryText" style={{ marginBottom: 8, maxWidth: 200 }}>
                عدم دسترسی به موقعیت.
            </p>
            <p className="ThirdText" style={{ marginBottom: 24, maxWidth: 200, textAlign: 'justify' }}>
                دسترسی به موقعیت دستگاه فراهم نیست. جهت صدور این دسترسی برروی گزینه زیر کلیک نمایید.
            </p>
            <Button
                onClick={() => {
                    navigator.geolocation.getCurrentPosition(
                        function (position) {
                            //console.log('posws:: ' + position)
                            position.latitude = position.coords.latitude
                            position.longitude = position.coords.longitude
                            localStorage.setItem('latitude', position.coords.latitude)
                            localStorage.setItem('longitude', position.coords.longitude)
                            // notifySuccessfulMessage('posws:: ' + position)
                            props.callback()
                        },
                        function (error) {
                            notifyErrorMessage('سرویس دهنده موقعیت جغرافیایی را ارائه نمی کند.')
                            console.error('Error Code = ' + error.code + ' - ' + error.message)
                        }

                    )
                }}
                variant="contained"
                style={{ width: 200, marginBottom: 56 + props.marginBottom ?? 0 }}
            >
                صدور دسترسی
            </Button>
        </div>
    )
}

export default NeedLocationView
