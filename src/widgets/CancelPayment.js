import React from 'react'
import { useHistory } from 'react-router-dom'
import { MdPriorityHigh } from 'react-icons/md'
import { accentcolor, blueColor } from '../../App'
import { Button } from '@material-ui/core'
function CancelPayment() {
    let history = useHistory()
    return (
        <div
            style={{
                width: window.innerWidth,
                height: window.innerHeight,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <MdPriorityHigh size={100} color={accentcolor} />
            <p className="PrimaryText" style={{ color: accentcolor, marginTop: 16 }}>
                انصراف از پرداخت
            </p>
            <p className="ThirdText" style={{ marginBottom: 8, marginTop: 4 }}>
                لطفا دوباره تلاش کنید.
            </p>
            <Button onClick={() => history.replace('/cart')} variant="contained" style={{ backgroundColor: blueColor, color: 'white' }}>
                بازگشت
            </Button>
        </div>
    )
}

export default CancelPayment
