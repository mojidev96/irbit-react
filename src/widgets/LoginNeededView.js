import { Button } from '@material-ui/core'
import React, { Component } from 'react'
import { useHistory } from 'react-router-dom'
import smile from './../icons/smile.png'
function LoginNeededView(props) {
    let history = useHistory()
    const pushToLoginScreen = () => {
        history.push('/login')
    }
    return (
        <div
            style={{
                width: '100%',
                maxWidth:500,
                height: window.innerHeight - (props.data == null ? 0 : props.data.minusHeight),
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <img src={smile} style={{ width: 150, height: 150, marginBottom: 16 }} />
            <p className="PrimaryText">لطفا وارد حساب کاربری خود شوید.</p>
            <p className="ThirdText" style={{ maxWidth: 250, textAlign:'center', marginTop: 8 }}>
                کاربر گرامی؛ برای دسترسی به کلیه امکانات برنامه لطفا ابتدا وارد حساب کاربری خود شوید.
            </p>
            <Button variant="contained" onClick={pushToLoginScreen} style={{ width: 200, marginBottom: 56 + props.marginBottom ?? 0, marginTop: 16 }}>ورود | ثبت نام</Button>
        </div>
    )
}

export default LoginNeededView
