import React, { Component } from 'react'

export class BoldText extends Component {
    render() {
        return <p className={this.props.styleClass}>{this.props.text}</p>
    }
}

export default BoldText
