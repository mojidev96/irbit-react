import React, { Component } from 'react'

export class PriceText extends Component {
    render() {
        var CurrencyFormat = require('react-currency-format')
        return <CurrencyFormat class={this.props.styleClassName} value={this.props.price} displayType={'text'} thousandSeparator={true} suffix={' ت'} />
    }
}

export default PriceText
