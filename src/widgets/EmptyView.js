import React, { Component } from 'react'
import empty from './../icons/empty512.png'
function EmptyView(props) {
    return (
        <div
            style={{
                width: '100%',
                height: window.innerHeight,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <img src={empty} style={{ width: 120, height: 120 }} />
            <p className="PrimaryLightText" style={{ color: '#606060', marginBottom: 48 + (props.marginBottom ?? 0) }}>
                خالیست.
            </p>
        </div>
    )
}

export default EmptyView
