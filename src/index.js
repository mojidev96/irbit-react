import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import * as serviceWorker from './serviceWorker'
import { BrowserRouter } from 'react-router-dom'
import Root from './Root'
import SocketContext from './SocketContext'
import socketIOClient from 'socket.io-client'
import { ReactRouterGlobalHistory } from 'react-router-global-history'
import { ThemeProvider } from '@material-ui/core/styles'
import { createMuiTheme } from '@material-ui/core/styles'
import HttpsRedirect from 'react-https-redirect'

// export const ENDPOINT = 'http://192.168.1.115:3000'
// export const ENDPOINT = 'http://192.168.1.131:3000'
export const ENDPOINT = 'wss://irbit.co'

const socket = new socketIOClient(ENDPOINT, { transports: ['websocket'], forceNew: true })

const theme = createMuiTheme({
    direction: 'rtl',
    palette: {
        type: 'dark',
        primary: {
            main: '#363636',
        },
        secondary: {
            main: '#FF324D',
        },
        text: {
            primary: '#fff',
        },
    },
})

localStorage.setItem('tabIndex', localStorage.getItem('tabIndex') ?? 0)

localStorage.setItem('usersScroll', 0)
localStorage.setItem('usersPage', 1)
localStorage.setItem('usersData', '[]')
localStorage.setItem('usersWord', '')

localStorage.setItem('chatsScroll', 0)
localStorage.setItem('chatsPage', 1)
localStorage.setItem('chatsData', '[]')

localStorage.setItem('societyScroll', 0)
localStorage.setItem('societyData', '[]')

localStorage.setItem('marketsData', '[]')
localStorage.setItem('marketsPage', 1)
localStorage.setItem('marketsScroll', 0)
localStorage.setItem('marketsWord', '')

localStorage.setItem('newsScroll', 0)
localStorage.setItem('newsData', '[]')
localStorage.setItem('newsPage', 1)
localStorage.setItem('newsWord', '')

localStorage.setItem('exploreScroll', 0)
localStorage.setItem('explorePage', 1)
localStorage.setItem('exploreData', '[]')
//render
ReactDOM.render(
    // <HttpsRedirect>
    <ThemeProvider theme={theme}>
        <SocketContext.Provider value={socket}>
            <BrowserRouter>
                <ReactRouterGlobalHistory />
                <Root />
            </BrowserRouter>
        </SocketContext.Provider>
    </ThemeProvider>
    // </HttpsRedirect>
    ,
    document.getElementById('root')
)

if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register(process.env.PUBLIC_URL + '/firebase-messaging-sw.js')
        .then(function (registration) {
            // console.log('webtoken Registration successful, scope is:', registration.scope)
        })
        .catch(function (err) {
            // console.log('webtoken Service worker registration failed, error:', err)
        })
}

document.body.setAttribute('dir', 'rtl')
document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl')
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register()
