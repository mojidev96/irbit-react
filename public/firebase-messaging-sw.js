importScripts("https://www.gstatic.com/firebasejs/7.20.0/firebase-app.js")
importScripts("https://www.gstatic.com/firebasejs/7.20.0/firebase-messaging.js")
importScripts("https://www.gstatic.com/firebasejs/7.20.0/firebase-analytics.js")


firebase.initializeApp({
    // Project Settings => Add Firebase to your web app
    apiKey: "AIzaSyDIFGj8T-sshZHM_7jUskFv2pWOEK_YnWw",
    authDomain: "irbit-f7b38.firebaseapp.com",
    projectId: "irbit-f7b38",
    storageBucket: "irbit-f7b38.appspot.com",
    messagingSenderId: "425118420550",
    appId: "1:425118420550:web:3c6585b37ca6c11fbb4447",
    measurementId: "G-Z4YF80CRFW"

    // messagingSenderId: '962676166711',
    // apiKey: 'AIzaSyBOcUYRq_AlP8gwc5a_hJDQYu_MTBzG3kc',
    // authDomain: 'forusha.firebaseapp.com',
    // databaseURL: 'https://forusha.firebaseio.com',
    // projectId: 'forusha',
    // storageBucket: 'forusha.appspot.com',
    // appId: '1:962676166711:web:b670d2a2f4293dd8326fb2',
    // measurementId: 'G-CFFVN8BE52',
})
const messaging = firebase.messaging()
messaging.setBackgroundMessageHandler(function (payload) {
    const promiseChain = clients
        .matchAll({
            type: 'window',
            includeUncontrolled: true,
        })
        .then((windowClients) => {
            for (let i = 0; i < windowClients.length; i++) {
                const windowClient = windowClients[i]
                windowClient.postMessage(payload)
            }
        })
        .then(() => {
            return registration.showNotification('پیام جدید دارید.')
        })
    return promiseChain
})

self.addEventListener('notificationclick', function (event) {
    // do what you want
    // ...
})
