# Details

Date : 2020-08-26 00:43:47

Directory /home/mojtaba/developement/react/forusha/src

Total : 31 files,  3353 codes, 159 comments, 288 blanks, all 3800 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.css](/src/App.css) | CSS | 321 | 3 | 55 | 379 |
| [src/App.js](/src/App.js) | JavaScript | 396 | 15 | 21 | 432 |
| [src/App.test.js](/src/App.test.js) | JavaScript | 8 | 0 | 2 | 10 |
| [src/Root.js](/src/Root.js) | JavaScript | 30 | 0 | 4 | 34 |
| [src/components/ChatsScreen.js](/src/components/ChatsScreen.js) | JavaScript | 102 | 0 | 8 | 110 |
| [src/components/HomeScreen.js](/src/components/HomeScreen.js) | JavaScript | 377 | 7 | 12 | 396 |
| [src/components/PeopleScreen.js](/src/components/PeopleScreen.js) | JavaScript | 300 | 2 | 36 | 338 |
| [src/components/ProductsScreen.js](/src/components/ProductsScreen.js) | JavaScript | 84 | 0 | 9 | 93 |
| [src/components/StoresScreen.js](/src/components/StoresScreen.js) | JavaScript | 120 | 0 | 9 | 129 |
| [src/components/charts/LineChart.js](/src/components/charts/LineChart.js) | JavaScript | 0 | 59 | 4 | 63 |
| [src/components/home-items/HomeHeaderSection.js](/src/components/home-items/HomeHeaderSection.js) | JavaScript | 47 | 0 | 3 | 50 |
| [src/components/home-items/MostLikeProductItem.js](/src/components/home-items/MostLikeProductItem.js) | JavaScript | 51 | 0 | 2 | 53 |
| [src/components/home-items/MostVisitedStoreItem.js](/src/components/home-items/MostVisitedStoreItem.js) | JavaScript | 53 | 0 | 3 | 56 |
| [src/components/home-items/TagItem.js](/src/components/home-items/TagItem.js) | JavaScript | 8 | 0 | 3 | 11 |
| [src/components/home-items/UpperDiscountItem.js](/src/components/home-items/UpperDiscountItem.js) | JavaScript | 54 | 0 | 3 | 57 |
| [src/components/home-items/UserItem.js](/src/components/home-items/UserItem.js) | JavaScript | 59 | 0 | 3 | 62 |
| [src/components/screens/AccountScreen.js](/src/components/screens/AccountScreen.js) | JavaScript | 173 | 0 | 9 | 182 |
| [src/components/screens/LoginScreen.js](/src/components/screens/LoginScreen.js) | JavaScript | 141 | 0 | 13 | 154 |
| [src/components/screens/ProductDetailScreen.js](/src/components/screens/ProductDetailScreen.js) | JavaScript | 293 | 4 | 14 | 311 |
| [src/components/screens/StoreDetailScreen.js](/src/components/screens/StoreDetailScreen.js) | JavaScript | 290 | 3 | 18 | 311 |
| [src/components/screens/VotesScreen.js](/src/components/screens/VotesScreen.js) | JavaScript | 242 | 1 | 20 | 263 |
| [src/index.css](/src/index.css) | CSS | 12 | 0 | 2 | 14 |
| [src/index.js](/src/index.js) | JavaScript | 18 | 6 | 3 | 27 |
| [src/logo.svg](/src/logo.svg) | XML | 7 | 0 | 1 | 8 |
| [src/serviceWorker.js](/src/serviceWorker.js) | JavaScript | 98 | 31 | 13 | 142 |
| [src/setupTests.js](/src/setupTests.js) | JavaScript | 1 | 4 | 1 | 6 |
| [src/tools/MyHelper.js](/src/tools/MyHelper.js) | JavaScript | 0 | 24 | 3 | 27 |
| [src/tools/extentions.js](/src/tools/extentions.js) | JavaScript | 33 | 0 | 6 | 39 |
| [src/widgets/BoldText.js](/src/widgets/BoldText.js) | JavaScript | 7 | 0 | 3 | 10 |
| [src/widgets/EmptyView.js](/src/widgets/EmptyView.js) | JavaScript | 20 | 0 | 2 | 22 |
| [src/widgets/PriceText.js](/src/widgets/PriceText.js) | JavaScript | 8 | 0 | 3 | 11 |

[summary](results.md)