Date : 2020-08-26 00:43:47
Directory : /home/mojtaba/developement/react/forusha/src
Total : 31 files,  3353 codes, 159 comments, 288 blanks, all 3800 lines

Languages
+------------+------------+------------+------------+------------+------------+
| language   | files      | code       | comment    | blank      | total      |
+------------+------------+------------+------------+------------+------------+
| JavaScript |         28 |      3,013 |        156 |        230 |      3,399 |
| CSS        |          2 |        333 |          3 |         57 |        393 |
| XML        |          1 |          7 |          0 |          1 |          8 |
+------------+------------+------------+------------+------------+------------+

Directories
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                       | files      | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                          |         31 |      3,353 |        159 |        288 |      3,800 |
| components                                                                                 |         17 |      2,394 |         76 |        169 |      2,639 |
| components/charts                                                                          |          1 |          0 |         59 |          4 |         63 |
| components/home-items                                                                      |          6 |        272 |          0 |         17 |        289 |
| components/screens                                                                         |          5 |      1,139 |          8 |         74 |      1,221 |
| tools                                                                                      |          2 |         33 |         24 |          9 |         66 |
| widgets                                                                                    |          3 |         35 |          0 |          8 |         43 |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| filename                                                                                   | language   | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| /home/mojtaba/developement/react/forusha/src/App.css                                       | CSS        |        321 |          3 |         55 |        379 |
| /home/mojtaba/developement/react/forusha/src/App.js                                        | JavaScript |        396 |         15 |         21 |        432 |
| /home/mojtaba/developement/react/forusha/src/App.test.js                                   | JavaScript |          8 |          0 |          2 |         10 |
| /home/mojtaba/developement/react/forusha/src/Root.js                                       | JavaScript |         30 |          0 |          4 |         34 |
| /home/mojtaba/developement/react/forusha/src/components/ChatsScreen.js                     | JavaScript |        102 |          0 |          8 |        110 |
| /home/mojtaba/developement/react/forusha/src/components/HomeScreen.js                      | JavaScript |        377 |          7 |         12 |        396 |
| /home/mojtaba/developement/react/forusha/src/components/PeopleScreen.js                    | JavaScript |        300 |          2 |         36 |        338 |
| /home/mojtaba/developement/react/forusha/src/components/ProductsScreen.js                  | JavaScript |         84 |          0 |          9 |         93 |
| /home/mojtaba/developement/react/forusha/src/components/StoresScreen.js                    | JavaScript |        120 |          0 |          9 |        129 |
| /home/mojtaba/developement/react/forusha/src/components/charts/LineChart.js                | JavaScript |          0 |         59 |          4 |         63 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/HomeHeaderSection.js    | JavaScript |         47 |          0 |          3 |         50 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/MostLikeProductItem.js  | JavaScript |         51 |          0 |          2 |         53 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/MostVisitedStoreItem.js | JavaScript |         53 |          0 |          3 |         56 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/TagItem.js              | JavaScript |          8 |          0 |          3 |         11 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/UpperDiscountItem.js    | JavaScript |         54 |          0 |          3 |         57 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/UserItem.js             | JavaScript |         59 |          0 |          3 |         62 |
| /home/mojtaba/developement/react/forusha/src/components/screens/AccountScreen.js           | JavaScript |        173 |          0 |          9 |        182 |
| /home/mojtaba/developement/react/forusha/src/components/screens/LoginScreen.js             | JavaScript |        141 |          0 |         13 |        154 |
| /home/mojtaba/developement/react/forusha/src/components/screens/ProductDetailScreen.js     | JavaScript |        293 |          4 |         14 |        311 |
| /home/mojtaba/developement/react/forusha/src/components/screens/StoreDetailScreen.js       | JavaScript |        290 |          3 |         18 |        311 |
| /home/mojtaba/developement/react/forusha/src/components/screens/VotesScreen.js             | JavaScript |        242 |          1 |         20 |        263 |
| /home/mojtaba/developement/react/forusha/src/index.css                                     | CSS        |         12 |          0 |          2 |         14 |
| /home/mojtaba/developement/react/forusha/src/index.js                                      | JavaScript |         18 |          6 |          3 |         27 |
| /home/mojtaba/developement/react/forusha/src/logo.svg                                      | XML        |          7 |          0 |          1 |          8 |
| /home/mojtaba/developement/react/forusha/src/serviceWorker.js                              | JavaScript |         98 |         31 |         13 |        142 |
| /home/mojtaba/developement/react/forusha/src/setupTests.js                                 | JavaScript |          1 |          4 |          1 |          6 |
| /home/mojtaba/developement/react/forusha/src/tools/MyHelper.js                             | JavaScript |          0 |         24 |          3 |         27 |
| /home/mojtaba/developement/react/forusha/src/tools/extentions.js                           | JavaScript |         33 |          0 |          6 |         39 |
| /home/mojtaba/developement/react/forusha/src/widgets/BoldText.js                           | JavaScript |          7 |          0 |          3 |         10 |
| /home/mojtaba/developement/react/forusha/src/widgets/EmptyView.js                          | JavaScript |         20 |          0 |          2 |         22 |
| /home/mojtaba/developement/react/forusha/src/widgets/PriceText.js                          | JavaScript |          8 |          0 |          3 |         11 |
| Total                                                                                      |            |      3,353 |        159 |        288 |      3,800 |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+