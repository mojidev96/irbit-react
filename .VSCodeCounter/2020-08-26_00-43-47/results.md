# Summary

Date : 2020-08-26 00:43:47

Directory /home/mojtaba/developement/react/forusha/src

Total : 31 files,  3353 codes, 159 comments, 288 blanks, all 3800 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 28 | 3,013 | 156 | 230 | 3,399 |
| CSS | 2 | 333 | 3 | 57 | 393 |
| XML | 1 | 7 | 0 | 1 | 8 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 31 | 3,353 | 159 | 288 | 3,800 |
| components | 17 | 2,394 | 76 | 169 | 2,639 |
| components/charts | 1 | 0 | 59 | 4 | 63 |
| components/home-items | 6 | 272 | 0 | 17 | 289 |
| components/screens | 5 | 1,139 | 8 | 74 | 1,221 |
| tools | 2 | 33 | 24 | 9 | 66 |
| widgets | 3 | 35 | 0 | 8 | 43 |

[details](details.md)