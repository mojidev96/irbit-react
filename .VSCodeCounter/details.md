# Details

Date : 2020-08-22 01:33:05

Directory /home/mojtaba/developement/react/forusha/src

Total : 24 files,  2080 codes, 143 comments, 188 blanks, all 2411 lines

[summary](results.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/App.css](/src/App.css) | CSS | 224 | 1 | 39 | 264 |
| [src/App.js](/src/App.js) | JavaScript | 379 | 18 | 20 | 417 |
| [src/App.test.js](/src/App.test.js) | JavaScript | 8 | 0 | 2 | 10 |
| [src/components/ChatsScreen.js](/src/components/ChatsScreen.js) | JavaScript | 102 | 0 | 8 | 110 |
| [src/components/HomeScreen.js](/src/components/HomeScreen.js) | JavaScript | 357 | 7 | 8 | 372 |
| [src/components/PeopleScreen.js](/src/components/PeopleScreen.js) | JavaScript | 300 | 2 | 36 | 338 |
| [src/components/ProductsScreen.js](/src/components/ProductsScreen.js) | JavaScript | 84 | 0 | 9 | 93 |
| [src/components/StoresScreen.js](/src/components/StoresScreen.js) | JavaScript | 120 | 0 | 9 | 129 |
| [src/components/charts/LineChart.js](/src/components/charts/LineChart.js) | JavaScript | 0 | 59 | 4 | 63 |
| [src/components/home-items/HomeHeaderSection.js](/src/components/home-items/HomeHeaderSection.js) | JavaScript | 47 | 0 | 3 | 50 |
| [src/components/home-items/MostLikeProductItem.js](/src/components/home-items/MostLikeProductItem.js) | JavaScript | 51 | 0 | 2 | 53 |
| [src/components/home-items/MostVisitedStoreItem.js](/src/components/home-items/MostVisitedStoreItem.js) | JavaScript | 53 | 0 | 3 | 56 |
| [src/components/home-items/TagItem.js](/src/components/home-items/TagItem.js) | JavaScript | 8 | 0 | 3 | 11 |
| [src/components/home-items/UpperDiscountItem.js](/src/components/home-items/UpperDiscountItem.js) | JavaScript | 54 | 0 | 3 | 57 |
| [src/components/home-items/UserItem.js](/src/components/home-items/UserItem.js) | JavaScript | 61 | 18 | 4 | 83 |
| [src/components/screens/AccountScreen.js](/src/components/screens/AccountScreen.js) | JavaScript | 46 | 0 | 3 | 49 |
| [src/index.css](/src/index.css) | CSS | 12 | 0 | 2 | 14 |
| [src/index.js](/src/index.js) | JavaScript | 20 | 3 | 3 | 26 |
| [src/logo.svg](/src/logo.svg) | XML | 7 | 0 | 1 | 8 |
| [src/serviceWorker.js](/src/serviceWorker.js) | JavaScript | 98 | 31 | 13 | 142 |
| [src/setupTests.js](/src/setupTests.js) | JavaScript | 1 | 4 | 1 | 6 |
| [src/tools/extentions.js](/src/tools/extentions.js) | JavaScript | 33 | 0 | 6 | 39 |
| [src/widgets/BoldText.js](/src/widgets/BoldText.js) | JavaScript | 7 | 0 | 3 | 10 |
| [src/widgets/PriceText.js](/src/widgets/PriceText.js) | JavaScript | 8 | 0 | 3 | 11 |

[summary](results.md)