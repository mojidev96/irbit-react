# Summary

Date : 2020-08-22 01:33:05

Directory /home/mojtaba/developement/react/forusha/src

Total : 24 files,  2080 codes, 143 comments, 188 blanks, all 2411 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JavaScript | 21 | 1,837 | 142 | 146 | 2,125 |
| CSS | 2 | 236 | 1 | 41 | 278 |
| XML | 1 | 7 | 0 | 1 | 8 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 24 | 2,080 | 143 | 188 | 2,411 |
| components | 13 | 1,283 | 86 | 95 | 1,464 |
| components/charts | 1 | 0 | 59 | 4 | 63 |
| components/home-items | 6 | 274 | 18 | 18 | 310 |
| components/screens | 1 | 46 | 0 | 3 | 49 |
| tools | 1 | 33 | 0 | 6 | 39 |
| widgets | 2 | 15 | 0 | 6 | 21 |

[details](details.md)