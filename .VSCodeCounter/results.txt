Date : 2020-08-22 01:33:05
Directory : /home/mojtaba/developement/react/forusha/src
Total : 24 files,  2080 codes, 143 comments, 188 blanks, all 2411 lines

Languages
+------------+------------+------------+------------+------------+------------+
| language   | files      | code       | comment    | blank      | total      |
+------------+------------+------------+------------+------------+------------+
| JavaScript |         21 |      1,837 |        142 |        146 |      2,125 |
| CSS        |          2 |        236 |          1 |         41 |        278 |
| XML        |          1 |          7 |          0 |          1 |          8 |
+------------+------------+------------+------------+------------+------------+

Directories
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                                       | files      | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                                          |         24 |      2,080 |        143 |        188 |      2,411 |
| components                                                                                 |         13 |      1,283 |         86 |         95 |      1,464 |
| components/charts                                                                          |          1 |          0 |         59 |          4 |         63 |
| components/home-items                                                                      |          6 |        274 |         18 |         18 |        310 |
| components/screens                                                                         |          1 |         46 |          0 |          3 |         49 |
| tools                                                                                      |          1 |         33 |          0 |          6 |         39 |
| widgets                                                                                    |          2 |         15 |          0 |          6 |         21 |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| filename                                                                                   | language   | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| /home/mojtaba/developement/react/forusha/src/App.css                                       | CSS        |        224 |          1 |         39 |        264 |
| /home/mojtaba/developement/react/forusha/src/App.js                                        | JavaScript |        379 |         18 |         20 |        417 |
| /home/mojtaba/developement/react/forusha/src/App.test.js                                   | JavaScript |          8 |          0 |          2 |         10 |
| /home/mojtaba/developement/react/forusha/src/components/ChatsScreen.js                     | JavaScript |        102 |          0 |          8 |        110 |
| /home/mojtaba/developement/react/forusha/src/components/HomeScreen.js                      | JavaScript |        357 |          7 |          8 |        372 |
| /home/mojtaba/developement/react/forusha/src/components/PeopleScreen.js                    | JavaScript |        300 |          2 |         36 |        338 |
| /home/mojtaba/developement/react/forusha/src/components/ProductsScreen.js                  | JavaScript |         84 |          0 |          9 |         93 |
| /home/mojtaba/developement/react/forusha/src/components/StoresScreen.js                    | JavaScript |        120 |          0 |          9 |        129 |
| /home/mojtaba/developement/react/forusha/src/components/charts/LineChart.js                | JavaScript |          0 |         59 |          4 |         63 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/HomeHeaderSection.js    | JavaScript |         47 |          0 |          3 |         50 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/MostLikeProductItem.js  | JavaScript |         51 |          0 |          2 |         53 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/MostVisitedStoreItem.js | JavaScript |         53 |          0 |          3 |         56 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/TagItem.js              | JavaScript |          8 |          0 |          3 |         11 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/UpperDiscountItem.js    | JavaScript |         54 |          0 |          3 |         57 |
| /home/mojtaba/developement/react/forusha/src/components/home-items/UserItem.js             | JavaScript |         61 |         18 |          4 |         83 |
| /home/mojtaba/developement/react/forusha/src/components/screens/AccountScreen.js           | JavaScript |         46 |          0 |          3 |         49 |
| /home/mojtaba/developement/react/forusha/src/index.css                                     | CSS        |         12 |          0 |          2 |         14 |
| /home/mojtaba/developement/react/forusha/src/index.js                                      | JavaScript |         20 |          3 |          3 |         26 |
| /home/mojtaba/developement/react/forusha/src/logo.svg                                      | XML        |          7 |          0 |          1 |          8 |
| /home/mojtaba/developement/react/forusha/src/serviceWorker.js                              | JavaScript |         98 |         31 |         13 |        142 |
| /home/mojtaba/developement/react/forusha/src/setupTests.js                                 | JavaScript |          1 |          4 |          1 |          6 |
| /home/mojtaba/developement/react/forusha/src/tools/extentions.js                           | JavaScript |         33 |          0 |          6 |         39 |
| /home/mojtaba/developement/react/forusha/src/widgets/BoldText.js                           | JavaScript |          7 |          0 |          3 |         10 |
| /home/mojtaba/developement/react/forusha/src/widgets/PriceText.js                          | JavaScript |          8 |          0 |          3 |         11 |
| Total                                                                                      |            |      2,080 |        143 |        188 |      2,411 |
+--------------------------------------------------------------------------------------------+------------+------------+------------+------------+------------+